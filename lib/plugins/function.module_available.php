<?php

function smarty_cms_function_module_available($params, &$smarty)
{
	$name = '';
	if (isset($params['name'])) {
		$name = trim($params['name']);
	}
	if (isset($params['m'])) {
		$name = trim($params['m']);
	}
	if (isset($params['module'])) {
		$name = trim($params['module']);
	}
	$out = FALSE;
	if ($name) {
		$out = cms_utils::module_available($name);
	}
	if (isset($params['assign'])) {
		$smarty->assign(trim($params['assign']), $out);
		return;
	}
	return $out;
}

function smarty_cms_about_function_module_available()
{
	?>
	<p>Author: Robert Campbell&lt;calguy1000@cmsmadesimple.org&gt;</p>

	<p>Change History:</p>
	<ul>
		<li>Initial version (for CMSMS 1.11)</li>
	</ul>
<?php
}
?>