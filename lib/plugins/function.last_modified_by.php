<?php

function smarty_function_last_modified_by($params, &$smarty)
{
    $gCms = CmsApp::get_instance();
    $content_obj = $gCms->get_content_object();
    $id = "";

    if (isset($content_obj) && $content_obj->LastModifiedBy() > -1) {
        $id = $content_obj->LastModifiedBy();
    } else {
        return;
    }

    $format = "id";
    if (!empty($params['format'])) $format = $params['format'];
    $userops = UserOperations::get_instance();
    $thisuser = $userops->LoadUserByID($id);
    if (!$thisuser) return; // could not find user record.

    $output = '';
    if ($format === "id") {
        $output = $id;
    } else if ($format === "username") {
        $output = cms_htmlentities($thisuser->username);
    } else if ($format === "fullname") {
        $output = cms_htmlentities($thisuser->firstname . " " . $thisuser->lastname);
    }

    if (isset($params['assign'])) {
        $smarty->assign(trim($params['assign']), $output);
        return;
    }
    return $output;
}

function smarty_cms_about_function_last_modified_by()
{
    ?>
    <p>Author: Ted Kulp&lt;tedkulp@users.sf.net&gt;</p>

    <ul>Change History:</p>
        <ul>
            <li>Added assign parameter (Calguy)</li>
        </ul>
        </p>
    <?php
}
?>