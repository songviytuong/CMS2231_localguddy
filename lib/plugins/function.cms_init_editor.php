<?php

function smarty_cms_function_cms_init_editor($params, &$smarty)
{
  $wysiwyg = get_parameter_value($params, 'wysiwyg');
  $force = cms_to_bool(get_parameter_value($params, 'force', 0));

  $selector = null;
  if ($wysiwyg) {
    // we specified a wysiwyg, so we're gonna override every wysiwyg area on this page.
    $selector = 'textarea.cmsms_wysiwyg';
  } else {
    // we're gonna calll the wysiwygs
    $wysiwygs = CmsFormUtils::get_requested_wysiwyg_modules();
    if (!is_array($wysiwygs) || count($wysiwygs) == 0) return;
    $tmp = array_keys($wysiwygs);
    $wysiwyg = $tmp[0]; // first wysiwyg only, for now.
  }

  $mod = ModuleOperations::get_instance()->GetWYSIWYGModule($wysiwyg);
  if (!is_object($mod)) return;

  // get the output
  $output = $mod->WYSIWYGGenerateHeader($selector);
  if (!$output) return;

  if (isset($params['assign'])) {
    $smarty->assign(trim($params['assign']) . $output);
    return;
  }
  return $output;
}

function smarty_cms_about_function_cms_init_editor()
{
  ?>
  <p>Author: Robert Campbell&lt;calguy1000@cmsmadesimple.org&gt;</p>
  <p>Change History:</p>
  <ul>
    <li>None</li>
  </ul>
<?php
}
?>