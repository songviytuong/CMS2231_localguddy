<?php

use \CMSMS\HookManager;

function smarty_postfilter_postcompilefunc($tpl_output, $smarty)
{
    $result = explode(':', $smarty->_current_file);

    if (count($result) > 1) {
        switch ($result[0]) {
            case 'cms_stylesheet':
            case 'stylesheet':
                HookManager::do_hook('Core::StylesheetPostCompile', array('stylesheet' => &$tpl_output));
                break;

            case "content":
                HookManager::do_hook('Core::ContentPostCompile', array('content' => &$tpl_output));
                break;

            case 'cms_template':
            case "template":
            case 'tpl_top':
            case 'tpl_body':
            case 'tpl_head':
                HookManager::do_hook('Core::TemplatePostCompile', array('template' => &$tpl_output, 'type' => $result[0]));
                break;

            default:
                break;
        }
    }

    HookManager::do_hook('Core::SmartyPostCompile', array('content' => &$tpl_output));

    return $tpl_output;
}
