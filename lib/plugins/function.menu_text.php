<?php

function smarty_function_menu_text($params, &$smarty)
{
	$gCms = CmsApp::get_instance();
	$content_obj = $gCms->get_content_object();

	if (!is_object($content_obj) || $content_obj->Id() == -1) {
		// We've a custom error message...  set a message
		$result = "404 Error";
	} else {
		$result = $content_obj->MenuText();
		$result = preg_replace("/\{\/?php\}/", "", $result);
	}

	if (isset($params['assign'])) {
		$smarty->assign(trim($params['assign']), $result);
		return;
	}

	return $result;
}

function smarty_cms_about_function_menu_text()
{
	?>
	<p>Author: Ted Kulp&lt;tedkulp@users.sf.net&gt;</p>

	<p>Change History:</p>
	<ul>
		<li>None</li>
	</ul>
<?php
}
?>