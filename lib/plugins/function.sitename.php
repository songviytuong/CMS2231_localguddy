<?php

function smarty_function_sitename($params, &$smarty)
{
        $result = get_site_preference('sitename', 'CMSMS Site');

        if (isset($params['assign'])) {
                $smarty->assign(trim($params['assign']), $result);
                return;
        }
        return $result;
}

function smarty_cms_about_function_sitename()
{
        ?>
        <p>Author: Ted Kulp &lt;ted@cmsmadesimple.org&gt;</p>

        <p>Change History:</p>
        <ul>
                <li>None</li>
        </ul>
<?php
}
?>