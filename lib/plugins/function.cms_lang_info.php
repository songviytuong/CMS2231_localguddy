<?php

function smarty_function_cms_lang_info($params, &$smarty)
{
	$lang = CmsNlsOperations::get_current_language();
	if( isset($params['lang']) )
	{
		$lang = trim($params['lang']);
	}
	$info = CmsNlsOperations::get_language_info($lang);
	if( !$info ) return;

	if( isset($params['assign']) )
    {
		$smarty->assign($params['assign'],$info);
		return;
    }
	
	return $info;
}
