<?php

function smarty_cms_function_share_data($params, &$template)
{
    $dest = trim(strtolower(get_parameter_value($params, 'scope', 'parent')));
    $vars = (isset($params['data'])) ? $params['data'] : null;
    $vars = (isset($params['vars'])) ? $params['vars'] : $vars;
    if (!$vars) return; // nothing to do.

    if (is_string($vars)) {
        $t_list = explode(',', $vars);
        $t_list_2 = array();
        foreach ($t_list as $one) {
            $one = trim($one);
            if ($one) $t_list_2[] = $one;
        }
        $vars = $t_list_2;
    }

    if (!count($vars)) return;

    $scope = null;
    $fn = 'assign';
    switch ($dest) {
        case 'global':
            $scope = $template->smarty;
            $fn = 'assignGlobal';
            break;

        default: /* parent scope */
            $scope = $template->parent;
            if (!is_object($scope)) return;
            if ($scope == $template->smarty) {
                // a bit of a trick... if our parent is the global smarty object
                // we assume we want this variable available through the rest of the templates
                // so we assign it as a global.
                $fn = 'assignGlobal';
            }
            break;
    }

    foreach ($vars as $one) {
        $var = $template->getVariable($one, null, false, false);
        if (!is_a($var, 'Smarty_Undefined_Variable')) $scope->$fn($one, $var->value);
    }
}

#
# EOF
#
