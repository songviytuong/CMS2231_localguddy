<?php

function smarty_function_cms_version($params, $smarty)
{
	global $CMS_VERSION;
	if (isset($params['assign'])) {
		$smarty->assign(trim($params['assign']), $CMS_VERSION);
		return;
	}

	return $CMS_VERSION;
}

function smarty_cms_about_function_cms_version()
{
	?>
	<p>Author: Ted Kulp&lt;tedkulp@users.sf.net&gt;</p>

	<p>Change History:</p>
	<ul>
		<li>None</li>
	</ul>
<?php
}
?>