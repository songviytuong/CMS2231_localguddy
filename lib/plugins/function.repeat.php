<?php

function smarty_function_repeat($params, &$smarty)
{
	$out=(isset($params['times']) && intval($params['times']) > 0 ? str_repeat($params['string'], $params['times']) : '');
	
	if( isset($params['assign']) ){
		$smarty->assign(trim($params['assign']),$out);
		return;
	}
	return $out;
}
