<?php

function smarty_function_anchor($params, &$smarty)
{
    $content = cms_utils::get_current_content();
    if (!is_object($content)) return;

    $class = "";
    $title = "";
    $tabindex = "";
    $accesskey = "";
    if (isset($params['class'])) $class = ' class="' . $params['class'] . '"';
    if (isset($params['title'])) $title = ' title="' . $params['title'] . '"';
    if (isset($params['tabindex'])) $tabindex = ' tabindex="' . $params['tabindex'] . '"';
    if (isset($params['accesskey'])) $accesskey = ' accesskey="' . $params['accesskey'] . '"';

    $url = $content->GetURL() . '#' . trim($params['anchor']);
    $url = str_replace('&amp;', '***', $url);
    $url = str_replace('&', '&amp;', $url);
    $url = str_replace('***', '&amp;', $url);
    if (isset($params['onlyhref']) && ($params['onlyhref'] == '1' || $params['onlyhref'] == 'true')) {
        $tmp =  $url;
    } else {
        $text = get_parameter_value($params, 'text', '<!-- anchor tag: no text provided -->anchor');
        $tmp = '<a href="' . $url . '"' . $class . $title . $tabindex . $accesskey . '>' . $text . '</a>';
    }

    if (isset($params['assign'])) {
        $smarty->assign(trim($params['assign']), $tmp);
        return;
    }
    echo $tmp;
}
