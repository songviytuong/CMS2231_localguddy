<?php

function smarty_function_uploads_url($params, &$smarty)
{
	$config = CmsApp::get_instance()->GetConfig();

	$out = $config->smart_uploads_url();
	if (isset($params['assign'])) {
		$smarty->assign(trim($params['assign']), $out);
		return;
	}

	return $out;
}

function smarty_cms_about_function_uploads_url()
{
	?>
	<p>Author: Nuno Costa &ltnuno.mfcosta@sapo.pt&gt;</p>

	<p>Change History:</p>
	<ul>
		<li>None</li>
	</ul>
<?php
}
?>