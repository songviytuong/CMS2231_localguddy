<?php

function smarty_modifier_summarize($string, $numwords = '5', $etc = '...')
{
	$tmp = explode(" ", strip_tags($string));
	$stringarray = array();

	for ($i = 0; $i < count($tmp); $i++) {
		if ($tmp[$i] != '') $stringarray[] = $tmp[$i];
	}

	if ($numwords >= count($stringarray)) {
		return $string;
	}

	$tmp = array_slice($stringarray, 0, $numwords);
	$tmp = implode(' ', $tmp) . $etc;
	return $tmp;
}
