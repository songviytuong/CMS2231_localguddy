<?php

function smarty_function_cms_jquery($params, &$smarty)
{
	$exclude = trim(get_parameter_value($params, 'exclude'));
	$cdn = cms_to_bool(get_parameter_value($params, 'cdn'));
	$append = trim(get_parameter_value($params, 'append'));
	$ssl = cms_to_bool(get_parameter_value($params, 'ssl'));
	$custom_root = trim(get_parameter_value($params, 'custom_root'));
	$include_css = cms_to_bool(get_parameter_value($params, 'include_css', 1));

	// get the output
	$out = cms_get_jquery($exclude, $ssl, $cdn, $append, $custom_root, $include_css);
	if (isset($params['assign'])) {
		$smarty->assign(trim($params['assign']), $out);
		return;
	}

	return $out;
}
