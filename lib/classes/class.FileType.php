<?php

namespace CMSMS;

abstract class FileType
{
    const TYPE_IMAGE = 'image';
    const TYPE_AUDIO = 'audio';
    const TYPE_VIDEO = 'video';
    const TYPE_MEDIA = 'media';
    const TYPE_XML   = 'xml';
    const TYPE_DOCUMENT = 'document';
    const TYPE_ARCHIVE = 'archive';
    const TYPE_ANY = 'any';
} // end of class.