<?php

/**
 * Handles content related functions
 *
 * @package CMS
 */

/**
 * A convenience function to test if the site is marked as down according to the config panel.
 * This method includes handling the preference that indicates that site-down behaviour should
 * be disabled for certain IP address ranges.
 *
 * @return boolean
 */
function is_sitedown()
{
    global $CMS_INSTALL_PAGE;
    if (isset($CMS_INSTALL_PAGE)) return TRUE;

    if (cms_siteprefs::get('enablesitedownmessage') !== '1') return FALSE;

    $uid = get_userid(FALSE);
    if ($uid && cms_siteprefs::get('sitedownexcludeadmins')) return FALSE;

    if (!isset($_SERVER['REMOTE_ADDR'])) return TRUE;
    $excludes = cms_siteprefs::get('sitedownexcludes', '');
    if (empty($excludes)) return TRUE;

    $tmp = explode(',', $excludes);
    $ret = cms_ipmatches($_SERVER['REMOTE_ADDR'], $excludes);
    if ($ret) return FALSE;
    return TRUE;
}
