<?php

/* ============================================================================ 
  Module: MultiDomains
  ============================================================================ */

// Check authorisation
if (!isset($gCms))
    exit;

// Get var
$current_version = $oldversion;

// Get db handle
$dict = NewDataDictionary($db);
$taboptarray = array('mysql' => 'TYPE=MyISAM');

switch ($current_version) {
    case '1.1.6':

        $db = $this->GetDb();
        $dict = NewDataDictionary($db);

        $sqlarray = $dict->AddColumnSQL(cms_db_prefix() . "content", "domain_id I AFTER template_id");
        $dict->ExecuteSQLArray($sqlarray);
        // Increase version +Lee
        $current_version = '1.1.6.1';
}

// EOF
