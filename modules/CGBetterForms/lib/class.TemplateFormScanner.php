<?php
namespace CGBetterForms;
use CGBetterForms;

class TemplateFormScanner extends FormScanner
{
    private $_tpl_obj;

    public function __construct( CGBetterForms $mod, Form $form, Translator $tr )
    {
        $orig = ini_get('display_errors');
        @ini_set('display_errors',0);
        $rsrc = $form->getSmartyTemplateResource();
        //$rsrc = 'cms_template:'.$form->template_id;
        $this->_tpl_obj = $mod->CreateSmartyTemplate( $rsrc );
        $this->_tpl_obj->assign('form_data',new DummyFormResponse());
        $html = $this->_tpl_obj->fetch();
        parent::__construct( $html, $tr );
        @ini_set('display_errors',$orig);
    }

    public function get_template_contents()
    {
        return $this->_tpl_obj->source->getContent();
    }

    public function get_modified_time()
    {
        return $this->_tpl_obj->source->getTimeStamp();
    }

    public function get_fields()
    {
        $fields = parent::get_fields();
        $out = [];
        foreach( $fields as $field ) {
            if( $field->get_attribute('name') == 'mact' ) continue;
            if( $field->get_attribute('name') == CMS_SECURE_PARAM_NAME ) continue;
            $out[] = $field;
        }
        return $out;
    }
} // end of class


/* a simple form response object to use in the template scanner */
class DummyFormResponse
{
    public function __get( $key )
    {
        return '';
    }
}