<?php
namespace CGBetterForms\Dispositions;

abstract class Disposition implements IDisposition
{
    private $_guid;
    private $_active = true;

    public function __construct()
    {
        $this->_guid = \cge_utils::create_guid();
    }

    public function get_guid()
    {
        return $this->_guid;
    }

    public function is_active()
    {
        return $this->_active;
    }

    public function set_active( $flag = false )
    {
        $this->_active = (bool) $flag;
    }
}
