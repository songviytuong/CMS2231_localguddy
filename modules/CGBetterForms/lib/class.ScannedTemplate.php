<?php
namespace CGBetterForms;

// this class handles the metadata coming from the template tester and the templae scanner
class ScannedTemplate
{
    private $_messages;
    private $_fields;
    private $_has_captcha;
    private $_mtime;
    private $_replyto_field;

    public function __construct( IFormScanner $scanner, IFormTester $tester )
    {
        $messages = $scanner->get_errors();
        if( empty($messages) ) $messages = [];
        $messages2 = $tester->get_messages();
        if( empty($messages2) ) $messages2 = [];
        $this->_messages = array_merge( $messages, $messages2 );

        $dom_fields = $scanner->get_fields();
        $converter = new FormFieldConverter( $dom_fields );
        $this->_fields = $converter->get_fields();
        $this->_has_captcha = $scanner->has_captcha();
        $this->_mtime = $scanner->get_modified_time();
        $this->_replyto_field = $scanner->get_replyto_field();
    }

    public function get_modified_time()
    {
        return $this->_mtime;
    }

    public function get_messages()
    {
        return $this->_messages;
    }

    public function get_fields()
    {
        return $this->_fields;
    }

    public function has_captcha()
    {
        return $this->_has_captcha;
    }

    public function get_replyto_field()
    {
        return $this->_replyto_field;
    }
} // class
