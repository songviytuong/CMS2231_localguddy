<?php
//saved at 2019-07-24 20:31:27
define('MOD_ERROR_LOGGER_INFO',           false);
define('MOD_ERROR_LOGGER_WARNING',        false);
define('MOD_ERROR_LOGGER_ERROR',          true);
define('MOD_ERROR_LOGGER_EXCEPTION',      true);
define('MOD_ERROR_LOGGER_ITEMS_PER_PAGE', 10);
?>