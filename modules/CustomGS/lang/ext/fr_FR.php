<?php
$lang['moddescription'] = 'Un module qui étend les paramètres globaux. Vous pouvez definir un nombre illimité de paramètres réutilisables sous forme de variable SMARTY dans le template ou la page (et même dans les feuilles de styles)\';';
$lang['postinstall'] = 'Le module CustomGS a été installé avec succès !';
$lang['uninstall_confirm'] = 'Voulez-vous vraiment désinstaller le module CustomGS ?';
$lang['postuninstall'] = 'Le module CustomGS a été désinstallé avec succès !';
$lang['now'] = 'Maintenant';
$lang['title_general'] = 'Général';
$lang['choosetime'] = 'Choisir l\'heure';
$lang['time'] = 'heure';
$lang['title_fielddefs'] = 'Définitions de champ';
$lang['smartyvar'] = 'Variables Smarty';
$lang['textfield'] = 'Champ texte';
$lang['pulldown'] = 'Liste de choix';
$lang['checkbox'] = 'Case à cocher';
$lang['radiobuttons'] = 'Groupe de boutons radios';
$lang['datepicker'] = 'Sélecteur de date';
$lang['datetimepicker'] = 'Sélecteur de date et heure';
$lang['timepicker'] = 'Sélecteur d\'heure';
$lang['colorpicker'] = 'Sélecteur de couleur';
$lang['textarea'] = 'Zone de texte';
$lang['pageselect'] = 'Sélecteur de page';
$lang['wysiwyg'] = 'WYSIWYG&nbsp;';
$lang['fieldsetstart'] = 'Début de Fieldset';
$lang['fieldsetend'] = 'Fin de Fieldset';
$lang['button'] = 'Bouton';
$lang['maxlength'] = 'Longueur maxi.';
$lang['properties'] = 'Propriétés';
$lang['properties_help1'] = 'Entrez les valeurs, une par ligne. Supporte aussi la forme Valeur|Libellé et/ou une balises SMARTY';
$lang['parsesmarty'] = 'Interprétation SMARTY';
$lang['clearstylesheetcache'] = 'Vider le cache CSS';
$lang['clearstylesheetcache_help'] = 'Vider automatiquement le cache CSS après modification de ce paramètre. À utiliser avec précaution !';
$lang['showontab'] = 'afficher en onglet';
$lang['fielddefadded'] = 'La définition de champ est ajoutée';
$lang['fielddefsupdated'] = 'La définition de champ est mise à jour';
$lang['title_tabs'] = 'Onglets';
$lang['tabadded'] = 'L\'onglet est ajouté';
$lang['tabupdated'] = 'L\'onglet est mis à jour';
$lang['title_custom_modulename'] = 'Nom du module personnalisé';
$lang['help_custom_modulename'] = 'Vous pouvez changer le nom du module comme vous le souhaitez. Il sera utilisé comme titre dans les pages d\'admin. et dans les menus.';
$lang['title_admin_section'] = 'Section d\'administration';
$lang['help_admin_section'] = 'Choisissez la section dans laquelle le module apparaîtra.';
$lang['xml_export'] = 'Exporter les paramètres en XML';
$lang['xml_import'] = 'Importer les paramètres en XML';
$lang['settingssaved'] = 'Paramètres sauvegardés !';
$lang['updatefailed'] = 'La mise à jour a échoué !';
$lang['event_info_OnSettingChange'] = 'Evenement déclenché quand un paramètre est modifié.';
$lang['event_help_OnSettingChange'] = '<p>Evenement déclenché quand un paramètre est modifié.</p>
<h4>Parametres</h4>
<ul>
<li>fieldid</li>
<li>name</li>
<li>alias</li>
<li>value</li>
<li>clearcache</li>
</ul>';
?>