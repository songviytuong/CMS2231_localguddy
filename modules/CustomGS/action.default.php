<?php
#---------------------------------------------------------------------------
# Module: Custom Global Settings
# Author: Rolf Tjassens (cmscanbesimple.org), Jos
#---------------------------------------------------------------------------
# CMS Made Simple - Power for the professional, Simplicity for the end user.
# (c) 2004 - 2011 by Ted Kulp (wishy@cmsmadesimple.org)
# (c) 2011 - 2018 by The CMS Made Simple Development Team
# (c) 2018 - 2019 by The CMS Made Simple Foundation
# This project's homepage is: https://www.cmsmadesimple.org
# The module's homepage is: http://dev.cmsmadesimple.org/projects/customgs
#---------------------------------------------------------------------------
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#---------------------------------------------------------------------------

if (!isset($gCms)) exit;

// Pass the fields to the templates
if ( isset($params['showvars']) )
{
	$fields = $this->GetSettings();
	foreach ( $fields as $key=>$value )
	{
		if ( !is_numeric($key) ) echo '{$CustomGS.' . $key . '} = ' . $value . '<br />';
	}
}
else 
{
	// Redundant {CustomGS} tag found
	debug_to_log('Redundant {CustomGS} tag found. Since release 2.2 this tag is no longer needed. The module gets automatically initialized. You can remove the tag from your template(s).');
}

#
# EOF
#
?>