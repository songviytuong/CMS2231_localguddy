{if $items|@count > 0}

<script>
	var tabschanged = '';
	var ajax_load = '<img src="{$cgs_url_path}/images/loading.gif" alt="loading..." />';

	jQuery(document).ready(function($) {

		$(".sort_table_tabs").tableDnD( {
			onDragClass: "row1hover",
			onDrop: function(table, row) {

				jQuery(".sort_table_tabs").find("tbody tr").removeClass();
				jQuery(".sort_table_tabs").find("tbody tr:nth-child(2n+1)").addClass("row1");
				jQuery(".sort_table_tabs").find("tbody tr:nth-child(2n)").addClass("row2");

				var rows = table.tBodies[0].rows;
				var sortstr = rows[0].id;
				for (var i=1; i<rows.length; i++) {
					sortstr += ","+rows[i].id;
				}
				
				$('#loader').html(ajax_load).load('{cms_action_url module=CustomGS action=edittab forjs=1 mid="{$mod_id}" mode=sort showtemplate=false}&{$mod_id}sortseq='+sortstr);
				tabschanged = true;
			}
		} );

		//$(".updown").hide();

		$('#page_tabs div').click(function() {
			if(tabschanged) location.href = '{cms_action_url module=CustomGS action=defaultadmin forjs=1 mid="{$mod_id}"}&{$mod_id}active_tab=' + $(this).attr('id');
			return false;
		} );

	} );
</script>

<div class="pageoverflow">
	<p class="pageoptions">{$newtablink}&nbsp;&nbsp;&nbsp;<span id="loader"> </span></p>
</div>

<div class="pageoverflow">
	{form_start action='edittab'}
	<table class="pagetable sort_table_tabs">
		<thead>
	    	<tr>
				<th class="pagew25">{$name}</th>
				<th class="pageicon updown">&nbsp;</th>
				<th class="pageicon updown">&nbsp;</th>
				<th class="pageicon">&nbsp;</th>
				<th class="pageicon">&nbsp;</th>
			</tr>
		</thead>
		{foreach from=$items item=entry}
			{cycle values="row1,row2" assign=rowclass}
			<tr id="{$entry->tabid}" class="{$rowclass}">
				<td>{$entry->name}</td>
				<td class="updown">{$entry->moveup}</td>
				<td class="updown">{$entry->movedown}</td>
				<td>{$entry->editlink}</td>
				<td>{$entry->deletelink}</td>
			</tr>
		{/foreach}
	</table>
	{form_end}
</div>

{/if}

<div class="pageoverflow">
	<p class="pageoptions">{$newtablink}</p>
</div>