<script src="{$cgs_url_path}/lib/jquery/jquery.cookie.js"></script>
<script src="{$cgs_url_path}/lib/jquery/jquery.collapsible.js"></script>
<script src="{$cgs_url_path}/lib/jquery/jquery-ui-timepicker-addon.js"></script>
<script src="{$cgs_url_path}/lib/jquery/colorpicker.min.js"></script>

<script>
	$(function() {
		$('.cgs_collapsible').collapsible( {
			cookieName: 'collapsible'
		} );
		{$datapicker_locale}
		$('.datepicker input').datepicker( {
			dateFormat: 'yy-mm-dd',
			showOtherMonths: true,
			selectOtherMonths: true
		} );
		$('.datetimepicker input').datetimepicker( {
			dateFormat: 'yy-mm-dd',
			showOtherMonths: true,
			selectOtherMonths: true
		} );
		$('.timepicker input').timepicker( { } );
		$('.inputcolorpicker input').ColorPicker( {
			onSubmit: function(hsb, hex, rgb, el, parent) {
				$(el).val(hex);
				$(el).ColorPickerHide();
			} ,
			onBeforeShow: function() {
				$(this).ColorPickerSetColor(this.value);
			}
		} )
		.on('keyup', function() {
			$(this).ColorPickerSetColor(this.value);
		} );
	} );
</script>