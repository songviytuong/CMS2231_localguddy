{if $items|@count > 0}

<script>
	var fielddefschanged = '';
	var ajax_load = '<img src="{$cgs_url_path}/images/loading.gif" alt="loading..." />';
	var trueimg = '{$trueimage}';
	var falseimg = '{$falseimage}';

	jQuery(document).ready(function($) { 

		$('#tabselect').change( function() {
			location.href = '{cms_action_url module=CustomGS action=defaultadmin forjs=1 mid="{$mod_id}" active_tab="fielddefs"}&{$mod_id}tabid='+$(this).val();
		} );

		$('.sort_table_fielddefs').tableDnD( {
			onDragClass: "row1hover",
			onDrop: function(table, row) {

				jQuery(".sort_table_fielddefs").find("tbody tr").removeClass();
				jQuery(".sort_table_fielddefs").find("tbody tr:nth-child(2n+1)").addClass("row1");
				jQuery(".sort_table_fielddefs").find("tbody tr:nth-child(2n)").addClass("row2");

				var rows = table.tBodies[0].rows;
				var sortstr = rows[0].id;
				for (var i=1; i<rows.length; i++) {
					sortstr += ","+rows[i].id; 
				} 
				$('#loader').html(ajax_load).load('{cms_action_url module=CustomGS action=editfielddef forjs=1 mid="{$mod_id}" mode=sort showtemplate=false}&{$mod_id}sortseq='+sortstr);
				fielddefschanged = true;
			}
		} );

		//$('.updown').hide();
		
		$('a.assigntab').click(function(event) { 
			event.preventDefault(); 
			var self = $(this);
			$.ajax( { url: self.attr('href'), success: function(result) { 
				if(result == 1)
					self.html(trueimg);
				else
					self.html(falseimg);
				fielddefschanged = true;
			} } );
			return false;
		} );
		
		$('#page_tabs div').click(function() {
			if(fielddefschanged) location.href = '{cms_action_url module=CustomGS action=defaultadmin forjs=1 mid="{$mod_id}"}&{$mod_id}active_tab=' + $(this).attr('id');
			return false;
		} );
		
	} );
</script>

<div class="pageoverflow">
	<p class="pageoptions">{$newfielddeflink}&nbsp;&nbsp;&nbsp;<span id="loader"> </span></p>
</div>

<div class="pageoverflow">
	{form_start action='editfielddef'}
	<table class="pagetable sort_table_fielddefs">
		<thead>
			<tr>
				<th class="pagew25">{$name}</th>
				<th class="pagew25">{$type}</th>
				<th class="pagew25">{$smartyvar}</th>
				<th class="pageicon updown">&nbsp;</th>
				<th class="pageicon updown">&nbsp;</th>
				<th class="pageicon">{$showontab}&nbsp;{$tabselect}</th>
				<th class="pageicon">&nbsp;</th>
				<th class="pageicon">&nbsp;</th>
			</tr>
		</thead>
		{$groupindent = ''}
		{foreach from=$items item=entry}
			{cycle values="row1,row2" assign=rowclass}
			{if $entry->type == 'Fieldgroup start'}{$groupindent = '&nbsp;&nbsp;&nbsp;'}{/if}
			<tr id="i{$entry->fieldid}" class="{$rowclass}">
				<td>{if empty($entry->smartyvar)}<strong>{$entry->name}</strong>{else}{$groupindent}{$entry->name}{/if}</td>
				<td>{$entry->type}</td>
				<td><span class="smartyvar">{$entry->smartyvar}</span></td>
				<td class="updown">{$entry->moveup}</td>
				<td class="updown">{$entry->movedown}</td>
				<td style="text-align: center">{$entry->assigntab}</td>
				<td>{$entry->editlink}</td>
				<td>{$entry->deletelink}</td>
			</tr>
			{if $entry->type == 'Fieldgroup end'}{$groupindent = ''}{/if}
		{/foreach}
	</table>
	{form_end}
</div>

{/if}

<div class="pageoverflow">
	<p class="pageoptions">{$newfielddeflink}</p>
</div>