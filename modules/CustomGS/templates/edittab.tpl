<div class="pageoverflow"><h3>{$title}</h3></div>

{form_start action='edittab'}{$hidden}

<div class="pageoverflow">
  <p class="pagetext">{$prompt_name}:</p>
  <p class="pageinput">{$name}</p>
</div>

<div class="pageoverflow">
  <p class="pagetext">{$prompt_editors}:</p>
  <p class="pageinput">{$editors}</p>
</div>

<div class="pageoverflow">
  <p class="pagetext">&nbsp;</p>
  <p class="pageinput">{$submit}{$cancel}</p>
  <p>&nbsp;</p>
</div>

{form_end}