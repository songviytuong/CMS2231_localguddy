{*<div class="row">
<div class="pageoptions options-menu half">
<a accesskey="a" href="{cms_action_url action='admin_edit_design'}" title="{$mod->Lang('create_design')}">{admin_icon icon='newobject.gif'} {$mod->Lang('create_design')}</a>&nbsp;&nbsp;
<a accesskey="a" href="{cms_action_url action='admin_import_design'}" title="{$mod->Lang('title_import_design')}">{admin_icon icon='import.gif'} {$mod->Lang('import_design')}</a>
</div>
</div>*}

{if isset($list_domains)}
    <table class="pagetable">
        <thead>
            <tr>
                <th width="5%">{$mod->Lang('prompt_id')}</th>
                <th>{$mod->Lang('prompt_name')}</th>
                <th class="pageicon"><span title="{$mod->Lang('title_domain_default')}">{lang('default')}</span></th>
                <th class="pageicon"></th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$list_domains key='k' item='domain'}
                {cycle values="row1,row2" assign='rowclass'}
                {cms_action_url action=admin_delete_domain domain=$domain->get_id() assign='delete_url'}
                <tr class="{$rowclass}" onmouseover="this.className = '{$rowclass}hover';" onmouseout="this.className = '{$rowclass}';">
                    <td>{$domain->get_id()}</td>
                    <td>{$domain->get_domain()}</td>
                    <td>
                        {if $domain->get_default()}
                            {admin_icon icon='true.gif' title=$mod->Lang('prompt_dflt')}
                        {else}
                            <a href="{cms_action_url domain_setdflt=$domain->get_id()}">{admin_icon icon='false.gif' title=$mod->Lang('prompt_setdflt_domain')}</a>
                        {/if}
                    </td>
                    <td>
                        {if !$domain->get_default()}
                            <a href="{$delete_url}" title="{$mod->Lang('delete_domain')}">{admin_icon icon='delete.gif'}</a>
                        {/if}
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>
{/if}