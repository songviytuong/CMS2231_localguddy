{form_start domain=$domain->get_id()}
<h3>{$mod->Lang('delete_domain')}: {$domain->get_domain()} ({$domain->get_id()})</h3>

<div class="pagewarning">{$mod->Lang('warning_deletedomain')}</div>

<div class="pageoverflow">
  <p class="pagetext">{$mod->Lang('confirm_delete_domain_1')}:</p>
  <p class="pageinput">
    <input type="checkbox" id="opt_delete1" value="yes" name="{$actionid}confirm_delete1"/>&nbsp;
<label for="opt_delete1">{$mod->Lang('confirm_delete_2a')}:</label><br/>
    <input type="checkbox" id="opt_delete2" value="yes" name="{$actionid}confirm_delete2"/>&nbsp;
<label for="opt_delete2">{$mod->Lang('confirm_delete_2b')}:</label>
  </p>
</div>

<div class="pageoverflow">
  <p class="pagetext"></p>
  <p class="pageinput">
    <input type="submit" name="{$actionid}submit" value="{$mod->Lang('submit')}"/>
    <input type="submit" name="{$actionid}cancel" value="{$mod->Lang('cancel')}"/>
  </p>
</div>
{form_end}

<div style="display: none;">
  <div id="help_rm_tpl" title="{$mod->Lang('prompt_help')}">{$mod->Lang('help_rm_tpl')}</div>
  <div id="help_rm_css" title="{$mod->Lang('prompt_help')}">{$mod->Lang('help_rm_css')}</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
  $('.helpicon').click(function(){
    var x = $(this).attr('name');
    $('#'+x).dialog();
  });
});
</script>