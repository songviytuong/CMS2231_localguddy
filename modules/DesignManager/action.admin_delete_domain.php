<?php

if( !isset($gCms) ) exit;
if( !$this->CheckPermission('Manage Domains') ) return;

$this->SetCurrentTab('domains');
if( isset($params['cancel']) ) {
    $this->SetMessage($this->Lang('msg_cancelled'));
    $this->RedirectToAdminTab();
}

try {
    if( !isset($params['domain']) ) {
        throw new CmsException($this->Lang('error_missingparam'));
    }
    $domain = CmsDomainCollection::load($params['domain']);

    if( isset($params['submit']) ) {
        
        if( !isset($params['confirm_delete1']) || $params['confirm_delete1'] != 'yes' ||
            !isset($params['confirm_delete2']) || $params['confirm_delete2'] != 'yes') {
            $this->SetError($this->Lang('error_notconfirmed'));
            $this->RedirectToAdminTab();
        }

        // done... we 'force' the delete because we loaded the design object
		// before deleting the templates and stylesheets.
        $domain->delete(TRUE);
        
        $this->SetMessage($this->Lang('msg_domain_deleted'));
        $this->RedirectToAdminTab();
    }

    $smarty->assign('domain',$domain);
    echo $this->ProcessTemplate('admin_delete_domain.tpl');
}
catch( CmsException $e ) {
    $this->SetError($e->GetMessage());
    $this->RedirectToAdminTab();
}
