<?php
namespace CGExtensions;

interface ITranslator
{
    public function __invoke(/* var args */);
} // class