<!DOCTYPE html>
<html>

<head>
    <title>::Submit Your Localtion Services::Near You::</title>
    <meta charset="utf-8">
    <link href="//www.google.com/images/branding/product/ico/maps_32dp.ico" rel="shortcut icon">
    <meta name="viewport" content="initial-scale=1.0, width=device-width, maximum-scale=1">
    <style>
        #map {
            height: 100%;
        }

        html,
        body {
            height: 100%;
            margin: 0;
            padding: 0;
        }

        #description {
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
        }

        #infowindow-content .title {
            font-weight: bold;
        }

        #infowindow-content {
            display: none;
        }

        #map #infowindow-content {
            display: inline;
        }

        .info i {
            color: #F6AA93;
        }

        p span {
            color: #F00;
        }

        p {
            margin: 0px;
            font-weight: 500;
            line-height: 2;
            color: #333;
        }

        h1 {
            text-align: center;
            color: #666;
            text-shadow: 1px 1px 0px #FFF;
            margin: 50px 0px 0px 0px
        }

        input {
            border-radius: 0px 5px 5px 0px;
            border: 1px solid #eee;
            width: 75%;
            height: 40px;
            float: left;
            padding: 0px 15px;
        }

        select {
            border-radius: 0px 5px 5px 0px;
            border: 1px solid #eee;
            width: 84%;
            height: 42px;
            float: left;
            padding: 0px 15px;
        }

        a {
            text-decoration: inherit
        }

        textarea {
            border-radius: 0px 5px 5px 0px;
            border: 1px solid #EEE;
            margin: 0;
            width: 75%;
            float: left;
            padding: 10px 15px;
        }

        .form-group {
            overflow: hidden;
            clear: both;
        }

        .icon-case {
            width: 35px;
            float: left;
            border-radius: 5px 0px 0px 5px;
            background: #eeeeee;
            height: 42px;
            position: relative;
            text-align: center;
            line-height: 40px;
        }

        i {
            color: #555;
        }

        .bouton-contact {
            background-color: #81BDA4;
            color: #FFF;
            text-align: center;
            width: 100%;
            border: 0;
            padding: 7px 10px;
            border-radius: 0px 0px 5px 5px;
            cursor: pointer;
            margin-top: 10px;
            font-size: 12px;
        }

        .rightcontact {
            width: 100%;
            float: right;
            box-sizing: border-box;
            padding: 0px 0px 0px 15px;
        }

        #sendmessage {
            border: 1px solid #fff;
            display: none;
            text-align: center;
            margin: 10px 0;
            font-weight: 600;
            background-color: #EBF6E0;
            color: #5F9025;
            border: 1px solid #B3DC82;
            border-radius: 3px;
            box-shadow: 0px 1px 1px 0px rgba(0, 0, 0, 0.03);
        }

        #pac-card {
            margin: 10px 10px 0 0;
            border-radius: 2px 0 0 2px;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            outline: none;
            box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
            background-color: #fff;
            font-family: Roboto;
            position: absolute;
            top: 50px !important;
            right: -280px !important;
            -webkit-transition: all 0.5s ease-in-out;
            -moz-transition: all 0.5s ease-in-out;
            -ms-transition: all 0.5s ease-in-out;
            -o-transition: all 0.5s ease-in-out;
        }

        #pac-card:hover {
            /* right:-360px !important; */
        }

        #pac-card.show {
            right: 0px !important;
        }

        #pac-card a.controller {
            position: absolute;
            left: 10px !important;
            top: -10px !important;
            text-decoration: none;
            -webkit-transition: all 0.5s ease-in-out;
            -moz-transition: all 0.5s ease-in-out;
            -ms-transition: all 0.5s ease-in-out;
            -o-transition: all 0.5s ease-in-out;
            color: black;
            font-weight: bold;
        }

        #pac-card.show a.controller {
            -webkit-transform: rotate(180deg);
            -moz-transform: rotate(180deg);
            -ms-transform: rotate(180deg);
            -o-transform: rotate(180deg);
        }

        a.controller {
            border: 1px solid #B3DC82;
            width: 20px;
            text-align: center;
            line-height: 20px;
            border-radius: 10px;
            position: absolute;
            left: -10px !important;
            background: #fff;
        }

        button {
            border-radius: 20px;
        }
    </style>

</head>

<body>
    <div id="pac-card">
        <a href="#" onclick="showLeftPanel();" class="controller">&lt;</a>
        <br/>
        <div class="contentform">

            <div class="rightcontact">
                <div class="form-group">
                    <p>Location <span>*</span></p>
                    <span class="icon-case"><button onClick="blah()">x</button></span>
                    <input id="pac-input" type="text" placeholder="Enter a location" />
                </div>

                <div class="form-group">
                    <p>Latitude <span>*</span></p>
                    <span class="icon-case"><i class="fa fa-info"></i></span>
                    <input type="text" name="lat" id="lat" data-rule="required" data-msg="" />
                </div>

                <div class="form-group">
                    <p>Longitude <span>*</span></p>
                    <span class="icon-case"><i class="fa fa-comment-o"></i></span>
                    <input type="text" name="lng" id="lng" data-rule="required" data-msg="" />
                </div>

                <div class="form-group">
                    <p>Category</p>
                    <span class="icon-case"><i class="fa fa-comment-o"></i></span>
                    <select id="kategori" name="kategori">
                        <option value="1">Photo-Copy Service</option>
                        <option value="2">Key repair service</option>
                        <option value="3">Shoes repair service</option>
                    </select>
                </div>

                <div class="form-group">
                    <p>Country</p>
                    <span class="icon-case"><i class="fa fa-comment-o"></i></span>
                    <select id="country_id" name="country_id">
                        <option value="1">Afghanistan</option>
                        <option value="2">Aland Islands</option>
                        <option value="3">Albania</option>
                        <option value="4">Algeria</option>
                        <option value="5">American Samoa</option>
                        <option value="6">Andorra</option>
                        <option value="7">Angola</option>
                        <option value="8">Anguilla</option>
                        <option value="9">Antarctica</option>
                        <option value="10">Antigua and Barbuda</option>
                        <option value="11">Argentina</option>
                        <option value="12">Armenia</option>
                        <option value="13">Aruba</option>
                        <option value="14">Australia</option>
                        <option value="15">Austria</option>
                        <option value="16">Azerbaijan</option>
                        <option value="17">Bahamas</option>
                        <option value="18">Bahrain</option>
                        <option value="19">Bangladesh</option>
                        <option value="20">Barbados</option>
                        <option value="21">Belarus</option>
                        <option value="22">Belgium</option>
                        <option value="23">Belize</option>
                        <option value="24">Benin</option>
                        <option value="25">Bermuda</option>
                        <option value="26">Bhutan</option>
                        <option value="27">Bolivia</option>
                        <option value="28">Bonaire, Sint Eustatius and Saba</option>
                        <option value="29">Bosnia and Herzegovina</option>
                        <option value="30">Botswana</option>
                        <option value="31">Bouvet Island</option>
                        <option value="32">Brazil</option>
                        <option value="33">British Indian Ocean Territory</option>
                        <option value="34">Brunei</option>
                        <option value="35">Bulgaria</option>
                        <option value="36">Burkina Faso</option>
                        <option value="37">Burundi</option>
                        <option value="38">Cambodia</option>
                        <option value="39">Cameroon</option>
                        <option value="40">Canada</option>
                        <option value="41">Cape Verde</option>
                        <option value="42">Cayman Islands</option>
                        <option value="43">Central African Republic</option>
                        <option value="44">Chad</option>
                        <option value="45">Chile</option>
                        <option value="46">China</option>
                        <option value="47">Christmas Island</option>
                        <option value="48">Cocos (Keeling) Islands</option>
                        <option value="49">Colombia</option>
                        <option value="50">Comoros</option>
                        <option value="51">Congo</option>
                        <option value="52">Cook Islands</option>
                        <option value="53">Costa Rica</option>
                        <option value="54">Cote d'ivoire (Ivory Coast)</option>
                        <option value="55">Croatia</option>
                        <option value="56">Cuba</option>
                        <option value="57">Curacao</option>
                        <option value="58">Cyprus</option>
                        <option value="59">Czech Republic</option>
                        <option value="60">Democratic Republic of the Congo</option>
                        <option value="61">Denmark</option>
                        <option value="62">Djibouti</option>
                        <option value="63">Dominica</option>
                        <option value="64">Dominican Republic</option>
                        <option value="65">Ecuador</option>
                        <option value="66">Egypt</option>
                        <option value="67">El Salvador</option>
                        <option value="68">Equatorial Guinea</option>
                        <option value="69">Eritrea</option>
                        <option value="70">Estonia</option>
                        <option value="71">Ethiopia</option>
                        <option value="72">Falkland Islands (Malvinas)</option>
                        <option value="73">Faroe Islands</option>
                        <option value="74">Fiji</option>
                        <option value="75">Finland</option>
                        <option value="76">France</option>
                        <option value="77">French Guiana</option>
                        <option value="78">French Polynesia</option>
                        <option value="79">French Southern Territories</option>
                        <option value="80">Gabon</option>
                        <option value="81">Gambia</option>
                        <option value="82">Georgia</option>
                        <option value="83">Germany</option>
                        <option value="84">Ghana</option>
                        <option value="85">Gibraltar</option>
                        <option value="86">Greece</option>
                        <option value="87">Greenland</option>
                        <option value="88">Grenada</option>
                        <option value="89">Guadaloupe</option>
                        <option value="90">Guam</option>
                        <option value="91">Guatemala</option>
                        <option value="92">Guernsey</option>
                        <option value="93">Guinea</option>
                        <option value="94">Guinea-Bissau</option>
                        <option value="95">Guyana</option>
                        <option value="96">Haiti</option>
                        <option value="97">Heard Island and McDonald Islands</option>
                        <option value="98">Honduras</option>
                        <option value="99">Hong Kong</option>
                        <option value="100">Hungary</option>
                        <option value="101">Iceland</option>
                        <option value="102">India</option>
                        <option value="103">Indonesia</option>
                        <option value="104">Iran</option>
                        <option value="105">Iraq</option>
                        <option value="106">Ireland</option>
                        <option value="107">Isle of Man</option>
                        <option value="108">Israel</option>
                        <option value="109">Italy</option>
                        <option value="110">Jamaica</option>
                        <option value="111">Japan</option>
                        <option value="112">Jersey</option>
                        <option value="113">Jordan</option>
                        <option value="114">Kazakhstan</option>
                        <option value="115">Kenya</option>
                        <option value="116">Kiribati</option>
                        <option value="117">Kosovo</option>
                        <option value="118">Kuwait</option>
                        <option value="119">Kyrgyzstan</option>
                        <option value="120">Laos</option>
                        <option value="121">Latvia</option>
                        <option value="122">Lebanon</option>
                        <option value="123">Lesotho</option>
                        <option value="124">Liberia</option>
                        <option value="125">Libya</option>
                        <option value="126">Liechtenstein</option>
                        <option value="127">Lithuania</option>
                        <option value="128">Luxembourg</option>
                        <option value="129">Macao</option>
                        <option value="130">Macedonia</option>
                        <option value="131">Madagascar</option>
                        <option value="132">Malawi</option>
                        <option value="133">Malaysia</option>
                        <option value="134">Maldives</option>
                        <option value="135">Mali</option>
                        <option value="136">Malta</option>
                        <option value="137">Marshall Islands</option>
                        <option value="138">Martinique</option>
                        <option value="139">Mauritania</option>
                        <option value="140">Mauritius</option>
                        <option value="141">Mayotte</option>
                        <option value="142">Mexico</option>
                        <option value="143">Micronesia</option>
                        <option value="144">Moldava</option>
                        <option value="145">Monaco</option>
                        <option value="146">Mongolia</option>
                        <option value="147">Montenegro</option>
                        <option value="148">Montserrat</option>
                        <option value="149">Morocco</option>
                        <option value="150">Mozambique</option>
                        <option value="151">Myanmar (Burma)</option>
                        <option value="152">Namibia</option>
                        <option value="153">Nauru</option>
                        <option value="154">Nepal</option>
                        <option value="155">Netherlands</option>
                        <option value="156">New Caledonia</option>
                        <option value="157">New Zealand</option>
                        <option value="158">Nicaragua</option>
                        <option value="159">Niger</option>
                        <option value="160">Nigeria</option>
                        <option value="161">Niue</option>
                        <option value="162">Norfolk Island</option>
                        <option value="163">North Korea</option>
                        <option value="164">Northern Mariana Islands</option>
                        <option value="165">Norway</option>
                        <option value="166">Oman</option>
                        <option value="167">Pakistan</option>
                        <option value="168">Palau</option>
                        <option value="169">Palestine</option>
                        <option value="170">Panama</option>
                        <option value="171">Papua New Guinea</option>
                        <option value="172">Paraguay</option>
                        <option value="173">Peru</option>
                        <option value="174">Phillipines</option>
                        <option value="175">Pitcairn</option>
                        <option value="176">Poland</option>
                        <option value="177">Portugal</option>
                        <option value="178">Puerto Rico</option>
                        <option value="179">Qatar</option>
                        <option value="180">Reunion</option>
                        <option value="181">Romania</option>
                        <option value="182">Russia</option>
                        <option value="183">Rwanda</option>
                        <option value="184">Saint Barthelemy</option>
                        <option value="185">Saint Helena</option>
                        <option value="186">Saint Kitts and Nevis</option>
                        <option value="187">Saint Lucia</option>
                        <option value="188">Saint Martin</option>
                        <option value="189">Saint Pierre and Miquelon</option>
                        <option value="190">Saint Vincent and the Grenadines</option>
                        <option value="191">Samoa</option>
                        <option value="192">San Marino</option>
                        <option value="193">Sao Tome and Principe</option>
                        <option value="194">Saudi Arabia</option>
                        <option value="195">Senegal</option>
                        <option value="196">Serbia</option>
                        <option value="197">Seychelles</option>
                        <option value="198">Sierra Leone</option>
                        <option value="199">Singapore</option>
                        <option value="200">Sint Maarten</option>
                        <option value="201">Slovakia</option>
                        <option value="202">Slovenia</option>
                        <option value="203">Solomon Islands</option>
                        <option value="204">Somalia</option>
                        <option value="205">South Africa</option>
                        <option value="206">South Georgia and the South Sandwich Islands</option>
                        <option value="207">South Korea</option>
                        <option value="208">South Sudan</option>
                        <option value="209">Spain</option>
                        <option value="210">Sri Lanka</option>
                        <option value="211">Sudan</option>
                        <option value="212">Suriname</option>
                        <option value="213">Svalbard and Jan Mayen</option>
                        <option value="214">Swaziland</option>
                        <option value="215">Sweden</option>
                        <option value="216">Switzerland</option>
                        <option value="217">Syria</option>
                        <option value="218">Taiwan</option>
                        <option value="219">Tajikistan</option>
                        <option value="220">Tanzania</option>
                        <option value="221">Thailand</option>
                        <option value="222">Timor-Leste (East Timor)</option>
                        <option value="223">Togo</option>
                        <option value="224">Tokelau</option>
                        <option value="225">Tonga</option>
                        <option value="226">Trinidad and Tobago</option>
                        <option value="227">Tunisia</option>
                        <option value="228">Turkey</option>
                        <option value="229">Turkmenistan</option>
                        <option value="230">Turks and Caicos Islands</option>
                        <option value="231">Tuvalu</option>
                        <option value="232">Uganda</option>
                        <option value="233">Ukraine</option>
                        <option value="234">United Arab Emirates</option>
                        <option value="235">United Kingdom</option>
                        <option value="236">United States</option>
                        <option value="237">United States Minor Outlying Islands</option>
                        <option value="238">Uruguay</option>
                        <option value="239">Uzbekistan</option>
                        <option value="240">Vanuatu</option>
                        <option value="241">Vatican City</option>
                        <option value="242">Venezuela</option>
                        <option value="243" selected>Vietnam</option>
                        <option value="244">Virgin Islands, British</option>
                        <option value="245">Virgin Islands, US</option>
                        <option value="246">Wallis and Futuna</option>
                        <option value="247">Western Sahara</option>
                        <option value="248">Yemen</option>
                        <option value="249">Zambia</option>
                        <option value="250">Zimbabwe</option>
                    </select>
                </div>

                <div class="form-group">
                    <p>Information About The Place <span>*</span></p>
                    <span class="icon-case"><i class="fa fa-comments-o"></i></span>
                    <textarea name="aciklama" id="aciklama" rows="2" data-rule="required" data-msg=""></textarea>
                </div>
            </div>
        </div>
        <button type="submit" class="bouton-contact">Submit Your Location Service</button>

    </div>
    <div id="map"></div>
    <div id="infowindow-content">
        <img src="" width="16" height="16" id="place-icon">
        <span id="place-name" class="title"></span>
        <br>
        <span id="place-address"></span>
    </div>

    <script>
        var map;
        var geocoder;
        var marker;
        var infowindow;

        function detectmob() {
        if(window.innerWidth <= 800 || window.innerHeight <= 600) {
                return true;
            } else {
                return false;
            }
        }

        if (!detectmob()){
            //top.location.href= "http://www.vietnammuslimtourkaka.com";
        }

        function blah() {
            document.getElementById("pac-input").value = "";
        }

        function showLeftPanel() {
            var elem = document.getElementById("pac-card");
            elem.classList.toggle("show");
        }

        function initMap() {
            var latlng = new google.maps.LatLng(10.823099, 106.629664);
            var myOptions = {
                zoom: 17,
                center: latlng
            };
            map = new google.maps.Map(document.getElementById("map"),
                myOptions);
            geocoder = new google.maps.Geocoder();
            // marker = new google.maps.Marker({
            //     position: latlng,
            //     map: map
            // });
			
			var locations = [
			  ['Bondi Beach', 10.786652, 106.692632, 4],
			  ['Coogee Beach', 10.786283,106.692595, 5],
			  ['Cronulla Beach', 10.785688, 106.692203, 3],
			  ['Manly Beach', 10.786020, 106.690943, 2],
			  ['Maroubra Beach', 10.787553, 106.690374, 1]
			];

			var map = new google.maps.Map(document.getElementById('map'), {
			  zoom: 18,
			  center: new google.maps.LatLng(10.786156, 106.691819),
			  mapTypeId: google.maps.MapTypeId.ROADMAP
			});

			var infowindow = new google.maps.InfoWindow();

			var marker, i;

			for (i = 0; i < locations.length; i++) {  
			  marker = new google.maps.Marker({
				position: new google.maps.LatLng(locations[i][1], locations[i][2]),
				map: map
			  });

			  google.maps.event.addListener(marker, 'click', (function(marker, i) {
				return function() {
                    
				  infowindow.setContent(locations[i][0]);
				  infowindow.open(map, marker);
				}
			  })(marker, i));
			}
			
            var card = document.getElementById('pac-card');
            var input = document.getElementById('pac-input');
            var types = document.getElementById('type-selector');
            var strictBounds = document.getElementById('strict-bounds-selector');

            map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

            var autocomplete = new google.maps.places.Autocomplete(input);

            // Bind the map's bounds (viewport) property to the autocomplete object,
            // so that the autocomplete requests use the current map bounds for the
            // bounds option in the request.
            autocomplete.bindTo('bounds', map);

            // Set the data fields to return when the user selects a place.
            autocomplete.setFields(
                ['address_components', 'geometry', 'icon', 'name']);

            var infowindow = new google.maps.InfoWindow();
            var infowindowContent = document.getElementById('infowindow-content');
            infowindow.setContent(infowindowContent);
            
            // Try HTML5 geolocation.
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };

                geocoder.geocode({
                    'latLng': pos
                }, function(results, status) {
                    
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            document.getElementById('aciklama').value = results[0].formatted_address;
                        }
                    }
                });

                infowindow.setPosition(pos);
                var im = 'http://i.stack.imgur.com/orZ4x.png';

                infowindow.setContent('Blah blah blah :)');
                map.setCenter(pos);

                //show current position
                var userMarker = new google.maps.Marker({
                    position: pos,
                    map: map,
                    icon: im
                });

                infowindow.open(map, userMarker);


                document.getElementById('lat').value = position.coords.latitude.toFixed(6);
                document.getElementById('lng').value = position.coords.longitude.toFixed(6);

                // marker.setPosition(pos);

                }, function() {
                    handleLocationError(true, infowindow, map.getCenter());
                });
            } else {
                // Browser doesn't support Geolocation
                handleLocationError(false, infowindow, map.getCenter());

            }

            function handleLocationError(browserHasGeolocation, infowindow, pos) {
                infowindow.setPosition(pos);
                infowindow.setContent(browserHasGeolocation ?
                                        'Error: The Geolocation service failed.' :
                                        'Error: Your browser doesn\'t support geolocation.');
                }

            var marker = new google.maps.Marker({
                map: map,
                anchorPoint: new google.maps.Point(0, -29)
            });

            google.maps.event.addListener(map, 'click', function(event) {

                geocoder.geocode({
                    'latLng': event.latLng
                }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            console.log(results[0].formatted_address);
                            document.getElementById('aciklama').value = results[0].formatted_address;
                        }
                    }
                });
                
                marker.setPosition(event.latLng);

                var yeri = event.latLng;

                var latlongi = "(" + yeri.lat().toFixed(6) + " , " +
                    +yeri.lng().toFixed(6) + ")";

                infowindow.setContent(latlongi);

                document.getElementById('lat').value = yeri.lat().toFixed(6);
                document.getElementById('lng').value = yeri.lng().toFixed(6);
            });

            autocomplete.addListener('place_changed', function() {

                infowindow.close();
                marker.setVisible(false);
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    // User entered the name of a Place that was not suggested and
                    // pressed the Enter key, or the Place Details request failed.
                    window.alert("No details available for input: '" + place.name + "'");
                    return;
                }

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17); // Why 17? Because it looks good.
                }
                marker.setPosition(place.geometry.location);
                marker.setVisible(true);

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''), (place.address_components[1] && place.address_components[1].short_name || ''), (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                }

                infowindowContent.children['place-icon'].src = place.icon;
                infowindowContent.children['place-name'].textContent = place.name;
                infowindowContent.children['place-address'].textContent = address;

                // mapMaker(address);

                infowindow.open(map, marker);



                var yeri = place.geometry.location;

                var latlongi = "(" + yeri.lat().toFixed(6) + " , " +
                    +yeri.lng().toFixed(6) + ")";

                infowindow.setContent(latlongi);

                document.getElementById('lat').value = yeri.lat().toFixed(6);
                document.getElementById('lng').value = yeri.lng().toFixed(6);

                var address_full = '';
                if (place.address_components) {
                    address_full = [
                        (place.address_components[0] && place.address_components[0].short_name || ''), 
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || ''),
                        (place.address_components[3] && place.address_components[3].short_name || ''),
                        (place.address_components[4] && place.address_components[4].long_name || '')
                    ].join(', ');
                }

                document.getElementById('aciklama').value = address_full;

            });

        }

        function mapMaker(address) {
            geocoder.geocode({
                'address': address,
                'region': 'US'
            }, function(results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                    document.getElementById('lat').value = results[0].geometry.location.lat().toFixed(6);
                    document.getElementById('lng').value = results[0].geometry.location.lng().toFixed(6);
                    var latlong = "(" + results[0].geometry.location.lat().toFixed(6) + " , " +
                        +results[0].geometry.location.lng().toFixed(6) + ")";

                    var adrss = results[0].formatted_address;
                    var lines = adrss.split(',');

                    if (lines.length > 1) {
                        //document.getElementById('adi').value = lines[0];
                        document.getElementById('aciklama').value = results[0].formatted_address;
                        var cnrtsi = lines[lines.length - 1].trim();
                        // console.log(cnrtsi);

                        var dd = document.getElementById('country_id');
                        for (var i = 0; i < dd.options.length; i++) {
                            if (dd.options[i].text === cnrtsi) {
                                dd.selectedIndex = i;
                                // console.log(dd.value);
                                break;
                            }
                        }
                    } else {
                        if (infowindow) {
                            infowindow.close();
                        }

                    }

                } else {
                    alert("Lat and long cannot be found.");
                }
            });
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDefxj8UxYRvKODp9fiErghXY9AEjQb80M&libraries=places&callback=initMap" async defer></script>
</body>

</html>
