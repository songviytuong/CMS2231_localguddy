﻿(function($) {
	// Show Demo
	$(document).ready(function () {

		let instance = $("img.preview-lazy-load").Lazy({
				chainable: false,
				afterLoad: function(el) {
					el.parent('a').addClass('preview-lazy-load-wrap')
				}
			}),
			$showDemoBtn = $('.preview-demo-show');

		$showDemoBtn.on('click', function(event) {
			$('.preview-demo').toggleClass('active');
			instance.loadAll();
			localStorage.setItem('wiloke_listgo_show_preview', 'clicked');
			$("img.preview-lazy-load").addClass('loaded');
		});

		if ( !localStorage.getItem('wiloke_listgo_show_preview') && $(window).width() > 960 ){
			$showDemoBtn.trigger('click');
		}
	});
})(jQuery);

