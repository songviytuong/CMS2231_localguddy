! function() {
    "use strict";
    ! function() {
        function t(t) {
            this.value = t
        }

        function e(e) {
            function i(n, a) {
                try {
                    var r = e[n](a),
                        l = r.value;
                    l instanceof t ? Promise.resolve(l.value).then(function(t) {
                        i("next", t)
                    }, function(t) {
                        i("throw", t)
                    }) : o(r.done ? "return" : "normal", r.value)
                } catch (t) {
                    o("throw", t)
                }
            }

            function o(t, e) {
                switch (t) {
                    case "return":
                        n.resolve({
                            value: e,
                            done: !0
                        });
                        break;
                    case "throw":
                        n.reject(e);
                        break;
                    default:
                        n.resolve({
                            value: e,
                            done: !1
                        })
                }(n = n.next) ? i(n.key, n.arg): a = null
            }
            var n, a;
            this._invoke = function(t, e) {
                return new Promise(function(o, r) {
                    var l = {
                        key: t,
                        arg: e,
                        resolve: o,
                        reject: r,
                        next: null
                    };
                    a ? a = a.next = l : (n = a = l, i(t, e))
                })
            }, "function" != typeof e.return && (this.return = void 0)
        }
        "function" == typeof Symbol && Symbol.asyncIterator && (e.prototype[Symbol.asyncIterator] = function() {
            return this
        }), e.prototype.next = function(t) {
            return this._invoke("next", t)
        }, e.prototype.throw = function(t) {
            return this._invoke("throw", t)
        }, e.prototype.return = function(t) {
            return this._invoke("return", t)
        }
    }();
    var t = function(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function")
        },
        e = function() {
            function t(t, e) {
                for (var i = 0; i < e.length; i++) {
                    var o = e[i];
                    o.enumerable = o.enumerable || !1, o.configurable = !0, "value" in o && (o.writable = !0), Object.defineProperty(t, o.key, o)
                }
            }
            return function(e, i, o) {
                return i && t(e.prototype, i), o && t(e, o), e
            }
        }();
    ! function(i) {
        var o = function() {
                function o() {
                    t(this, o), this.$app = i(".wiloke-twitter-login-js"), this.init()
                }
                return e(o, [{
                    key: "init",
                    value: function() {
                        this.$app.length && this.$app.on("click", function(t) {
                            t.preventDefault();
                            var e = i(this),
                                o = e.data("wiloke_twitter_login_nonce"),
                                n = e.data("redirect"),
                                a = i("body");
                            if (a.trigger("wiloke_login_with_social/connecting", [e]), !0 === e.data("is_ajax")) return a.trigger("wiloke_login_with_social/ajax_completed", [e, response]), !1;
                            e.data("js_ajax", !0), i.ajax({
                                type: "POST",
                                global: !1,
                                data: {
                                    action: "wiloke_twitter_login_request_token",
                                    security: o,
                                    redirect: n
                                },
                                url: WilokeLoginWithSocial.ajaxurl,
                                success: function(t) {
                                    t.success ? (e.data("js_ajax", !1), window.location.href = t.data.url) : e.data("js_ajax", !1), a.trigger("wiloke_login_with_social/ajax_completed", [e, t])
                                }
                            })
                        })
                    }
                }]), o
            }(),
            n = function() {
                function o() {
                    t(this, o), this.$app = i(".wiloke-facebook-login-js"), this.$target = null, this.init()
                }
                return e(o, [{
                    key: "handleResponse",
                    value: function(t) {
                        var e = this.$target;
                        "connected" === t.status ? i.ajax({
                            data: {
                                action: "wiloke_facebook_login",
                                fb_response: t,
                                security: this.$target.data("wiloke_facebook_login_nonce")
                            },
                            global: !1,
                            type: "POST",
                            url: WilokeLoginWithSocial.ajaxurl,
                            success: function(t) {
                                t.success ? window.location.href = t.data.redirect : $body.trigger("wiloke_login_with_social/ajax_completed", [e, t])
                            }
                        }) : ($body.trigger("wiloke_login_with_social/ajax_completed", [e]), navigator.userAgent.match("CriOS") && window.open("https://www.facebook.com/dialog/oauth?client_id=" + e.data("appid") + "&redirect_uri=" + window.location.href + "&scope=email,public_profile", "", null))
                    }
                }, {
                    key: "init",
                    value: function() {
                        var t = this;
                        this.$app.length && (this.$body = i("body"), this.$app.on("click", function(e) {
                            e.preventDefault();
                            var o = i(e.currentTarget);
                            if (t.$target = o, window.fbl_button = o, window.fbl_button.addClass("fbl-loading"), t.$body.trigger("wiloke_login_with_social/connecting", [o]), !0 === o.data("is_ajax")) return t.$body.trigger("wiloke_login_with_social/ajax_completed", [o]), !1;
                            if (o.data("js_ajax", !0), navigator.userAgent.match("CriOS")) i('<p class="fbl_error">' + fbl.l18n.chrome_ios_alert + "</p>").insertAfter(window.fbl_button), FB.getLoginStatus(function(e) {
                                t.handleResponse(e)
                            });
                            else try {
                                FB.login(function(e) {
                                    t.handleResponse(e)
                                }, {
                                    scope: "email,public_profile",
                                    return_scopes: !0,
                                    auth_type: "rerequest"
                                })
                            } catch (e) {
                                t.$body.trigger("wiloke_login_with_social/ajax_completed", [o]), alert("Facebook Init is not loaded. Check that you are not running any blocking software or that you have tracking protection turned off if you use Firefox")
                            }
                        }))
                    }
                }]), o
            }(),
            a = function() {
                function o() {
                    //t(this, o), this.$app = i("#wiloke-login-with-google"), this.appID = "wiloke-login-with-google"
                    t(this, o)
                }
                return e(o, [{
                    key: "processingLoginRegister",
                    value: function(t) {
                        var e = this,
                            o = i("body");
                        this.auth2.attachClickHandler(t, {}, function(t) {
                            o.trigger("wiloke_login_with_social/connecting", [e.$app]), i.ajax({
                                type: "POST",
                                global: !1,
                                url: WilokeLoginWithSocial.ajaxurl,
                                data: {
                                    action: "wiloke_google_login",
                                    security: e.$app.data("wiloke_google_login_nonce"),
                                    client_token: t.getAuthResponse().id_token
                                },
                                success: function(t) {
                                    t.success ? window.location.href = t.data.redirect : o.trigger("wiloke_login_with_social/ajax_completed", [e.$app, t])
                                }
                            })
                        }, function() {
                            o.trigger("wiloke_login_with_social/ajax_completed", [e.$app])
                        })
                    }
                }]), o
            }();
        i(document).ready(function() {
            new o, new n, new a
        })
    }(jQuery)
}();