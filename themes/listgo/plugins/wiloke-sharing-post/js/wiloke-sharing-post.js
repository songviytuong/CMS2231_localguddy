﻿;(function ($) {
	"use strict";
	$(document).ready(function () {
		$('.wiloke-sharing-post-social .copy_link').on('click', function (event) {
			event.preventDefault();
			window.prompt("Press Ctrl+C to copy this link", $(this).data('shortlink'));
		})
	});
})(jQuery);
