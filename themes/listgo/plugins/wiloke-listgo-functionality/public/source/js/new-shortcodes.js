! function() {
    "use strict";
    ! function(e) {
        function t() {
            return (new Date).getTime()
        }
        var i = {
                Models: {},
                Collections: {},
                Views: {}
            },
            a = {
                encode: function(e) {
                    return window.btoa(encodeURIComponent(e))
                },
                decode: function(e) {
                    return decodeURIComponent(window.atob(e))
                }
            },
            n = e(".addlisting-popup__close"),
            l = {
                config: {
                    $wrapper: e("#wiloke-menu-price-settings")
                },
                init: function(t) {
                    e.extend(l.config, t), this.$wrapper = this.config.$wrapper, this.$navWrapper = this.$wrapper.find(".addlisting-popup__nav"), this.$popupPanel = this.$wrapper.find(".addlisting-popup__panel"), this.handleClick(), this.addNewTab(), this.firstTime()
                },
                firstTime: function() {
                    this.$navWrapper.find("a").removeClass("active"), this.$popupPanel.find(".addlisting-popup__group").addClass("hidden"), this.$navWrapper.find("a:first").trigger("click")
                },
                handleClick: function() {
                    var t = this;
                    this.$wrapper.find(".addlisting-popup__nav").on("click", "a", function(i) {
                        i.preventDefault(), t.$navWrapper.find("a").removeClass("active"), e(this).addClass("active"), t.$popupPanel.find(".addlisting-popup__group").addClass("hidden"), e(e(this).attr("href")).removeClass("hidden")
                    })
                },
                addNewTab: function() {
                    var e = this;
                    this.$wrapper.on("addedNew", function() {
                        e.$wrapper.find(".addlisting-popup__nav").find("a:last").trigger("click")
                    })
                }
            };
        i.emulateSC = {
            priceTable: '<div id="{{idhere}}" data-title="{{titlehere}}" class="addlisting-placeholder addlisting-placeholder-prices" data-settings="{{valuehere}}" draggable="true" contenteditable="false"><div class="addlisting-placeholder__icon" style="background-image: url(' + WILOKE_LISTGO_FUNCTIONALITY.url + 'public/source/img/icon-price.png)"></div><div class="addlisting-placeholder__title">{{titlehere}}</div><div class="addlisting-placeholder__actions"><span data-id="{{idhere}}" class="addlisting-placeholder__action-edit wiloke-edit-menu-prices">' + WILOKE_LISTGO_SC_TRANSLATION.edit + '</span><span class="addlisting-placeholder__action-remove">' + WILOKE_LISTGO_SC_TRANSLATION.remove + "</span></div></div>",
            accordion: '<div id="{{idhere}}" data-title="{{titlehere}}" class="addlisting-placeholder addlisting-placeholder-accordion" data-settings="{{valuehere}}" draggable="true" contenteditable="false"><div class="addlisting-placeholder__icon" style="background-image: url(' + WILOKE_LISTGO_FUNCTIONALITY.url + 'public/source/img/icon-accordion.png)"></div><div class="addlisting-placeholder__title">{{titlehere}}</div><div class="addlisting-placeholder__actions"><span class="addlisting-placeholder__action-edit wiloke-edit-accordion" data-id="{{idhere}}">' + WILOKE_LISTGO_SC_TRANSLATION.edit + '</span><span class="addlisting-placeholder__action-remove">' + WILOKE_LISTGO_SC_TRANSLATION.remove + "</span></div></div>",
            listFeatures: '<div id="{{idhere}}" data-title="{{titlehere}}" class="addlisting-placeholder addlisting-placeholder-list-features" data-settings="{{valuehere}}" draggable="true" contenteditable="false"><div class="addlisting-placeholder__icon" style="background-image: url(' + WILOKE_LISTGO_FUNCTIONALITY.url + 'public/source/img/icon-list.png)"></div><div class="addlisting-placeholder__title">{{titlehere}}</div><div class="addlisting-placeholder__actions"><span class="addlisting-placeholder__action-edit wiloke-edit-list-features" data-id="{{idhere}}">' + WILOKE_LISTGO_SC_TRANSLATION.edit + '</span><span class="addlisting-placeholder__action-remove">' + WILOKE_LISTGO_SC_TRANSLATION.remove + "</span></div></div>"
        }, i.Models.priceTable = Backbone.Model.extend({
            defaults: {
                name: "Name",
                price: "5$",
                description: "Write something describes this food"
            }
        }), i.Collections.priceTable = Backbone.Collection.extend({
            model: i.Models.priceTable,
            initialize: function() {
                this.on("add", function(e) {}), this.on("change", function() {})
            }
        }), i.Views.priceTable = Backbone.View.extend({
            el: "#wiloke-menu-price-settings",
            events: {
                "click .addlisting-popup__plus": "addOne",
                "click .addlisting-popup__nav-remove": "removeModel"
            },
            tagName: "div",
            loadIntro: function() {
                var e = this.$el.find(".wiloke-sc-intro");
                e.hasClass("loaded") && e.attr("src", e.data("src"))
            },
            removeModel: function(t) {
                t.preventDefault(), this.collection.length > 1 ? (this.collection.remove(e(t.target).data("id")), l.init({
                    $wrapper: this.$el
                })) : alert("You need at least one item")
            },
            initialize: function() {
                this.listenTo(this.collection, "add", this.render), this.listenTo(this.collection, "remove", this.render), this.loadIntro()
            },
            addOne: function(t) {
                t.preventDefault();
                var a = new i.Models.priceTable;
                this.collection.add(a);
                var n = new i.Views.priceItem({
                    model: a
                });
                this.$el.find(".addlisting-popup__panel").append(n.render().el);
                var l = new i.Views.priceTab({
                    model: a
                });
                e(t.target).before(l.render().el), this.$el.find(".addlisting-popup__panel").append(n.render().el), this.$el.trigger("addedNew")
            },
            render: function() {
                var e = 0;
                this.$el.find(".addlisting-popup__panel").empty(), this.$el.find(".addlisting-popup__nav").empty(), this.collection.each(function(t) {
                    var a = new i.Views.priceTab({
                        model: t
                    });
                    this.$el.find(".addlisting-popup__nav").append(a.render(e).el), e++
                }, this), this.$el.find(".addlisting-popup__nav").append('<span class="addlisting-popup__plus">+</span>'), this.collection.each(function(e) {
                    var t = new i.Views.priceItem({
                        model: e
                    });
                    this.$el.find(".addlisting-popup__panel").append(t.render().el)
                }, this)
            }
        }), i.Views.priceItem = Backbone.View.extend({
            tagName: "div",
            className: "addlisting-popup__group",
            events: {
                "keypress input": "changedValue",
                "paste input": "changedValue",
                "cut input": "changedValue",
                "keypress textarea": "changedValue",
                "cut textarea": "changedValue",
                "paste textarea": "changedValue"
            },
            initialize: function() {
                this.handling = null
            },
            template: _.template(e("#wiloke-price-item").html()),
            changedValue: function(t) {
                null !== this.handling && clearTimeout(this.handling);
                var i = this,
                    a = e(t.target);
                this.handling = setTimeout(function() {
                    i.model.set(a.attr("name"), a.val()), clearTimeout(i.handling)
                }, 500)
            },
            render: function() {
                return this.$el.html(this.template(this.model.toJSON())), this.$el.attr("id", this.model.cid), this
            }
        }), i.Views.priceTab = Backbone.View.extend({
            tagName: "a",
            className: "",
            template: _.template('<span class="title"><%= name %></span><span class="addlisting-popup__nav-remove" data-id="<%= id %>"></span>'),
            initialize: function() {
                this.activateClass = !1, this.listenTo(this.model, "change:name", this.updateName)
            },
            updateName: function() {
                this.$el.find(".title").html(this.model.get("name"))
            },
            render: function(e) {
                this.className = 0 === e ? "active" : "";
                var t = this.model.toJSON();
                return t.id = this.model.cid, this.$el.html(this.template(t)), this.$el.attr("href", "#" + this.model.cid), this
            }
        }), i.Models.priceTitle = Backbone.Model.extend({
            defaults: {
                title: "Menu Price"
            }
        }), i.Views.priceTitle = Backbone.View.extend({
            el: "#price-title-wrapper",
            tag: "div",
            template: _.template(e("#price-title-tpl").html()),
            events: {
                "change #price-title": "changedTitle",
                "cut #price-title": "changedTitle",
                "paste #price-title": "changedTitle",
                "keypress #price-title": "changedTitle"
            },
            initialize: function() {
                this.handling = null, this.render()
            },
            changedTitle: function(t) {
                null !== this.handling && clearTimeout(this.handling);
                var i = this;
                this.handling = setTimeout(function() {
                    i.model.set("title", e(t.currentTarget).val()), i.updateEmulateTitle(), clearTimeout(i.handling)
                }, 400)
            },
            updateEmulateTitle: function() {
                e("#show-price-title").html(this.model.get("title"))
            },
            render: function() {
                return this.$el.html(this.template(this.model.toJSON())), this.updateEmulateTitle(), this
            }
        }), i.Models.accordionTitle = Backbone.Model.extend({
            defaults: {
                title: ""
            }
        }), i.Views.accordionTitle = Backbone.View.extend({
            el: "#accordion-title-wrapper",
            tag: "div",
            template: _.template(e("#accordion-title-tpl").html()),
            events: {
                "change #accordion-title": "changedTitle",
                "cut #accordion-title": "changedTitle",
                "paste #accordion-title": "changedTitle",
                "keypress #accordion-title": "changedTitle"
            },
            initialize: function() {
                this.handling = null, this.render()
            },
            changedTitle: function(t) {
                null !== this.handling && clearTimeout(this.handling);
                var i = this;
                this.handling = setTimeout(function() {
                    i.model.set("title", e(t.currentTarget).val()), i.updateEmulateTitle(), clearTimeout(i.handling)
                }, 400)
            },
            updateEmulateTitle: function() {
                e("#show-accordion-title").html(this.model.get("title"))
            },
            render: function() {
                return this.$el.html(this.template(this.model.toJSON())), this.updateEmulateTitle(), this
            }
        }), i.Models.accordion = Backbone.Model.extend({
            defaults: {
                title: WILOKE_LISTGO_SC_TRANSLATION.accordion_title,
                description: WILOKE_LISTGO_SC_TRANSLATION.accordion_desc
            }
        }), i.Collections.accordion = Backbone.Collection.extend({
            model: i.Models.accordion,
            initialize: function() {
                this.on("add", function(e) {}), this.on("change", function() {})
            }
        }), i.Views.accordion = Backbone.View.extend({
            el: "#wiloke-accordion-settings",
            events: {
                "click .addlisting-popup__plus": "addOne",
                "click .addlisting-popup__nav-remove": "removeModel"
            },
            tagName: "div",
            loadIntro: function() {
                var e = this.$el.find(".wiloke-sc-intro");
                e.hasClass("loaded") && e.attr("src", e.data("src"))
            },
            removeModel: function(t) {
                t.preventDefault(), this.collection.length > 1 ? (this.collection.remove(e(t.target).data("id")), l.init({
                    $wrapper: e(this.el)
                })) : alert("You need at least one item")
            },
            initialize: function() {
                this.listenTo(this.collection, "add", this.render), this.listenTo(this.collection, "remove", this.render), this.loadIntro()
            },
            addOne: function(t) {
                t.preventDefault();
                var a = new i.Models.accordion;
                this.collection.add(a);
                var n = new i.Views.accordionItem({
                    model: a
                });
                this.$el.find(".addlisting-popup__panel").append(n.render().el);
                var l = new i.Views.accordionTab({
                    model: a
                });
                e(t.target).before(l.render().el), this.$el.find(".addlisting-popup__panel").append(n.render().el), this.$el.trigger("addedNew")
            },
            render: function() {
                var e = 0;
                this.$el.find(".addlisting-popup__panel").empty(), this.$el.find(".addlisting-popup__nav").empty(), this.collection.each(function(t) {
                    var a = new i.Views.accordionTab({
                        model: t
                    });
                    this.$el.find(".addlisting-popup__nav").append(a.render(e).el), e++
                }, this), this.$el.find(".addlisting-popup__nav").append('<span class="addlisting-popup__plus">+</span>'), this.collection.each(function(e) {
                    var t = new i.Views.accordionItem({
                        model: e
                    });
                    this.$el.find(".addlisting-popup__panel").append(t.render().el)
                }, this)
            }
        }), i.Views.accordionItem = Backbone.View.extend({
            tagName: "div",
            className: "addlisting-popup__group",
            events: {
                "keypress input": "changedValue",
                "cut input": "changedValue",
                "paste input": "changedValue",
                "keypress textarea": "changedValue",
                "cut textarea": "changedValue",
                "paste textarea": "changedValue"
            },
            initialize: function() {
                this.handling = null
            },
            template: _.template(e("#wiloke-accordion-item").html()),
            changedValue: function(t) {
                null !== this.handling && clearTimeout(this.handling);
                var i = this,
                    a = e(t.target);
                this.handling = setTimeout(function() {
                    i.model.set(a.attr("name"), a.val()), clearTimeout(i.handling)
                }, 500)
            },
            render: function() {
                return this.$el.html(this.template(this.model.toJSON())), this.$el.attr("id", this.model.cid), this
            }
        }), i.Views.accordionTab = Backbone.View.extend({
            tagName: "a",
            className: "",
            template: _.template('<span class="title"><%= title %></span><span class="addlisting-popup__nav-remove" data-id="<%= id %>"></span>'),
            initialize: function() {
                this.activateClass = !1, this.listenTo(this.model, "change:title", this.updateName)
            },
            updateName: function() {
                return this.$el.find(".title").html(this.model.get("title")), this
            },
            render: function(e) {
                this.className = 0 === e ? "active" : "";
                var t = this.model.toJSON();
                return t.id = this.model.cid, this.$el.html(this.template(t)), this.$el.attr("href", "#" + this.model.cid), this
            }
        }), i.Models.listFeatures = Backbone.Model.extend({
            defaults: {
                name: "Wifi",
                unavailable: ""
            }
        }), i.Collections.listFeatures = Backbone.Collection.extend({
            model: i.Models.listFeatures,
            initialize: function() {
                this.on("add", function(e) {}), this.on("change", function() {})
            }
        }), i.Views.listFeatures = Backbone.View.extend({
            el: "#wiloke-list-features-settings",
            events: {
                "click .addlisting-popup__plus": "addOne",
                "click .addlisting-popup__nav-remove": "removeModel"
            },
            tagName: "div",
            loadIntro: function() {
                var e = this.$el.find(".wiloke-sc-intro");
                e.hasClass("loaded") && e.attr("src", e.data("src"))
            },
            removeModel: function(t) {
                t.preventDefault(), this.collection.length > 1 ? (this.collection.remove(e(t.target).data("id")), l.init({
                    $wrapper: this.$wrapper
                })) : alert("You need at least one item")
            },
            initialize: function() {
                this.listenTo(this.collection, "add", this.render), this.listenTo(this.collection, "remove", this.render), this.loadIntro()
            },
            addOne: function(t) {
                t.preventDefault();
                var a = new i.Models.listFeatures;
                this.collection.add(a);
                var n = new i.Views.listFeatureItem({
                    model: a
                });
                this.$el.find(".addlisting-popup__panel").append(n.render().el);
                var l = new i.Views.listFeatureTab({
                    model: a
                });
                e(t.target).before(l.render().el), this.$el.find(".addlisting-popup__panel").append(n.render().el), this.$el.trigger("addedNew")
            },
            render: function() {
                var e = 0;
                this.$el.find(".addlisting-popup__panel").empty(), this.$el.find(".addlisting-popup__nav").empty(), this.collection.each(function(t) {
                    var a = new i.Views.listFeatureTab({
                        model: t
                    });
                    this.$el.find(".addlisting-popup__nav").append(a.render(e).el), e++
                }, this), this.$el.find(".addlisting-popup__nav").append('<span class="addlisting-popup__plus">+</span>'), this.collection.each(function(e) {
                    var t = new i.Views.listFeatureItem({
                        model: e
                    });
                    this.$el.find(".addlisting-popup__panel").append(t.render().el)
                }, this)
            }
        }), i.Views.listFeatureItem = Backbone.View.extend({
            tagName: "div",
            className: "addlisting-popup__group",
            events: {
                "keypress input": "changedValue",
                "cut input": "changedValue",
                "paste input": "changedValue",
                "change #feature-unavailable": "changedValue"
            },
            initialize: function() {
                this.handling = null
            },
            template: _.template(e("#wiloke-list-feature-item").html()),
            changedValue: function(t) {
                null !== this.handling && clearTimeout(this.handling);
                var i = this,
                    a = e(t.target);
                this.handling = setTimeout(function() {
                    "checkbox" === a.attr("type") ? a.is(":checked") ? i.model.set(a.attr("name"), "yes") : i.model.set(a.attr("name"), "") : i.model.set(a.attr("name"), a.val()), clearTimeout(i.handling)
                }, 500)
            },
            render: function() {
                return this.$el.html(this.template(this.model.toJSON())), this.$el.attr("id", this.model.cid), this
            }
        }), i.Views.listFeatureTab = Backbone.View.extend({
            tagName: "a",
            className: "",
            template: _.template('<span class="title"><%= name %></span><span class="addlisting-popup__nav-remove" data-id=<%= id %>></span>'),
            initialize: function() {
                this.activateClass = !1, this.listenTo(this.model, "change:name", this.updateName)
            },
            updateName: function() {
                this.$el.find(".title").html(this.model.get("name"))
            },
            render: function(e) {
                this.className = 0 === e ? "active" : "";
                var t = this.model.toJSON();
                return t.id = this.model.cid, this.$el.html(this.template(t)), this.$el.attr("href", "#" + this.model.cid), this
            }
        }), i.Models.listFeatureTitle = Backbone.Model.extend({
            defaults: {
                title: ""
            }
        }), i.Views.listFeatureTitle = Backbone.View.extend({
            el: "#list-features-title-wrapper",
            tag: "div",
            template: _.template(e("#list-features-title-tpl").html()),
            initialize: function() {
                this.handling = null, this.render()
            },
            events: {
                "change #list-features-title": "changedTitle",
                "cut #list-features-title": "changedTitle",
                "paste #list-features-title": "changedTitle",
                "keypress #list-features-title": "changedTitle"
            },
            changedTitle: function(t) {
                null !== this.handling && clearTimeout(this.handling);
                var i = this;
                this.handling = setTimeout(function() {
                    i.model.set("title", e(t.currentTarget).val()), i.updateEmulateTitle(), clearTimeout(i.handling)
                }, 400)
            },
            updateEmulateTitle: function() {
                e("#show-list-features-title").html(this.model.get("title"))
            },
            render: function() {
                return this.$el.html(this.template(this.model.toJSON())), this.updateEmulateTitle(), this
            }
        });
        var d = new i.Models.priceTitle,
            s = new i.Views.priceTitle({
                model: d
            }),
            o = new i.Collections.priceTable,
            r = (new i.Views.priceTable({
                collection: o
            }), new i.Models.accordionTitle),
            c = new i.Views.accordionTitle({
                model: r
            }),
            p = new i.Collections.accordion,
            u = (new i.Views.accordion({
                collection: p
            }), new i.Models.listFeatureTitle),
            h = new i.Views.listFeatureTitle({
                model: u
            }),
            g = new i.Collections.listFeatures;
        new i.Views.listFeatures({
            collection: g
        });
        tinymce.create("tinymce.plugins.WilokeListGoNewShortcodes", {
            init: function(_, m) {
                _.addButton("listgo_new_accordions", {
                    title: WILOKE_LISTGO_SC_TRANSLATION.accordion_btn,
                    image: WILOKE_LISTGO_FUNCTIONALITY.url + "public/source/img/icon-accordion.png",
                    onclick: function() {
                        if ("undefined" != typeof WILOKE_GLOBAL && "disable" === WILOKE_GLOBAL.toggleListingShortcodes) return alert(WILOKE_LISTGO_SC_TRANSLATION.needupdate), !1;
                        r.set("title", WILOKE_LISTGO_SC_TRANSLATION.title), c.render(), p.reset(), p.add({
                            title: WILOKE_LISTGO_SC_TRANSLATION.accordion_title,
                            description: WILOKE_LISTGO_SC_TRANSLATION.accordion_desc
                        });
                        var d = e("#wiloke-accordion-settings");
                        d.removeClass("hidden"), e("#save-accordion").off().on("click", function(e) {
                            var l = p.toJSON(),
                                d = r.get("title"),
                                s = i.emulateSC.accordion.replace("{{valuehere}}", a.encode(JSON.stringify(l)));
                            s = (s = s.replace(/{{idhere}}/g, t())).replace(/{{titlehere}}/g, d), _.execCommand("mceInsertContent", 0, s + "&nbsp;"), n.trigger("click")
                        }), l.init({
                            $wrapper: d
                        })
                    }
                }), _.addButton("listgo_new_list_features", {
                    title: WILOKE_LISTGO_SC_TRANSLATION.list_features_btn,
                    image: WILOKE_LISTGO_FUNCTIONALITY.url + "public/source/img/icon-list.png",
                    onclick: function() {
                        if ("undefined" != typeof WILOKE_GLOBAL && "disable" === WILOKE_GLOBAL.toggleListingShortcodes) return alert(WILOKE_LISTGO_SC_TRANSLATION.needupdate), !1;
                        u.set("title", WILOKE_LISTGO_SC_TRANSLATION.title), h.render(), g.reset(), g.add({
                            name: "Wifi",
                            unavailable: ""
                        });
                        var d = e("#wiloke-list-features-settings");
                        d.removeClass("hidden"), e("#save-list-features").off().on("click", function(e) {
                            var l = g.toJSON(),
                                d = u.get("title"),
                                s = i.emulateSC.listFeatures.replace("{{valuehere}}", a.encode(JSON.stringify(l)));
                            s = (s = s.replace(/{{idhere}}/g, t())).replace(/{{titlehere}}/g, d), _.execCommand("mceInsertContent", 0, s + "&nbsp;"), n.trigger("click")
                        }), l.init({
                            $wrapper: d
                        })
                    }
                }), _.addButton("listgo_new_menu_prices", {
                    title: WILOKE_LISTGO_SC_TRANSLATION.menu_price_btn,
                    image: WILOKE_LISTGO_FUNCTIONALITY.url + "public/source/img/icon-price.png",
                    onclick: function() {
                        if ("undefined" != typeof WILOKE_GLOBAL && "disable" === WILOKE_GLOBAL.toggleListingShortcodes) return alert(WILOKE_LISTGO_SC_TRANSLATION.needupdate), !1;
                        d.set("title", WILOKE_LISTGO_SC_TRANSLATION.title), s.render(), o.reset(), o.add({
                            name: WILOKE_LISTGO_SC_TRANSLATION.price_name,
                            price: WILOKE_LISTGO_SC_TRANSLATION.price_cost,
                            description: WILOKE_LISTGO_SC_TRANSLATION.price_desc
                        });
                        var r = e("#wiloke-menu-price-settings");
                        r.removeClass("hidden"), e("#save-price-table").off().on("click", function(e) {
                            var l = o.toJSON(),
                                s = d.get("title"),
                                r = i.emulateSC.priceTable.replace("{{valuehere}}", a.encode(JSON.stringify(l)));
                            r = (r = r.replace(/{{idhere}}/g, t())).replace(/{{titlehere}}/g, s), _.execCommand("mceInsertContent", 0, r + "&nbsp;"), n.trigger("click")
                        }), l.init({
                            $wrapper: r
                        })
                    }
                }), _.onPostProcess.add(function(e, t) {
                    if (t.get) {
                        var i = new RegExp('(?:<div) (id="(?:[^"]*)" class="addlisting-placeholder addlisting-placeholder-prices"(?:[^>]*))>(.+?)(?=(</span></div></div>))</span></div></div>', "g");
                        t.content = t.content.replace(i, function(e, t) {
                            return "[wiloke_price_table " + (t = t.trim()) + " /]"
                        });
                        var a = new RegExp('(?:<div) (id="(?:[^"]*)" class="addlisting-placeholder addlisting-placeholder-accordion"(?:[^>]*))>(.+?)(?=(</span></div></div>))</span></div></div>', "g");
                        t.content = t.content.replace(a, function(e, t) {
                            return "[wiloke_accordion " + (t = t.trim()) + " /]"
                        });
                        var n = new RegExp('(?:<div) (id="(?:[^"]*)" class="addlisting-placeholder addlisting-placeholder-list-features"(?:[^>]*))>(.+?)(?=(</span></div></div>))</span></div></div>', "g");
                        t.content = t.content.replace(n, function(e, t) {
                            return "[wiloke_list_features " + (t = t.trim()) + " /]"
                        })
                    }
                }), _.onBeforeSetContent.add(function(e, t) {
                    t.content = t.content.replace(/\[wiloke_price_table id="([^"]*)"([^\/\]]*)\/\]/g, function(e, t, i) {
                        var a = (i = i.trim()).match(/(?:data-title=")([^\"]*)(?:")/);
                        return '<div id="' + t + '" ' + i + ' draggable="true"><div class="addlisting-placeholder__icon" style="background-image: url(' + WILOKE_LISTGO_FUNCTIONALITY.url + 'public/source/img/icon-price.png)"></div><div class="addlisting-placeholder__title">' + a[1] + '</div><div class="addlisting-placeholder__actions"><span data-id="' + t + '" class="addlisting-placeholder__action-edit wiloke-edit-menu-prices">' + WILOKE_LISTGO_SC_TRANSLATION.edit + '</span><span class="addlisting-placeholder__action-remove">' + WILOKE_LISTGO_SC_TRANSLATION.remove + "</span></div></div>"
                    }), t.content = t.content.replace(/\[wiloke_accordion id="([^"]*)"([^\/\]]*)\/\]/g, function(e, t, i) {
                        var a = (i = i.trim()).match(/(?:data-title=")([^\"]*)(?:")/);
                        return '<div id="' + t + '" ' + i + ' draggable="true"><div class="addlisting-placeholder__icon" style="background-image: url(' + WILOKE_LISTGO_FUNCTIONALITY.url + 'public/source/img/icon-accordion.png)"></div><div class="addlisting-placeholder__title">' + a[1] + '</div><div class="addlisting-placeholder__actions"><span data-id="' + t + '" class="addlisting-placeholder__action-edit wiloke-edit-accordion">' + WILOKE_LISTGO_SC_TRANSLATION.edit + '</span><span class="addlisting-placeholder__action-remove">' + WILOKE_LISTGO_SC_TRANSLATION.remove + "</span></div></div>"
                    }), t.content = t.content.replace(/\[wiloke_list_features id="([^"]*)"([^\/\]]*)\/\]/g, function(e, t, i) {
                        var a = (i = i.trim()).match(/(?:data-title=")([^\"]*)(?:")/);
                        return '<div id="' + t + '" ' + i + ' draggable="true"><div class="addlisting-placeholder__icon" style="background-image: url(' + WILOKE_LISTGO_FUNCTIONALITY.url + 'public/source/img/icon-list.png)"></div><div class="addlisting-placeholder__title">' + a[1] + '</div><div class="addlisting-placeholder__actions"><span data-id="' + t + '" class="addlisting-placeholder__action-edit wiloke-edit-list-features">' + WILOKE_LISTGO_SC_TRANSLATION.edit + '</span><span class="addlisting-placeholder__action-remove">' + WILOKE_LISTGO_SC_TRANSLATION.remove + "</span></div></div>"
                    })
                }), _.onInit.add(function(m) {
                    _.on("mousedown", function(m) {
                        var v = e(m.target);
                        if (v.hasClass("wiloke-edit-menu-prices")) {
                            var f = v.data("id"),
                                T = v.closest("#" + f),
                                O = T.data("title"),
                                L = T.data("settings");
                            d.set("title", O), s.render(), L = a.decode(L), o.reset(), L = "" !== L ? JSON.parse(L) : L, o.add(L);
                            var I = e("#wiloke-menu-price-settings");
                            I.removeClass("hidden"), e("#save-price-table").off().on("click", function(e) {
                                if ("undefined" != typeof WILOKE_GLOBAL && "disable" === WILOKE_GLOBAL.toggleListingShortcodes) return n.trigger("click"), !1;
                                var l = "wiloke_price_table id=('|\")" + f + "('|\")([^\\]]*)",
                                    s = o.toJSON(),
                                    r = _.getContent(),
                                    c = new RegExp("\\[" + l + "\\]", "g"),
                                    p = r.replace(c, function(e) {
                                        var n = i.emulateSC.priceTable.replace("{{valuehere}}", a.encode(JSON.stringify(s)));
                                        return n = n.replace(/{{idhere}}/g, t()), n = n.replace(/{{titlehere}}/g, d.get("title"))
                                    });
                                _.setContent(p), n.trigger("click")
                            }), l.init({
                                $wrapper: I
                            })
                        }
                        if (v.hasClass("wiloke-edit-accordion")) {
                            var w = v.data("id"),
                                S = v.closest("#" + w),
                                N = S.data("title"),
                                b = S.data("settings");
                            b = a.decode(b), r.set("title", N), c.render(), p.reset(), b = "" !== b ? JSON.parse(b) : b, p.add(b);
                            var k = e("#wiloke-accordion-settings");
                            k.removeClass("hidden"), e("#save-accordion").off().on("click", function(e) {
                                if ("undefined" != typeof WILOKE_GLOBAL && "disable" === WILOKE_GLOBAL.toggleListingShortcodes) return n.trigger("click"), !1;
                                var l = "wiloke_accordion id=('|\")" + w + "('|\")([^\\]]*)",
                                    d = p.toJSON(),
                                    s = _.getContent(),
                                    o = new RegExp("\\[" + l + "\\]", "g"),
                                    c = s.replace(o, function(e) {
                                        var n = i.emulateSC.accordion.replace("{{valuehere}}", a.encode(JSON.stringify(d)));
                                        return n = n.replace(/{{idhere}}/g, t()), n = n.replace(/{{titlehere}}/g, r.get("title"))
                                    });
                                _.setContent(c), n.trigger("click")
                            }), l.init({
                                $wrapper: k
                            })
                        }
                        if (v.hasClass("wiloke-edit-list-features")) {
                            var C = v.data("id"),
                                A = v.closest("#" + C),
                                $ = A.data("title"),
                                E = A.data("settings");
                            E = a.decode(E), u.set("title", $), h.render(), g.reset(), E = "" !== E ? JSON.parse(E) : E, g.add(E);
                            var W = e("#wiloke-list-features-settings");
                            W.removeClass("hidden"), e("#save-list-features").off().on("click", function(e) {
                                if ("undefined" != typeof WILOKE_GLOBAL && "disable" === WILOKE_GLOBAL.toggleListingShortcodes) return n.trigger("click"), !1;
                                var l = "wiloke_list_features id=('|\")" + C + "('|\")([^\\]]*)",
                                    d = g.toJSON(),
                                    s = _.getContent(),
                                    o = new RegExp("\\[" + l + "\\]", "g"),
                                    r = s.replace(o, function(e) {
                                        var n = i.emulateSC.listFeatures.replace("{{valuehere}}", a.encode(JSON.stringify(d)));
                                        return n = n.replace(/{{idhere}}/g, t()), n = n.replace(/{{titlehere}}/g, u.get("title"))
                                    });
                                _.setContent(r), n.trigger("click")
                            }), l.init({
                                $wrapper: W
                            })
                        }
                        v.hasClass("addlisting-placeholder__action-remove") && v.closest(".addlisting-placeholder").remove()
                    })
                })
            },
            createControl: function(e, t) {
                return null
            } //listgo_new_shortcodes
        }), tinymce.PluginManager.add("listgo_new_shortcode_s", tinymce.plugins.WilokeListGoNewShortcodes), e(document).ready(function() {
            e(".addlisting-popup__close, .cancel-shortcode").on("click", function(t) {
                t.preventDefault(), e(this).closest(".addlisting-popup-wrap").addClass("hidden")
            })
        }), e(window).load(function() {
            e(".wiloke-sc-intro").each(function() {
                e(this).attr("src", e(this).data("src")), e(this).addClass("loaded")
            });
            var t = e('.mce-widget[aria-label="Toolbar Toggle"]'),
                i = e("#insert-media-button"),
                a = t.attr("aria-pressed");
            t.on("click", function(e) {
                "false" === a || void 0 === a ? i.addClass("hidden") : i.removeClass("hidden")
            }), "undefined" != typeof WILOKE_GLOBAL && "disable" === WILOKE_GLOBAL.toggleListingShortcodes && (e('.mce-widget[aria-label="' + WILOKE_LISTGO_SC_TRANSLATION.menu_price_btn + '"]').addClass("disable"), e('.mce-widget[aria-label="' + WILOKE_LISTGO_SC_TRANSLATION.list_features_btn + '"]').addClass("disable"), e('.mce-widget[aria-label="' + WILOKE_LISTGO_SC_TRANSLATION.accordion_btn + '"]').addClass("disable"))
        })
    }(jQuery)
}();