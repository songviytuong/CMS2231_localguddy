! function() {
    "use strict";
    ! function() {
        function e(e) {
            this.value = e
        }

        function i(i) {
            function t(r, n) {
                try {
                    var s = i[r](n),
                        l = s.value;
                    l instanceof e ? Promise.resolve(l.value).then(function(e) {
                        t("next", e)
                    }, function(e) {
                        t("throw", e)
                    }) : a(s.done ? "return" : "normal", s.value)
                } catch (e) {
                    a("throw", e)
                }
            }

            function a(e, i) {
                switch (e) {
                    case "return":
                        r.resolve({
                            value: i,
                            done: !0
                        });
                        break;
                    case "throw":
                        r.reject(i);
                        break;
                    default:
                        r.resolve({
                            value: i,
                            done: !1
                        })
                }(r = r.next) ? t(r.key, r.arg): n = null
            }
            var r, n;
            this._invoke = function(e, i) {
                return new Promise(function(a, s) {
                    var l = {
                        key: e,
                        arg: i,
                        resolve: a,
                        reject: s,
                        next: null
                    };
                    n ? n = n.next = l : (r = n = l, t(e, i))
                })
            }, "function" != typeof i.return && (this.return = void 0)
        }
        "function" == typeof Symbol && Symbol.asyncIterator && (i.prototype[Symbol.asyncIterator] = function() {
            return this
        }), i.prototype.next = function(e) {
            return this._invoke("next", e)
        }, i.prototype.throw = function(e) {
            return this._invoke("throw", e)
        }, i.prototype.return = function(e) {
            return this._invoke("return", e)
        }
    }();
    var e = function(e, i) {
            if (!(e instanceof i)) throw new TypeError("Cannot call a class as a function")
        },
        i = function() {
            function e(e, i) {
                for (var t = 0; t < i.length; t++) {
                    var a = i[t];
                    a.enumerable = a.enumerable || !1, a.configurable = !0, "value" in a && (a.writable = !0), Object.defineProperty(e, a.key, a)
                }
            }
            return function(i, t, a) {
                return t && e(i.prototype, t), a && e(i, a), i
            }
        }();
    ! function(t) {
        function a() {
            var e = t(".listgo-add-extra-hour"),
                i = t(".listgo-remove-extra-hour");
            e.on("click", function(e) {
                var i = t(this).data("key");
                t(".listgo-extra-hour-wrapper-" + i).removeClass("hidden"), t(this).addClass("visible-hidden")
            }), i.on("click", function(e) {
                var i = t(this).data("key"),
                    a = t(".listgo-extra-hour-wrapper-" + i);
                a.addClass("hidden"), a.find("input").attr("value", ""), t('.listgo-add-extra-hour[data-key="' + i + '"]').removeClass("visible-hidden")
            })
        }
        var r = function() {
                function a() {
                    e(this, a), this.$form = t("#form-submit-listing"), this.$submit = t("#wiloke-listgo-submit-listing"), this.$editListing = t("#wiloke-listgo-update-listing"), this.$acfWrapper = this.$form.find(".acf_postbox").length ? this.$form.find(".acf_postbox") : this.$form.find(".acf-fields"), this.acfData = "", this.xhrPreview = null, this.previewListing(), this.submitListing(), this.removeGallery(), this.mediaUpload(), this.anonymousACFUpload(), this.anonymousUploadFeaturedImg(), this.anonymousUploadGalleryImg(), this.toggleBusinessHours(), this.editListing(), this.solvedAFCPostID()
                }
                return i(a, [{
                    key: "solvedAFCPostID",
                    value: function() {
                        var e = this;
                        t(window).on("load", function() {
                            if ("undefined" != typeof acf) {
                                var i = e.$form.find('[name="post_id"]'),
                                    t = e.$form.find('[name="_acf_post_id"]'),
                                    a = e.$form.find('[name="listing_id"]');
                                t.length ? (t.val(a.val()), acf.o.post_id = a.val()) : i.length && (i.val(a.val()), acf.o.post_id = a.val())
                            }
                        })
                    }
                }, {
                    key: "toggleBusinessHours",
                    value: function() {
                        var e = t("#wiloke-toggle-business-hours"),
                            i = t("#wiloke-tbl-business-hours");
                        e.on("change", function() {
                            "enable" === t(this).val() ? i.fadeIn("slow") : i.fadeOut("slow")
                        }).trigger("change")
                    }
                }, {
                    key: "getContent",
                    value: function() {
                        return t("#wp-listing_content-wrap").hasClass("tmce-active") ? tinyMCE.get("listing_content").getContent() : t("#listing_content").val()
                    }
                }, {
                    key: "getWYSIWYGContent",
                    value: function() {
                        var e = t(".wysiwyg-wrapper"),
                            i = {};
                        return e.length && e.each(function() {
                            var e = t(this).data("contentid"),
                                a = "";
                            a = t("#" + e).val(), i[e] = a
                        }), i
                    }
                }, {
                    key: "anonymousACFUpload",
                    value: function() {
                        var e = null,
                            i = this;
                        t("body").on("processAFCUploads", function() {
                            var a = i.$form.find(".acf-field-image"),
                                r = a.length,
                                n = 1;
                            a.each(function() {
                                var i = t(this),
                                    s = i.data("key");
                                if (!i.val().length || a.find('input[type="hidden"]') && !a.find('input[type="hidden"]').val().length) return r == n && t("body").trigger("processUploadGallery"), !0;
                                i.wrap('<form id="wiloke-create-form-submission" enctype="multipart/form-data"></form>');
                                var l = document.getElementById("wiloke-create-form-submission"),
                                    o = new FormData(l);
                                e = t.ajax({
                                    url: WILOKE_GLOBAL.ajaxurl + "?type=afc_single&action=wiloke_submission_insert_media&where=acf-form-data&name=" + s,
                                    type: "POST",
                                    async: !0,
                                    cache: !1,
                                    contentType: !1,
                                    processData: !1,
                                    data: o,
                                    success: function(e) {
                                        e.success && (i.find('input[type="hidden"]').val(e.data.message), i.find('[name="acf[' + s + ']"]:last').remove()), i.unwrap(), r == n && t("body").trigger("processUploadGallery")
                                    }
                                }), n++
                            })
                        })
                    }
                }, {
                    key: "anonymousUploadFeaturedImg",
                    value: function() {
                        var e = null,
                            i = t("body");
                        i.on("processUploadFeaturedImage", function() {
                            var a = t(".upload-file.featured-image-wrapper");
                            if (a.find('input[name="wiloke_raw_featured_image"]').length && !a.find('input[name="wiloke_raw_featured_image"]').val().length) return a.addClass("uploaded"), t(".acf-field-image").length ? i.trigger("processAFCUploads") : i.trigger("processUploadGallery"), !0;
                            a.wrap('<form id="wiloke-create-form-submission" enctype="multipart/form-data"></form>');
                            var r = document.getElementById("wiloke-create-form-submission"),
                                n = new FormData(r);
                            e = t.ajax({
                                url: WILOKE_GLOBAL.ajaxurl1 + "?type=single&action=wiloke_submission_insert_media&where=wiloke-upload-feature-image&name=wiloke_raw_featured_image",
                                type: "POST",
                                async: !0,
                                cache: !1,
                                contentType: !1,
                                processData: !1,
                                data: n,
                                success: function(e) {
                                    e.success && a.find(".wiloke-insert-id").attr("value", e.data.message), a.unwrap(), a.addClass("uploaded"), t(".acf-field-image").length ? i.trigger("processAFCUploads") : i.trigger("processUploadGallery")
                                }
                            })
                        })
                    }
                }, {
                    key: "anonymousUploadGalleryImg",
                    value: function() {
                        var e = t("body"),
                            i = null;
                        t(".upload-file:not(.uploaded)").length;
                        e.on("processUploadGallery", function() {
                            var a = t(".upload-file:not(.uploaded)").first(),
                                r = a.find(".wiloke-simple-upload"),
                                n = "multiple-upload" === r.data("uploadmode") ? "multiple" : "single";
                            if (!a.length) return e.data("Wiloke/Submission/UploadedGallery", !0), e.trigger("galleryHasbeenUploaded"), !1;
                            if (!r.val().length) return a.addClass("uploaded"), e.trigger("processUploadGallery"), !0;
                            a.wrap('<form id="wiloke-create-form-submission" enctype="multipart/form-data"></form>');
                            var s = document.getElementById("wiloke-create-form-submission"),
                                l = new FormData(s),
                                o = r.attr("name");
                            o = o.replace("[]", ""), i = t.ajax({
                                url: WILOKE_GLOBAL.ajaxurl + "?type=" + n + "&action=wiloke_submission_insert_media&where=" + a.attr("id") + "&name=" + o,
                                type: "POST",
                                async: !0,
                                cache: !1,
                                contentType: !1,
                                processData: !1,
                                data: l,
                                success: function(i) {
                                    a.addClass("uploaded"), i.success && a.find(".wiloke-gallery-ids").attr("value", i.data.message), a.unwrap(), (a = t(".upload-file:not(.uploaded)").first()).length ? e.trigger("processUploadGallery") : (e.data("Wiloke/Submission/UploadedGallery", !0), e.trigger("galleryHasbeenUploaded"))
                                }
                            })
                        })
                    }
                }, {
                    key: "mediaUpload",
                    value: function() {
                        if (!_.isUndefined(this.$form.data("isuserloggedin")) && this.$form.data("isuserloggedin")) this.$form.find(".wiloke-add-featured-image, .wiloke-upload-image").each(function() {
                            new n(t(this))
                        });
                        else {
                            var e = this.$form;
                            this.$form.find(".wiloke-simple-upload").each(function() {
                                new s(t(this), e)
                            })
                        }
                    }
                }, {
                    key: "validate",
                    value: function() {
                        if (t("#createaccount").is(":checked")) {
                            var e = t("#wiloke-reg-email");
                            if ("" === e.val()) return this.$error = e, this.scrollTop(), !1
                        } else {
                            /*
                            var i = t("#wiloke-user-login"),
                                a = t("#wiloke-my-password");
                            if ("" === i.val()) return this.$error = i, this.scrollTop(), !1;
                            if ("" === a.val()) return this.$error = a, this.scrollTop(), !1
                            */
                        }
                        return !0
                    }
                }, {
                    key: "validateACF",
                    value: function() {
                        if (this.$acfWrapper.length) {
                            var e = "",
                                i = this;
                            this.$acfWrapper.find(".field").each(function() {
                                var a = t(this);
                                if (a.hasClass("required") && (e = a.data("field_type"), "" === a.find("." + e).val())) return a.addClass("validate-required"), i.$error = a, i.scrollTop(), !1
                            })
                        }
                        return this.getACFCustomFieldData(), !0
                    }
                }, {
                    key: "getACFCustomFieldData",
                    value: function() {
                        if (this.$acfWrapper.length) {
                            var e = "",
                                i = "",
                                a = {},
                                r = "",
                                n = ".field",
                                s = "field_type",
                                l = "field_key",
                                o = "field_key";
                            this.$acfWrapper.find(".acf-field").length && (s = "type", l = "name", o = "key", n = ".acf-field"), this.$acfWrapper.find(n).each(function() {
                                var n = t(this);
                                if (e = n.data(s), r = n.data(o), i = n.data(l), "wysiwyg" === e) {
                                    var d = n.find(".wp-editor-area").attr("id");
                                    t("#wp-acf_settings-wrap").hasClass("tmce-active") ? a[r] = tinyMCE.get(d).getContent() : a[r] = t("#" + d).val()
                                } else if ("checkbox" === e) n.find('[type="checkbox"]:checked').each(function() {
                                    void 0 === a[r] && (a[r] = []), a[r].push(t(this).val())
                                });
                                else {
                                    var u = n.find('[name="fields[' + r + ']"]').length ? n.find('[name="fields[' + r + ']"]') : n.find('[name="acf[' + r + ']"]');
                                    a[r] = u.val()
                                }
                            }), this.acfData = JSON.stringify(a)
                        }
                    }
                }, {
                    key: "scrollTop",
                    value: function() {
                        if (!this.$error.length) return !1;
                        this.addErrorClass(), t("html, body").animate({
                            scrollTop: this.$error.offset().top - 40
                        }, 300)
                    }
                }, {
                    key: "addErrorClass",
                    value: function() {
                        this.$error.closest(".form-item").addClass("validate-required")
                    }
                }, {
                    key: "removeGallery",
                    value: function() {
                        this.$form.on("click", ".wil-addlisting-gallery__list-remove", function() {
                            "no" === WILOKE_GLOBAL.isLoggedIn ? t(this).closest("#wiloke-show-gallery").empty() : t(this).closest(".gallery-item").remove()
                        })
                    }
                }, {
                    key: "requestGallery",
                    value: function() {
                        var e = this.$form.find(".wil-addlisting-gallery"),
                            i = [];
                        e.length && "no" !== WILOKE_GLOBAL.isLoggedIn && e.each(function() {
                            i = [];
                            var e = t(this);
                            e.find(".gallery-item").each(function() {
                                void 0 !== t(this).data("id") && i.push(t(this).data("id"))
                            });
                            var a = i.join(",");
                            e.parent().find(".wiloke-gallery-ids").attr("value", a)
                        })
                    }
                }, {
                    key: "validateEmailRegister",
                    value: function() {
                        // var e = this;
                        // t.ajax({
                        //     url: WILOKE_GLOBAL.ajaxurl,
                        //     type: "POST",
                        //     data: {
                        //         action: "wiloke_verify_email",
                        //         email: t("#wiloke-reg-email").val(),
                        //         security: WILOKE_GLOBAL.wiloke_nonce
                        //     },
                        //     success: function(i) {
                        //         i.success ? (t("body").data("WilokeUserLoggedIn", !0), e.$form.find("#wiloke-reg-invalid-email").addClass("hidden"), e.$form.trigger("submit")) : (e.$form.find("#wiloke-reg-invalid-email").removeClass("hidden"), e.$form.trigger("stopping_upload"))
                        //     }
                        // })
                    }
                }, {
                    key: "previewListing",
                    value: function() {
                        // var e = this;
                        // if (this.$form.length) {
                        //     var i = t("body");
                        //     this.$error = null, this.$preview = t("#wiloke-listgo-preview-listing"), this.$printMessage = this.$form.find(".wiloke-print-msg-here"), i.on("galleryHasbeenUploaded", function() {
                        //         e.$form.trigger("submit")
                        //     }), this.$form.on("submit", function(i) {
                        //         i.preventDefault();
                        //         var a = t(i.target);
                        //         e.ajaxPreviewListing(a)
                        //     })
                        // }
                    }
                }, {
                    key: "submitListing",
                    value: function() {
                        // var e = this;
                        // this.$submit.length && (this.$submit.removeClass("not-active"), this.$submit.on("click", function(i) {
                        //     i.preventDefault();
                        //     var a = t(i.currentTarget);
                        //     if (a.data("processing")) return !1;
                        //     a.addClass("loading"), a.data("processing", !0), e.requestGallery(), t.ajax({
                        //         type: "POST",
                        //         global: !1,
                        //         url: WILOKE_GLOBAL.ajaxurl,
                        //         data: {
                        //             action: "wiloke_submission_submit_listing",
                        //             security: WILOKE_GLOBAL.wiloke_nonce,
                        //             listing_id: a.data("postid"),
                        //             planID: t("#package_id").val()
                        //         },
                        //         success: function(e) {
                        //             e.success ? window.location.href = decodeURIComponent(e.data.redirectTo) : alert(e.data.msg), a.removeClass("loading"), a.data("processing", !1)
                        //         }
                        //     })
                        // }))
                    }
                }, {
                    key: "ajaxPreviewListing",
                    value: function(e) {
                        // var i = this,
                        //     a = t("body"),
                        //     r = this.validate();
                        // if (this.$printMessage.html(""), !r) return !1;
                        // if (!(r = this.validateACF())) return !1;
                        // if (this.$preview.addClass("loading"), this.$editListing.addClass("loading"), this.$preview.prop("disabled", !0), null !== this.xhrPreview && 200 !== this.xhrPreview.status && (this.$preview.removeClass("loading"), this.$preview.prop("disabled", !1), this.xhrPreview.abort()), this.$form.on("stopping_upload", function(e) {
                        //         return i.$preview.removeClass("loading"), i.$preview.prop("disabled", !1), null !== i.xhrPreview && 200 !== i.xhrPreview && i.xhrPreview.abort(), !1
                        //     }), this.$preview.addClass("loading")){};
                        // var n = this.getContent(),
                        //     s = this.getWYSIWYGContent();
                        // this.requestGallery(), this.$preview.prop("disabled", !1), this.xhrPreview = t.ajax({
                        //     dataType: "json",
                        //     type: "POST",
                        //     //url: urls.postListing + "&showtemplate=false",
                        //     data: {
                        //         data: this.$form.serialize(),
                        //         content: n,
                        //         oWYSIWYG: s
                        //     },
                        //     success: function(e) {
                        //         console.log(e);
                        //         if (e.success) window.location.href = decodeURIComponent(e.data.redirectTo);
                        //         else {
                        //             var a = "";
                        //             _.forEach(e.data, function(e, i) {
                        //                 if ("isCreatedNewAccount" != i) {
                        //                     var r = t("#" + i + "-wrapper");
                        //                     (r = r.length ? r : t("#" + i)).length && (r.hasClass("add-listing-group") ? r.addClass("validate-required") : r.closest(".form-item").addClass("validate-required"), t("body, html").stop().animate({
                        //                         scrollTop: r.offset().top
                        //                     }, 500, "swing")), a += '<p class="update-status error-msg">' + e + "</p>"
                        //                 }
                        //             }), "" !== a && i.$printMessage.html(a).removeClass("hidden")
                        //         }
                        //         i.$preview.prop("disabled", !1), i.$preview.removeClass("loading"), i.$editListing.removeClass("loading")
                        //     }
                        // })
                    }
                }, {
                    key: "ajaxUpdateListing",
                    value: function() {
                        var e = this.validate();
                        return this.$printMessage.html(""), !!e && (!!(e = this.validateACF()) && (null !== this.xhrUpdateListing && 200 !== this.xhrUpdateListing.status && this.xhrUpdateListing.abort(), this.$form.addClass("loading"), void this.ajaxPreviewListing(this.$form)))
                    }
                }, {
                    key: "confirmEditListing",
                    value: function(e) {
                        var i = this;
                        t("#listgo-cancel-edit-listing").on("click", function(t) {
                            t.preventDefault(), i.$editListing.removeClass("loading"), e.removeClass("wil-modal--open")
                        }), t("#listgo-continue-editing-listing").on("click", function(t) {
                            i.$form.trigger("submit"), t.preventDefault(), e.removeClass("wil-modal--open")
                        })
                    }
                }, {
                    key: "editListing",
                    value: function() {
                        var e = this;
                        if (this.$editListing.length) {
                            this.xhrUpdateListing = null;
                            var i = t("#wiloke-form-update-listing-wrapper");
                            this.$editListing.on("click", function(a) {
                                a.preventDefault(), t(a.preventDefault).addClass("loading"), "allow_need_review" === e.$editListing.data("edittype") ? (i.addClass("wil-modal--open"), e.confirmEditListing(i)) : e.$form.trigger("submit")
                            })
                        }
                    }
                }]), a
            }(),
            n = function() {
                function a(i) {
                    e(this, a), this.$trigger = i, this.init()
                }
                return i(a, [{
                    key: "template",
                    value: function(e, i) {
                        return _.template("<li class='gallery-item bg-scroll' data-id='<%- id %>' style='background-image: url(<%- backgroundUrl %>)'><span class='wil-addlisting-gallery__list-remove'>Remove</span></li>")({
                            backgroundUrl: e,
                            id: i
                        })
                    }
                }, {
                    key: "init",
                    value: function() {
                        var e = this;
                        this.$trigger.length && this.$trigger.on("click", function(i) {
                            var a = t(i.currentTarget);
                            i.preventDefault();
                            var r = "multiple-upload" === a.data("uploadmode");
                            if (a.data("frame")) return a.data("frame").open(), !1;
                            var n = wp.media({
                                title: "",
                                button: {
                                    text: "Select"
                                },
                                multiple: r
                            });
                            a.data("frame", n);
                            var s = void 0 !== a.data("imgsize") ? a.data("imgsize") : "thumbnail";
                            a.data("frame").on("select", function() {
                                if (r) {
                                    var i = a.data("frame").state().get("selection").toJSON(),
                                        t = "";
                                    _.forEach(i, function(i) {
                                        var a = !_.isUndefined(i.sizes.thumbnail) && i.sizes[s] ? i.sizes[s].url : i.url;
                                        t += e.template(a, i.id)
                                    }), a.parent().before(t)
                                } else {
                                    var n = a.data("frame").state().get("selection").first().toJSON(),
                                        l = !_.isUndefined(n.sizes[s]) && n.sizes[s] ? n.sizes[s].url : n.url;
                                    if (a.hasClass("wiloke-add-featured-image")) a.find(".wiloke-preview").attr("src", l), a.find(".add-listing__upload-preview").css("background-image", "url(" + n.url + ")");
                                    else {
                                        a.parent().prev().length && a.parent().prev().remove();
                                        var o = e.template(l, n.id);
                                        a.parent().before(o)
                                    }
                                    a.closest(".upload-file").find(".wiloke-insert-id").val(n.id)
                                }
                            }), a.data("frame").open()
                        })
                    }
                }]), a
            }(),
            s = function() {
                function a(i, t) {
                    e(this, a), this.$target = i, this.$form = t, this.$wrapper = this.$target.closest(".upload-file"), this.$preview = this.$wrapper.find(".wil-addlisting-gallery__list"), this.maxfilesize = t.data("uploadfilesize"), this.xhr = null, this.uploadInputChanged(), this.removeSingleUpload()
                }
                return i(a, [{
                    key: "uploadInputChanged",
                    value: function() {
                        var e = this;
                        this.$remider = this.$target.closest(".input-upload-file").siblings(".wiloke-submission-reminder"), isNaN(this.maxfilesize) && (this.maxfilesize = 1e3 * parseInt(this.maxfilesize.replace("M", ""), 10) * 1e3), this.$target.on("change", function() {
                            e.aListFiles = this.files, e.showImageReview()
                        })
                    }
                }, {
                    key: "removeSingleUpload",
                    value: function() {
                        this.$form.on("click", ".wil-addlisting-gallery__list-remove", function(e) {
                            var i = t(e.currentTarget).closest(".wil-addlisting-gallery");
                            i.hasClass("single-upload") ? i.find(".wil-addlisting-gallery__list").empty() : t(e.currentTarget).parent().remove()
                        })
                    }
                }, {
                    key: "showImageReview",
                    value: function() {
                        if (this.aListFiles.length) {
                            var e = this,
                                i = !this.$preview.hasClass("single-upload"),
                                a = this.$remider,
                                r = this.maxfilesize;
                            _.each(this.aListFiles, function(n) {
                                if (/\.(jpe?g|png|gif)$/i.test(n.name)) {
                                    var s = new FileReader;
                                    s.addEventListener("load", function() {
                                        var s = '<li class="bg-scroll' + (n.size <= r ? "" : " error") + '" style="background-image: url(' + this.result + ')"><span class="wil-addlisting-gallery__list-remove">Remove</span></li>';
                                        i ? e.$preview.append(s) : e.$preview.html(s), a.removeClass("review_status error-msg"), t("body").trigger("processUpload")
                                    }, !1), s.readAsDataURL(n)
                                }
                            })
                        }
                    }
                }]), a
            }();
        t(".input-select2").children().each(function() {
            var e = {},
                i = t(this);
            i.data("maximumSelection") && (e.maximumSelectionLength = i.data("maximumSelection")), i.data("placeholder") && (e.placeholder = i.data("placeholder")), i.select2(e)
        }), setTimeout(function() {
            new r
        }, 2e3), t(document).ready(function() {
            a()
        })
    }(jQuery)
}();