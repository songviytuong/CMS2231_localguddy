var listgo_backend_mapextend = (function(e) {
  "use strict";
  return (
    (function(e) {
      function t(e, t, o) {
        return void 0 === o || 0 == +o
          ? Math[e](t)
          : ((t = +t),
            (o = +o),
            isNaN(t) || "number" != typeof o || o % 1 != 0
              ? NaN
              : ((t = t.toString().split("e")),
                (t = Math[e](+(t[0] + "e" + (t[1] ? +t[1] - o : -o)))),
                +(
                  (t = t.toString().split("e"))[0] +
                  "e" +
                  (t[1] ? +t[1] + o : o)
                )));
      }
      Math.round10 ||
        (Math.round10 = function(e, o) {
          return t("round", e, o);
        }),
        (e.fn.WilokeMapExtendAdmin = function(t) {
          var o = this,
            n = e.extend(
              {
                initMap: function() {},
                beforeResize: function() {},
                afterResize: function() {},
                dragMarker: function() {},
                dragEndMarker: function() {}
              },
              t
            );
          (o.WilokeMapAdmin = {
            options: n,
            focusDetectMyLocation: !1,
            oPlaces: null,
            oLatLng: {
              lat: 10.823099,
              lng: 106.629664
            },
            el: o,
            init: function() {
              (this.stopParseGeocode = !1),
                (this.oTranslation =
                  "undefined" != typeof WILOKE_GLOBAL
                    ? WILOKE_LISTGO_TRANSLATION
                    : {
                        noresults: "No results found",
                        geocodefailed: "Geocoder failed due to:"
                      }),
                this.createHTML(),
                this.getCurrentPosition(),
                this.createMap();
            },
            getCurrentPosition: function() {
              var t = this;
              if (
                e("#wiloke-latlong").length &&
                "" == e("#wiloke-latlong").val()
              ) {
                var o = e("body").data("wiloke_listgo_user_latlng");
                void 0 !== o
                  ? ((this.oLatLng.lat = o.lat),
                    (this.oLatLng.lng = o.lng),
                    this.setCurrentLocation())
                  : this.getGeoLocation();
              }
              e("#wiloke-fill-my-location").on("click", function() {
                return t.getGeoLocation();
              });
            },
            setCurrentLocation: function() {
              var e = this,
                t = new google.maps.LatLng(e.oLatLng.lat, e.oLatLng.lng);
              e.marker.setPosition(t),
                e.infoWindow.setContent(e.oLatLng.lat + "," + e.oLatLng.lng),
                e.infoWindow.open(e.map, e.marker),
                e.map.setCenter(t),
                e.map.panTo(t),
                e.geocoder.geocode(
                  {
                    latLng: t
                  },
                  function(t) {
                    t && t.length > 0
                      ? (e.marker.formatted_address = t[0].formatted_address)
                      : (e.marker.formatted_address =
                          "Cannot determine address at this location."),
                      e.inputLocation.val(e.marker.formatted_address);
                  }
                );
              (e.location = e.marker.getPosition(e.map.getCenter())),
                e.geocodePosition(e.location);
            },
            getGeoLocation: function() {
              var e = this;
              navigator.geolocation &&
                navigator.geolocation.getCurrentPosition(function(t) {
                  (e.oLatLng.lat = t.coords.latitude),
                    (e.oLatLng.lng = t.coords.longitude),
                    e.inputLatLong.attr(
                      "value",
                      e.oLatLng.lat + "," + e.oLatLng.lng
                    ),
                    void 0 !== e.map && e.setCurrentLocation();
                  e.showLatLngInput(e);
                });
            },
            showLatLngInput: function(e) {
              var t = Math.round10(parseFloat(e.location.lat()), -6),
                o = Math.round10(parseFloat(e.location.lng()), -6);
              e.infoWindow.setContent(t + ", " + o),
                e.infoWindow.open(e.map, e.marker),
                n.dragEndMarker(),
                e.inputLocation.trigger("updateLatLng", {
                  changeLocation: !0
                });
              e.inputLat.attr("value", t);
              e.inputLng.attr("value", o);
            },
            createHTML: function() {
              (this.wrapperMap = e('<div class="pi-map-wrapper"></div>')),
                (this.iconMarker = e('<div class="icon-marker"></div>')),
                (this.inputLocation = e("#wiloke-location")),
                (this.inputLatLong = e("#wiloke-latlong")),
                (this.inputLat = e("#location_lat")),
                (this.inputLng = e("#location_lng")),
                (this.inputWebsite = e("#listing_website")),
                (this.inputPlaceInfo = e("#wiloke-place-information")),
                (this.inputPhone = e("#listing_phone")),
                (this.$usingMyLocation = e("#wiloke-fill-my-location")),
                (this.$listingPreview = e(".add-listing-group-preview")),
                (this.$mapPreview = e(".add-listing-group-preview-map")),
                this.el.append(this.wrapperMap),
                this.el.append(this.searchButton),
                this.el.append(this.iconMarker);
            },
            mapStyle: function() {
              return new google.maps.StyledMapType(
                [
                  {
                    elementType: "geometry",
                    stylers: [
                      {
                        color: "#ebe3cd"
                      }
                    ]
                  },
                  {
                    elementType: "labels.text.fill",
                    stylers: [
                      {
                        color: "#523735"
                      }
                    ]
                  },
                  {
                    elementType: "labels.text.stroke",
                    stylers: [
                      {
                        color: "#f5f1e6"
                      }
                    ]
                  },
                  {
                    featureType: "administrative",
                    elementType: "geometry.stroke",
                    stylers: [
                      {
                        color: "#c9b2a6"
                      }
                    ]
                  },
                  {
                    featureType: "administrative.land_parcel",
                    elementType: "geometry.stroke",
                    stylers: [
                      {
                        color: "#dcd2be"
                      }
                    ]
                  },
                  {
                    featureType: "administrative.land_parcel",
                    elementType: "labels.text.fill",
                    stylers: [
                      {
                        color: "#ae9e90"
                      }
                    ]
                  },
                  {
                    featureType: "landscape.natural",
                    elementType: "geometry",
                    stylers: [
                      {
                        color: "#dfd2ae"
                      }
                    ]
                  },
                  {
                    featureType: "poi",
                    elementType: "geometry",
                    stylers: [
                      {
                        color: "#dfd2ae"
                      }
                    ]
                  },
                  {
                    featureType: "poi",
                    elementType: "labels.text.fill",
                    stylers: [
                      {
                        color: "#93817c"
                      }
                    ]
                  },
                  {
                    featureType: "poi.park",
                    elementType: "geometry.fill",
                    stylers: [
                      {
                        color: "#a5b076"
                      }
                    ]
                  },
                  {
                    featureType: "poi.park",
                    elementType: "labels.text.fill",
                    stylers: [
                      {
                        color: "#447530"
                      }
                    ]
                  },
                  {
                    featureType: "road",
                    elementType: "geometry",
                    stylers: [
                      {
                        color: "#f5f1e6"
                      }
                    ]
                  },
                  {
                    featureType: "road.arterial",
                    elementType: "geometry",
                    stylers: [
                      {
                        color: "#fdfcf8"
                      }
                    ]
                  },
                  {
                    featureType: "road.highway",
                    elementType: "geometry",
                    stylers: [
                      {
                        color: "#f8c967"
                      }
                    ]
                  },
                  {
                    featureType: "road.highway",
                    elementType: "geometry.stroke",
                    stylers: [
                      {
                        color: "#e9bc62"
                      }
                    ]
                  },
                  {
                    featureType: "road.highway.controlled_access",
                    elementType: "geometry",
                    stylers: [
                      {
                        color: "#e98d58"
                      }
                    ]
                  },
                  {
                    featureType: "road.highway.controlled_access",
                    elementType: "geometry.stroke",
                    stylers: [
                      {
                        color: "#db8555"
                      }
                    ]
                  },
                  {
                    featureType: "road.local",
                    elementType: "labels.text.fill",
                    stylers: [
                      {
                        color: "#806b63"
                      }
                    ]
                  },
                  {
                    featureType: "transit.line",
                    elementType: "geometry",
                    stylers: [
                      {
                        color: "#dfd2ae"
                      }
                    ]
                  },
                  {
                    featureType: "transit.line",
                    elementType: "labels.text.fill",
                    stylers: [
                      {
                        color: "#8f7d77"
                      }
                    ]
                  },
                  {
                    featureType: "transit.line",
                    elementType: "labels.text.stroke",
                    stylers: [
                      {
                        color: "#ebe3cd"
                      }
                    ]
                  },
                  {
                    featureType: "transit.station",
                    elementType: "geometry",
                    stylers: [
                      {
                        color: "#dfd2ae"
                      }
                    ]
                  },
                  {
                    featureType: "water",
                    elementType: "geometry.fill",
                    stylers: [
                      {
                        color: "#b9d3c2"
                      }
                    ]
                  },
                  {
                    featureType: "water",
                    elementType: "labels.text.fill",
                    stylers: [
                      {
                        color: "#92998d"
                      }
                    ]
                  }
                ],
                {
                  name: "Styled Map"
                }
              );
            },
            createMap: function() {
              var e = "",
                t = "",
                o = void 0,
                n = void 0;
              (this.geocoder = new google.maps.Geocoder()),
                (this.location = new google.maps.LatLng(this.oLatLng)),
                (this.mapOptions = {
                  center: this.location,
                  zoom: 7,
                  mapTypeControlOptions: {
                    //mapTypeIds: ["roadmap", "satellite", "hybrid", "terrain", "styled_map"]
                    mapTypeIds: ["roadmap", "satellite", "hybrid"]
                  }
                }),
                (this.map = new google.maps.Map(
                  this.wrapperMap[0],
                  this.mapOptions
                )),
                this.map.mapTypes.set("styled_map", this.mapStyle()),
                this.map.setMapTypeId("styled_map"),
                this.map.setZoom(16),
                (this.marker = new google.maps.Marker({
                  map: this.map,
                  draggable: true
                })),
                (this.infoWindow = new google.maps.InfoWindow()),
                _.isUndefined(this.inputLatLong.val()) ||
                  _.isEmpty(this.inputLatLong.val()) ||
                  ((e = (n = this.inputLatLong.val().split(","))[0]),
                  (t = n[1])),
                "" !== e &&
                  "" !== t &&
                  ((o = new google.maps.LatLng(e, t)),
                  this.marker.setPosition(o),
                  this.infoWindow.setContent(e + "," + t),
                  this.infoWindow.open(this.map, this.marker)),
                (this.search = new google.maps.places.Autocomplete(
                  document.getElementById("wiloke-location")
                )),
                this.listenEvent();
            },
            setPhone: function(e) {
              this.inputPhone.length &&
                "" === this.inputPhone.val() &&
                this.inputPhone.val(e.international_phone_number);
            },
            setWebsite: function(e) {
              this.inputWebsite.length &&
                "" === this.inputWebsite.val() &&
                this.inputWebsite.val(e.website);
            },
            b64EncodeUnicode: function(e) {
              return btoa(
                encodeURIComponent(e).replace(/%([0-9A-F]{2})/g, function(
                  e,
                  t
                ) {
                  return String.fromCharCode("0x" + t);
                })
              );
            },
            setPlaceInfo: function(e) {
              this.inputPlaceInfo.length &&
                this.inputPlaceInfo.val(
                  this.b64EncodeUnicode(JSON.stringify(e))
                );
            },
            geocodePosition: function(e) {
              var t = this;
              t.stopParseGeocode
                ? (t.stopParseGeocode = !1)
                : t.geocoder.geocode(
                    {
                      latLng: e
                    },
                    function(e) {
                      e && e.length > 0
                        ? (t.marker.formatted_address = e[0].formatted_address)
                        : (t.marker.formatted_address =
                            "Cannot determine address at this location."),
                        t.inputLocation.val(t.marker.formatted_address),
                        t.inputLocation.trigger("updateLatLng", {
                          changeLocation: !0
                        });
                    }
                  );
            },
            listenEvent: function() {
              var e = this;
              e.inputLocation.on("updateLatLng", function(t, o) {
                if (e.location && o && !0 === o.changeLocation) {
                  var n = Math.round10(e.location.lat(), -6);
                  var a = Math.round10(e.location.lng(), -6);
                  e.inputLatLong.attr("value", n + "," + a),
                    e.resize(),
                    _.isEmpty(e.oPlaces) ||
                      (e.setPhone(e.oPlaces),
                      e.setWebsite(e.oPlaces),
                      e.setPlaceInfo(e.oPlaces));
                }
              }),
                google.maps.event.addListener(
                  e.search,
                  "place_changed",
                  function() {
                    var t = e.search.getPlace();
                    (e.location = t.geometry.location),
                      (e.oPlaces = t),
                      e.marker.setPosition(e.location),
                      (e.stopParseGeocode = !0),
                      google.maps.event.trigger(e.marker, "dragend");
                  }
                ),
                google.maps.event.addListener(e.marker, "dragend", function() {
                  n.dragMarker();
                }),
                google.maps.event.addListener(e.marker, "dragend", function() {
                  (e.location = e.marker.getPosition()),
                    e.geocodePosition(e.location);
                  var t = Math.round10(parseFloat(e.location.lat()), -6);
                  var o = Math.round10(parseFloat(e.location.lng()), -6);
                  e.infoWindow.setContent(t + ", " + o),
                    e.infoWindow.open(e.map, e.marker),
                    n.dragEndMarker(),
                    e.inputLocation.trigger("updateLatLng", {
                      changeLocation: !0
                    });
                  e.inputLat.attr("value", t);
                  e.inputLng.attr("value", o);
                }),
                google.maps.event.addListener(
                  e.map,
                  "center_changed",
                  function() {
                    e.marker.setPosition(e.map.getCenter());
                  }
                ),
                google.maps.event.addListener(e.map, "dragend", function() {
                  (e.location = e.marker.getPosition(e.map.getCenter())),
                    e.geocodePosition(e.location);
                  var t = Math.round10(parseFloat(e.location.lat()), -6),
                    o = Math.round10(parseFloat(e.location.lng()), -6);
                  e.infoWindow.setContent(t + ", " + o),
                    e.infoWindow.open(e.map, e.marker),
                    n.dragEndMarker(),
                    e.inputLocation.trigger("updateLatLng", {
                      changeLocation: !0
                    });
                  e.inputLat.attr("value", t);
                  e.inputLng.attr("value", o);
                }),
                google.maps.event.addDomListener(window, "resize", function() {
                  e.resize();
                });
            },
            resize: function() {
              n.beforeResize(this.el, n),
                google.maps.event.trigger(this.map, "resize"),
                this.map.setCenter(this.location),
                n.afterResize(this.el, n);
            }
          }),
            o.WilokeMapAdmin.init();
        }),
        e(window).load(function() {
          var t = e("#wiloke-map");
          t.length &&
            t.WilokeMapExtendAdmin({
              initMap: function() {},
              beforeResize: function() {},
              afterResize: function() {},
              dragMarker: function() {},
              dragEndMarker: function() {}
            });
        });
    })(jQuery),
    {}
  );
})();
