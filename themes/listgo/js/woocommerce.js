! function() {
    "use strict";
    ! function(a) {
        function n() {
            var n = a("#cart-mini-content");
            a(document).on("click", ".add_to_cart_button", function() {
                var a = parseInt(n.data("total"), 10);
                _.isNaN(a) ? a = 1 : a += 1, n.data("total", a), a = a > 1 ? "(" + a + " items)" : "(" + a + " item)", n.find("span").html(a)
            })
        }

        function t() {
            var n = setInterval(function() {
                var t = a(".woocommerce-product-gallery").find(".flex-control-nav");
                t.length ? (t.data("running") && clearInterval(n), t.owlCarousel({
                    loop: !1,
                    items: 4,
                    nav: !0,
                    rtl: !1,
                    navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
                    dragClass: "owl-drag owl-carousel nav-middle"
                }), t.data("running", !0)) : clearInterval(n)
            }, 300)
        }
        jQuery(document).ready(function() {
            t(), n()
        })
    }(jQuery)
}();