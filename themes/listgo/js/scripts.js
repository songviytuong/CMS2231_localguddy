! function() {
    "use strict";
    ! function() {
        function t(t) {
            this.value = t
        }

        function e(e) {
            function i(n, s) {
                try {
                    var o = e[n](s),
                        r = o.value;
                    r instanceof t ? Promise.resolve(r.value).then(function(t) {
                        i("next", t)
                    }, function(t) {
                        i("throw", t)
                    }) : a(o.done ? "return" : "normal", o.value)
                } catch (t) {
                    a("throw", t)
                }
            }

            function a(t, e) {
                switch (t) {
                    case "return":
                        n.resolve({
                            value: e,
                            done: !0
                        });
                        break;
                    case "throw":
                        n.reject(e);
                        break;
                    default:
                        n.resolve({
                            value: e,
                            done: !1
                        })
                }(n = n.next) ? i(n.key, n.arg): s = null
            }
            var n, s;
            this._invoke = function(t, e) {
                return new Promise(function(a, o) {
                    var r = {
                        key: t,
                        arg: e,
                        resolve: a,
                        reject: o,
                        next: null
                    };
                    s ? s = s.next = r : (n = s = r, i(t, e))
                })
            }, "function" != typeof e.return && (this.return = void 0)
        }
        "function" == typeof Symbol && Symbol.asyncIterator && (e.prototype[Symbol.asyncIterator] = function() {
            return this
        }), e.prototype.next = function(t) {
            return this._invoke("next", t)
        }, e.prototype.throw = function(t) {
            return this._invoke("throw", t)
        }, e.prototype.return = function(t) {
            return this._invoke("return", t)
        }
    }();
    var t = function(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function")
        },
        e = function() {
            function t(t, e) {
                for (var i = 0; i < e.length; i++) {
                    var a = e[i];
                    a.enumerable = a.enumerable || !1, a.configurable = !0, "value" in a && (a.writable = !0), Object.defineProperty(t, a.key, a)
                }
            }
            return function(e, i, a) {
                return i && t(e.prototype, i), a && t(e, a), e
            }
        }(),
        i = function() {
            function i() {
                t(this, i), this.blockID = null, this.name = null, this.prefix = "wiloke_listgo_", this.aPageLoaded = [], this.period = 86400, this.cachingMode = "default", this.aPageCaching = {}, this.oText = WILOKE_LISTGO_TRANSLATION
            }
            return e(i, [{
                key: "notFound",
                value: function() {
                    return _.template("<p><%- text %></p>")({
                        text: this.oText.notfound
                    })
                }
            }, {
                key: "setPageLoaded",
                value: function(t) {
                    this.aPageLoaded.push(t.fullPath)
                }
            }, {
                key: "getPageLoaded",
                value: function(t) {
                    return !!this.aPageLoaded.length && -1 !== this.aPageLoaded.indexOf(t.fullPath)
                }
            }, {
                key: "setCaching",
                value: function(t, e, i) {
                    this.name = this.prefix + e, this.blockID = i, _.isUndefined(this.blockID) || this.generateBlockIDKey();
                    var a = JSON.stringify(t);
                    "default" === this.cachingMode ? this.aPageCaching[this.name] = a : (localStorage.setItem(this.name, a), this.saveCreatedAt())
                }
            }, {
                key: "getCaching",
                value: function(t, e, i) {
                    this.name = this.prefix + t, this.blockID = e, this.generateBlockIDKey();
                    var a = this.getCreatedAt();
                    if (i = parseInt(i, 10), a = parseInt(a), !_.isNaN(i) && !_.isNaN(a) && i > a) return !1;
                    if ("default" === this.cachingMode) return void 0 !== this.aPageCaching[this.name] && JSON.parse(this.aPageCaching[this.name]);
                    var n = localStorage.getItem(this.name);
                    return JSON.parse(n)
                }
            }, {
                key: "parseJSON",
                value: function(t) {
                    try {
                        return JSON.parse(t)
                    } catch (t) {
                        return WilokeDevMod, !1
                    }
                }
            }, {
                key: "setListings",
                value: function(t) {
                    this.aPageCaching.all = t
                }
            }, {
                key: "getListings",
                value: function() {
                    var t = this.aPageCaching.all;
                    return !_.isUndefined(t) && t
                }
            }, {
                key: "generateBlockIDKey",
                value: function() {
                    this.name = this.name + "_" + this.blockID
                }
            }, {
                key: "saveCreatedAt",
                value: function() {
                    var t = (new Date).getTime();
                    localStorage.setItem(this.name + "_created_at", t)
                }
            }, {
                key: "getCreatedAt",
                value: function() {
                    return localStorage.getItem(this.name + "_created_at")
                }
            }], [{
                key: "deleteCaching",
                value: function(t) {
                    localStorage.setItem(t, !1)
                }
            }]), i
        }(),
        a = function() {
            function i(e, a, n) {
                t(this, i), this.currentPage = n, this.totalPosts = e, this.postsPerPage = a, this.maxPages = 0, this.oText = WILOKE_LISTGO_TRANSLATION
            }
            return e(i, [{
                key: "createPagination",
                value: function() {
                    var t = [],
                        e = "";
                    if (this.totalPosts = parseInt(this.totalPosts), this.postsPerPage = parseInt(this.postsPerPage), 0 === this.totalPosts || this.totalPosts < this.postsPerPage) return "";
                    if (this.maxPages = Math.ceil(this.totalPosts / this.postsPerPage), this.maxPages <= 1) return !1;
                    if (this.currentPage = _.isUndefined(this.currentPage) ? 1 : this.currentPage, this.maxPages <= 8)
                        for (var i = 1; i <= this.maxPages; i++) t.push(i);
                    else if (this.currentPage <= 3) t = [1, 2, 3, 4, "x", this.maxPages];
                    else if (this.currentPage < 7) t = [1, 2, 3, 4, 5, 6, 7, "x", this.maxPages];
                    else {
                        t = [1, "x"];
                        for (var a = 2; a >= 0; a--) t.push(this.currentPage - a);
                        if (this.maxPages - this.currentPage <= 8)
                            for (var n = this.currentPage + 1; n <= this.maxPages; n++) t.push(n);
                        else {
                            for (var s = 0; s <= 2; s++) t.push(this.currentPage + 1 + s);
                            t.push("x"), t.push(this.maxPages)
                        }
                    }
                    for (var o = 0, r = t.length; o < r; o++) this.currentPage === t[o] ? e += '<a class="page-numbers current" data-page="' + t[o] + '">' + t[o] + "</a>" : "x" === t[o] ? e += '<a href="#" class="page-numbers dots">...</a>' : e += '<a href="#" class="page-numbers"  data-page="' + t[o] + '">' + t[o] + "</a>";
                    return "" !== e && (1 !== this.currentPage && (e += '<a href="#" class="prev page-numbers">' + this.oText.prev + "</a>"), this.currentPage !== this.maxPages && (e += '<a href="#" class="next page-numbers">' + this.oText.next + "</a>")), e
                }
            }]), i
        }(),
        n = function() {
            function i(e) {
                t(this, i), this.$gallery = e, this.init()
            }
            return e(i, [{
                key: "init",
                value: function() {
                    this.$gallery.length && this.$gallery.each(function() {
                        jQuery(this).magnificPopup({
                            delegate: "a",
                            type: "image",
                            tLoading: "Loading image #%curr%...",
                            mainClass: "mfp-with-zoom mfp-img-mobile",
                            closeBtnInside: !1,
                            closeMarkup: '<div class="popup-gallery__close pe-7s-close"></div>',
                            gallery: {
                                enabled: !0,
                                navigateByImgClick: !0,
                                preload: [0, 1],
                                tCounter: "%curr%/%total%"
                            },
                            image: {
                                verticalFit: !0,
                                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                                titleSrc: function(t) {
                                    if (void 0 !== t.el.attr("data-title")) {
                                        var e = t.el.attr("data-title");
                                        return _.isUndefined(t.el.data("linkto")) || (e = '<a target="' + (_.isUndefined(t.el.attr("target")) ? "_self" : t.el.attr("target")) + '" href="' + t.el.data("linkto") + '">' + e + "</a>"), e
                                    }
                                    return ""
                                }
                            }
                        })
                    })
                }
            }]), i
        }(),
        s = function() {
            function i() {
                t(this, i)
            }
            return e(i, null, [{
                key: "Android",
                value: function() {
                    return navigator.userAgent.match(/Android/i)
                }
            }, {
                key: "BlackBerry",
                value: function() {
                    return navigator.userAgent.match(/BlackBerry/i)
                }
            }, {
                key: "IOS",
                value: function() {
                    return navigator.userAgent.match(/iPhone|iPad|iPod/i)
                }
            }, {
                key: "IPHONEIPOD",
                value: function() {
                    return navigator.userAgent.match(/iPhone|iPod/i)
                }
            }, {
                key: "Opera",
                value: function() {
                    return navigator.userAgent.match(/Opera Mini/i)
                }
            }, {
                key: "Windows",
                value: function() {
                    return navigator.userAgent.match(/IEMobile/i)
                }
            }, {
                key: "Any",
                value: function() {
                    return this.Android() || this.BlackBerry() || this.IOS() || this.Opera() || this.Windows()
                }
            }, {
                key: "exceptiPad",
                value: function() {
                    return this.Android() || this.BlackBerry() || this.IPHONEIPOD() || this.Opera() || this.Windows()
                }
            }]), i
        }();
    ! function() {
        function i() {
            t(this, i)
        }
        e(i, null, [{
            key: "isIE",
            value: function() {
                return -1 !== navigator.userAgent.indexOf("MSIE") || -1 !== navigator.userAgent.indexOf("Trident")
            }
        }])
    }();
    ! function(o) {
        function r() {
            o('[data-vc-full-width="true"]').each(function(t, e) {
                var i = o(this);
                i.css("right", i.css("left")).css("left", "")
            })
        }

        function l(t, e, i) {
            return void 0 === i || 0 == +i ? Math[t](e) : (e = +e, i = +i, isNaN(e) || "number" != typeof i || i % 1 != 0 ? NaN : (e = e.toString().split("e"), e = Math[t](+(e[0] + "e" + (e[1] ? +e[1] - i : -i))), +((e = e.toString().split("e"))[0] + "e" + (e[1] ? +e[1] + i : i))))
        }

        function c() {
            o(".wiloke-row-handling").each(function() {
                o(this).checkRowHandled("wiloke-row-handling", 2e3)
            }), o(".wiloke-row-handling:last").addClass("is-last-row"), setTimeout(function() {
                o("body").find(".wiloke-row-handling").removeClass("wiloke-row-handling")
            }, 3e3)
        }

        function d() {
            var t = o(".js_select2");
            if (t.length && !s.Any()) {
                var e = WILOKE_LISTGO_TRANSLATION;
                t.each(function() {
                    var t = void 0 !== o(this).data("tags"),
                        i = void 0 !== o(this).data("placeholder") ? o(this).data("placeholder") : e.selectoption;
                    o(this).select2({
                        tags: t,
                        placeholder: i,
                        templateResult: function(t) {
                            var e = o(t.element).data("img");
                            return "" === e || _.isUndefined(e) ? t.text : o('<span><img src="' + e + '" class="img-flag"> ' + t.text + "</span>")
                        }
                    }).addClass("created")
                })
            }
            new n(o(".popup-gallery")), o("#commentform").wrap('<div class="row"></div>'), o(".vc_row").each(function() {
                var t = o(this).data("overlaycolor");
                o(this).css({
                    "background-color": t
                })
            });
            var i = o(".wiloke-simple-posts-slider");
            i.children().length && i.owlCarousel({
                items: 1,
                nav: !0,
                loop: !0,
                rtl: y,
                navText: ['<i class="arrow_carrot-left"></i>', '<i class="arrow_carrot-right"></i>']
            });
            var a = o(".twitter-slider");
            a.children().length && a.owlCarousel({
                items: 1,
                nav: !0,
                loop: !0,
                rtl: y,
                autoHeight: !0,
                navText: ['<i class="arrow_carrot-left"></i>', '<i class="arrow_carrot-right"></i>']
            });
            var r = o("#listgo-searchform");
            r.on("click", ".icon_search", function() {
                r.trigger("submit")
            }), o(".copyright").children().length || o(".social_footer").children().length || o(".footer__widget").length || o("#footer").css({
                "background-color": "#fff"
            });
            var l = o(".widget_author__content");
            l.find(".widget_author__address").children().length || l.find(".widget_author__social").children().length || l.remove()
        }

        function h() {
            var t = o(".wil-team__carousel"),
                e = o(".wil-team-list");
            t.length && t.owlCarousel({
                items: 1,
                singleItem: !0,
                loop: !1,
                nav: !1,
                dots: !1,
                rtl: y,
                lazyLoad: !0,
                URLhashListener: !0,
                autoplayHoverPause: !0,
                startPosition: "URLHash"
            }), e.length && e.perfectScrollbar({
                wheelPropagation: !0
            })
        }

        function u() {
            var t = o(".wil-scroll-top");
            t.on("click", function(t) {
                t.preventDefault(), jQuery("html, body").stop().animate({
                    scrollTop: 0
                }, 300)
            });
            var e = !1;
            t.on("scrollTop", function() {
                !1 !== e && clearTimeout(e), e = setTimeout(function() {
                    var i = o(window).scrollTop();
                    o(window).height() < i ? t.addClass("active") : t.removeClass("active"), clearTimeout(e)
                }, 2e3)
            })
        }

        function g() {
            var t = {};
            o(".wiloke-pin-to-top").on("click", function(e) {
                e.preventDefault(), t = _.isEmpty(t) ? WILOKE_LISTGO_TRANSLATION : t;
                var i = o(e.currentTarget);
                i.append('<span class="second-loading"></span>'), o.ajax({
                    type: "POST",
                    url: WILOKE_GLOBAL.ajaxurl,
                    data: {
                        action: "wiloke_listgo_pin_to_top",
                        listingID: i.data("postid"),
                        status: i.attr("data-status"),
                        security: WILOKE_GLOBAL.wiloke_nonce
                    },
                    success: function(e) {
                        e.success ? (i.attr("data-status", e.data.new_status), "pinned" !== e.data.new_status && o(".wiloke-listgo-pinned-" + i.data("postid")).remove(), i.html('<i class="fa fa-thumb-tack"></i> ' + t[e.data.new_status]), i.attr("data-tooltip", e.data.title)) : alert(e.data.msg), i.find(".second-loading").remove()
                    }
                })
            })
        }

        function f() {
            var t = o(".header-page__scrolldown");
            t.length > 0 && o(window).on("scroll", function() {
                var e = o(window).scrollTop(),
                    i = (o(window).outerHeight(), t.offset()),
                    a = t.outerHeight(!0),
                    n = (.5 * i.top - e) / (.5 * i.top + a);
                e > .5 * i.top && (n = 0), e < 20 && (n = 1), t.css("opacity", n)
            }), t.on("click", function(t) {
                var e = o(this),
                    i = e.offset().top + e.height();
                o("html, body").stop().animate({
                    scrollTop: i
                }, 300)
            })
        }

        function m() {
            var t = o("body").find(['iframe[src*="player.vimeo.com"]', 'iframe[src*="youtube.com"]', 'iframe[src*="youtube-nocookie.com"]', 'iframe[src*="kickstarter.com"][src*="video.html"]', "object", "embed"].join(","));
            t.each(function() {
                var e = o(this),
                    i = e.outerWidth(),
                    a = e.outerHeight() / i * 100;
                t.addClass("embed-responsive-item"), 75 === a ? t.css("display", "block").wrap('<div class="embed-responsive embed-responsive-4by3"></div>') : t.css("display", "block").wrap('<div class="embed-responsive embed-responsive-16by9"></div>')
            })
        }

        function p() {
            var t = o("#header").data("navcolor");
            void 0 !== t && (o(".header__content .wiloke-menu:not(.wiloke-menu-responsive) .wiloke-menu-list > .wiloke-menu-item > a,.header__content .wiloke-menu:not(.wiloke-menu-responsive) .wiloke-menu-list .wiloke-menu-sub > .wiloke-menu-item > a,.header__add-listing a, .notifications__icon").css({
                color: t
            }), o(".user__icon g").css({
                fill: t
            }))
        }

        function v() {
            var t = o(".listing-single-bar");
            o(".tab__nav").on("click", "a", function(e) {
                e.preventDefault();
                var i = o(e.currentTarget),
                    a = i.closest(".tab"),
                    n = a.find(".tab__content"),
                    s = i.attr("href");
                o(s).length && (n.find(".tab__panel").removeClass("active"), o(s).addClass("active"), i.closest(".tab__nav").find("li").removeClass("active"), i.parent().addClass("active")), a.hasClass("listing-single__tab") && t.length && (t.find("li").removeClass("active"), t.find('a[href="' + s + '"]').parent().addClass("active"))
            }), t.on("click", ".tab__nav > li > a", function(t) {
                var e = o(t.target).attr("href");
                o(".listing-single__tab").find('a[href="' + e + '"]').trigger("click")
            }), o("a.switch-tab").on("click", function(t) {
                t.preventDefault();
                var e = o(this).data("trigger");
                void 0 !== e && o(e).trigger("click")
            })
        }

        function w() {
            var t = o(".pricing__content"),
                e = o(".pricing__header"),
                i = 0;
            t.length && (t.each(function() {
                i < o(this).outerHeight() && (i = o(this).outerHeight())
            }), t.css({
                height: i
            }), i = 0, e.each(function() {
                i < o(this).outerHeight() && (i = o(this).outerHeight())
            }), e.css({
                height: i
            }))
        }
        var y = !1;
        o('html[dir="rtl"]').length && (y = !0, o(document).on("vc-full-width-row", function() {
            r()
        }), r()), Math.round10 || (Math.round10 = function(t, e) {
            return l("round", t, e)
        }), o.fn.checkRowHandled = function(t, e) {
            var i = o(this),
                a = new Date;
            if (i.data("watchAt", a.getSeconds()), i.data("watchAt")) {
                var n = parseInt(i.data("watchAt"));
                if (a.getSeconds() - n > 4) return clearTimeout(i.data("watchRow")), o(window).trigger("resize"), o(window).trigger("Wiloke.Resizemap"), !1
            }
            i.data("watchRow", setTimeout(function() {
                "" !== i.attr("style") && void 0 !== i.attr("style") && (i.removeClass(t), clearTimeout(i.data("watchRow")), i.removeData("watchRow"), i.hasClass("is-last-row") && (o(window).trigger("resize"), o(window).trigger("Wiloke.Resizemap")))
            }, e))
        };
        var k = new i,
            $ = function() {
                function i() {
                    t(this, i), this.$masonryCaching = null, this.$masonry = o(".wil_masonry"), this.init()
                }
                return e(i, [{
                    key: "init",
                    value: function() {
                        this.$masonry.length && (this.$masonryWrapper = this.$masonry.parent(), this._oOwnData = this.$masonryWrapper.data(), this.$items = this.$masonry.find(".grid-item"), this.listenResize(), this.getCurrentWindowWidth(), this.setColMdForGridItems(), this.gridItemCalculation(), this.$masonryCaching = this.$masonry.isotope({
                            itemSelector: ".grid-item",
                            masonry: {
                                columnWidth: ".grid-sizer"
                            }
                        }))
                    }
                }, {
                    key: "listenResize",
                    value: function() {
                        var t = this,
                            e = null;
                        o(window).on("resize", function() {
                            if (null === t.$masonryCaching) return !1;
                            null !== e && clearTimeout(e), e = setTimeout(function() {
                                t.getCurrentWindowWidth(), t.gridItemCalculation(), t.setColMdForGridItems(), t.$masonryCaching.isotope("layout"), clearTimeout(e)
                            }, 400)
                        })
                    }
                }, {
                    key: "getCurrentWindowWidth",
                    value: function() {
                        this._windowInnerWidth = o(window).innerWidth()
                    }
                }, {
                    key: "gridItemCalculation",
                    value: function() {
                        var t = !1;
                        this.$items.each(function() {
                            o(this).css("width", "");
                            var e = Math.floor(o(this).outerWidth());
                            if (o(this).css("width", e + "px"), !t) {
                                var i = o(this).parent().children(".wide"),
                                    a = i.outerWidth() / 2;
                                i.css("height", Math.floor(a) + "px"), t = !0
                            }
                        })
                    }
                }, {
                    key: "setColMdForGridItems",
                    value: function() {
                        var t = void 0,
                            e = void 0;
                        this._windowInnerWidth >= 768 && this._windowInnerWidth < 992 ? (t = this._oOwnData.smHorizontal, e = this._oOwnData.smVertical) : this._windowInnerWidth >= 992 && this._windowInnerWidth < 1200 ? (t = this._oOwnData.mdHorizontal, e = this._oOwnData.mdVertical) : this._windowInnerWidth >= 1200 ? (t = this._oOwnData.lgHorizontal, e = this._oOwnData.lgVertical) : (t = this._oOwnData.xsHorizontal, e = this._oOwnData.xsVertical), this.$masonryWrapper.css({
                            "margin-top": -e / 2 + "px",
                            "margin-bottom": -e / 2 + "px",
                            "margin-left": -t / 2 + "px",
                            "margin-right": -t / 2 + "px"
                        }), this.$items.find(".grid-item__content-wrapper").each(function() {
                            o(this).css({
                                margin: e / 2 + "px " + t / 2 + "px",
                                top: e / 2 + "px",
                                bottom: e / 2 + "px",
                                left: t / 2 + "px",
                                right: t / 2 + "px"
                            })
                        })
                    }
                }]), i
            }(),
            L = function() {
                function i() {
                    t(this, i), this.$main = o("#main"), this.$component = this.$main.find(".wiloke-listing-layout"), this.init()
                }
                return e(i, [{
                    key: "init",
                    value: function() {
                        var t = this;
                        this.$component.length && (this.isFirstSearch = !0, this.oText = WILOKE_LISTGO_TRANSLATION, this.$top = this.$main.find(".listgo-listlayout-on-page-template"), this.isNewLoad = !1, this.$navFilter = this.$component.find(".nav-filter, .listgo-dropdown-filter"), this.$navLink = this.$component.find(".nav-links"), this.$loadmoreBtn = this.$component.find(".listgo-loadmore"), this.$gridWrapper = this.$component.find(".listgo-wrapper-grid-items"), this.currentFilter = "all", this.totalPosts = this.$navLink.data("total"), this.aAllFilters = [], this.atts = this.$component.data("atts"), this.postsPerPage = null, this.orderBy = null, this.displayStyle = null, this.aPostIDs = [], this.paged = 1, this.isFocusQuery = !1, this.blockID = this.$component.attr("id"), this.blockCreatedAt = this.$component.attr("createdat"), this.locationIDs = null, this.aListingTags = [], this.searchUnit = "KM", this.method = "", this.xhr = null, this.$complexFormSearch = this.$top.find("#listgo-searchform"), this.$s = this.$complexFormSearch.find("#s_search"), this.s = this.$s.val(), this.orderBy = this.atts.order_by, this.displayStyle = this.atts.display_style, this.postsPerPage = this.atts.posts_per_page, this.$sOpenNow = this.$complexFormSearch.find("#s_opennow"), this.$sUnit = this.$complexFormSearch.find("#s_unit"), this.$sRadius = this.$complexFormSearch.find("#s_radius"), this.searchWithin = this.$sRadius.val(), this.$sLocation = this.$complexFormSearch.find("#s_listing_location"), this.$sCat = this.$complexFormSearch.find("#s_listing_cat"), this.priceSegment = this.$complexFormSearch.find("#s_price_segment").val(), this.isOpenNow = this.$sOpenNow.is(":checked"), this.isHigestRated = this.$complexFormSearch.find("#s_highestrated").is(":checked"), this.latLng = this.$complexFormSearch.find("#s-location-latitude-longitude-id").attr("value"), this.atts = this.$component.data("atts"), this.$sCat.length ? this.aListingCats = this.$sCat.val() : void 0 !== this.atts.listing_cat && (this.aListingCats = this.atts.listing_cat), this.$complexFormSearch.length && this.$complexFormSearch.find("#s-location-term-id").length ? this.aListingLocations = this.$complexFormSearch.find("#s-location-term-id").val() : void 0 !== this.atts.listing_location && (this.aListingLocations = this.atts.listing_location), this.complexSearch(), this.navFilter(), this.pagination(), this.loadmore(), this.fetchPostIDs(), this.sOpenNowConditional(), this.searchOnMobileOnly(), this.currentFilter = this.$navFilter.length > 0 ? this.$navFilter.find(".active").data("filter") : "all", this.$navLink.on("reset_pagination", function() {
                            t.generatePagination()
                        }).trigger("reset_pagination"))
                    }
                }, {
                    key: "searchOnMobileOnly",
                    value: function() {
                        var t = this;
                        o("#listgo-mobile-search-only").on("click", function(e) {
                            e.preventDefault(), t.method = "pagination", t.ajaxLoading()
                        })
                    }
                }, {
                    key: "complexSearch",
                    value: function() {
                        var t = this;
                        this.filterBy = "";
                        var e = null;
                        this.$complexFormSearch.on("ajax_loading", function(i, a) {
                            _.isNull(e) || clearTimeout(e), e = setTimeout(function() {
                                if (t.isNewLoad = !0, t.isFirstSearch) t.isFirstSearch = !1, t.aListingLocations = null !== t.locationIDs ? t.locationIDs : t.$complexFormSearch.find("#s-location-term-id").val(), t.aListingCats = t.$complexFormSearch.find('[name="s_listing_cat[]"]').val(), t.$complexFormSearch.find(".listgo-filter-by-tag:checked").each(function(e, i) {
                                    t.aListingTags.push(o(i).val())
                                }), t.searchUnit = t.$sUnit.val(), t.searchWithin = t.$sRadius.val();
                                else if (void 0 !== a.changed) switch (a.changed) {
                                    case "location":
                                        t.aListingLocations = null !== t.locationIDs ? t.locationIDs : t.$complexFormSearch.find("#s-location-term-id").val();
                                        break;
                                    case "cat":
                                        t.aListingCats = t.$complexFormSearch.find('[name="s_listing_cat[]"]').val();
                                        break;
                                    case "tag":
                                        t.aListingTags = [], t.$complexFormSearch.find(".listgo-filter-by-tag:checked").each(function(e, i) {
                                            t.aListingTags.push(o(i).val())
                                        });
                                        break;
                                    case "s_unit":
                                        t.searchUnit = t.$sUnit.val();
                                        break;
                                    case "s_radius":
                                        t.searchWithin = t.$sRadius.val()
                                }
                                t.isFocusQuery = !0, 1 === t.paged && (t.aPostIDs = []), s.exceptiPad() || (t.method = "pagination", t.ajaxLoading())
                            }, 100)
                        }), this.$complexFormSearch.change(function() {
                            t.aPostIDs = [], t.paged = 1
                        }), this.$complexFormSearch.find(".listgo-filter-by-tag").on("change", function() {
                            t.$complexFormSearch.trigger("ajax_loading", [{
                                changed: "tag"
                            }])
                        }), this.$complexFormSearch.find("#s_listing_cat").on("change", function() {
                            t.s = "", t.$complexFormSearch.trigger("ajax_loading", [{
                                changed: "cat"
                            }])
                        }), this.$complexFormSearch.find("#s_search").on("keywordchanged search_changed", function(e) {
                            t.s = o(e.currentTarget).attr("value"), t.$complexFormSearch.trigger("ajax_loading", [{
                                changed: "search"
                            }])
                        }), this.$sOpenNow.on("change", function(e) {
                            t.isOpenNow = t.$sOpenNow.is(":checked"), t.$complexFormSearch.trigger("ajax_loading", [{
                                changed: "time"
                            }])
                        }), this.$complexFormSearch.find("#s_highestrated").on("change", function(e) {
                            t.isHigestRated = o(e.currentTarget).is(":checked"), t.$complexFormSearch.trigger("ajax_loading", [{
                                changed: "rate"
                            }])
                        }), this.$complexFormSearch.find("#s_price_segment").on("change", function(e) {
                            t.priceSegment = o(e.currentTarget).val(), t.$complexFormSearch.trigger("ajax_loading", [{
                                changed: "price"
                            }])
                        }), this.$s.on("keyup", function(e) {
                            13 !== e.keyCode && (t.aListingCats = [])
                        }), this.$sLocation.on("change", function(e) {
                            t.sOpenNowConditional(), t.locationIDs = null, o("#s_listing_location").data("previous-location", ""), t.$complexFormSearch.trigger("ajax_loading", [{
                                changed: "location"
                            }])
                        }), this.$sLocation.on("location_changed", function(e, i) {
                            t.paged = 1, t.sOpenNowConditional(i), i.is_suggestion ? (t.locationIDs = i.term_id, t.$complexFormSearch.trigger("ajax_loading", [{
                                changed: "location"
                            }]), t.latLng = null) : (t.latLng = i.latLng, t.locationIDs = i.value, t.$complexFormSearch.trigger("ajax_loading", [{
                                changed: "location"
                            }]))
                        }), this.$sRadius.on("change", function(e) {
                            t.$complexFormSearch.trigger("ajax_loading", [{
                                changed: "s_radius"
                            }])
                        }), this.$sUnit.on("change", function(e) {
                            t.$complexFormSearch.trigger("ajax_loading", [{
                                changed: "s_unit"
                            }])
                        })
                    }
                }, {
                    key: "sOpenNowConditional",
                    value: function(t) {
                        void 0 === t && "" === this.$sLocation.val() || void 0 !== t && "" === t.value ? (this.$sOpenNow.prop("disabled", !0), this.$sOpenNow.prop("checked", !1), this.$sOpenNow.closest("label").attr("data-tooltip", this.oText.requirelocation)) : (this.$sOpenNow.prop("disabled", !1), this.$sOpenNow.closest("label").removeAttr("data-tooltip"))
                    }
                }, {
                    key: "ajaxLoading",
                    value: function(t) {
                        var e = this;
                        null !== this.xhr && 200 !== this.xhr.status && this.xhr.abort(), this.preloader(), t = _.isUndefined(t) || _.isNaN(t) ? this.postsPerPage : t;
                        var i = {
                            post__not_in: this.aPostIDs,
                            action: "wiloke_loadmore_listing_layout",
                            posts_per_page: t,
                            listing_locations: this.aListingLocations,
                            latLng: this.latLng,
                            listing_tags: this.aListingTags,
                            listing_cats: this.aListingCats,
                            get_posts_from: null,
                            is_focus_query: this.isFocusQuery,
                            is_open_now: this.isOpenNow,
                            is_highest_rated: this.isHigestRated,
                            price_segment: this.priceSegment,
                            paged: this.paged,
                            customerUTCTimezone: this.getVisitorTimezone(),
                            s: this.s,
                            displayStyle: this.displayStyle,
                            sUnit: this.searchUnit,
                            sWithin: this.searchWithin,
                            atts: this.atts,
                            currentPageID: WILOKE_GLOBAL.postID
                        };
                        this.$gridWrapper.find(".wiloke-notfound-wrapper").remove(), this.xhr = o.ajax({
                            type: "POST",
                            url: WILOKE_GLOBAL.ajaxurl,
                            data: i,
                            success: function(t) {
                                t.success ? (e.totalPosts = parseInt(t.data.total, 10), e.addNewPost(t.data.content, e.method), e.fetchPostIDs(), "loadmore" === e.method && ("*" === e.currentFilter || "all" === e.currentFilter ? e.totalPosts === e.$gridWrapper.find(".wiloke-listgo-listing-item").length && e.$loadmoreBtn.remove() : e.totalPosts === e.$gridWrapper.find("." + e.currentFilter).length && e.$loadmoreBtn.addClass("hidden"))) : (e.totalPosts = 0, "pagination" !== e.method && ("all" === e.currentFilter || ("*" !== e.currentFilter || e.$gridWrapper.find(".wiloke-listgo-listing-item").length) && e.$gridWrapper.find("." + e.currentFilter).length) || e.addNewPost('<div class="wiloke-notfound-wrapper col-xs-12"><div class="wil-alert wil-alert-has-icon alert-danger"><span class="wil-alert-icon"><i class="icon_box-checked"></i></span><p class="wil-alert-message">' + [k.notFound()] + "</p></div></div>", "html"), e.$loadmoreBtn.addClass("hidden")), e.preloader("loaded"), e.isNewLoad = !1, e.method = ""
                            }
                        })
                    }
                }, {
                    key: "getVisitorTimezone",
                    value: function() {
                        if (this.visitorUTCTimezone) return this.visitorUTCTimezone;
                        var t = -(new Date).getTimezoneOffset() / 60,
                            e = "";
                        return e = t > 0 ? "UTC+" : "UTC-", this.visitorUTCTimezone = e + t, this.visitorUTCTimezone
                    }
                }, {
                    key: "preloader",
                    value: function(t) {
                        var e = this.$gridWrapper;
                        "loadmore" === this.displayStyle && "" === this.s && !1 === this.isNewLoad && (e = this.$loadmoreBtn), "loaded" === t ? e.removeClass("loading") : e.addClass("loading")
                    }
                }, {
                    key: "addNewPost",
                    value: function(t, e) {
                        var i = "";
                        i = _.isArray(t) ? t.join("") : _.map(t, function(t) {
                            return t
                        }).join("");
                        var a = o(i);
                        "loadmore" === this.method ? this.$gridWrapper.data("isotope") ? this.$gridWrapper.append(a).isotope("appended", a) : this.$gridWrapper.append(a) : this.$gridWrapper.data("isotope") ? (this.$gridWrapper.isotope("remove", this.$gridWrapper.isotope("getItemElements")), this.$gridWrapper.append(a).isotope("appended", a), this.$gridWrapper.isotope("layout")) : this.$gridWrapper.html(a), this.$gridWrapper.find(".lazy").Lazy(), this.$navLink.trigger("reset_pagination"), this.$component.trigger("recheck_loadmore"), this.$component.trigger("ajax_completed"), this.isFocusQuery = !1, this.$gridWrapper.find('.listing[class*="listing--grid"]').WilokeNiceGridTitle()
                    }
                }, {
                    key: "fetchPostIDs",
                    value: function() {
                        var t = this;
                        this.aPostIDs = [], this.$gridWrapper.find(".wiloke-listgo-listing-item").each(function(e, i) {
                            var a = o(i).data("postid");
                            t.aPostIDs.push(a.toString())
                        })
                    }
                }, {
                    key: "generatePagination",
                    value: function() {
                        this.$complexFormSearch.find("#s_search").val();
                        var t = new a(this.totalPosts, this.postsPerPage, this.paged);
                        this.$navLink.html(t.createPagination()), 0 == this.totalPosts ? this.$navLink.addClass("hidden") : this.$navLink.removeClass("hidden")
                    }
                }, {
                    key: "navFilter",
                    value: function() {
                        var t = this;
                        this.filterBy = this.$navFilter.data("filterby"), this.$navFilter.hasClass("listgo-dropdown-filter") ? (this.$navFilter.on("change", function(e) {
                            t.handleFilter(o(e.target).find("option:selected"))
                        }), this.showResultOnDropdownFilter()) : this.$navFilter.find("a").on("click", function(e) {
                            e.preventDefault(), t.handleFilter(o(e.target))
                        })
                    }
                }, {
                    key: "showResultOnDropdownFilter",
                    value: function() {
                        var t = this,
                            e = "",
                            i = this.$component.find(".listing__result-right"),
                            a = i.data("result"),
                            n = i.data("singularres"),
                            s = i.data("pluralres");
                        a = a.replace("*open_result*", "<span><ins>"), a = a.replace("*end_result*", "</ins></span>"), a = a.replace("%total_listing%", this.$gridWrapper.data("total")), this.$component.on("ajax_completed", function() {
                            var o = ".wiloke-listgo-listing-item",
                                r = a;
                            "all" !== t.currentFilter && (o += "." + t.currentFilter);
                            var l = t.$gridWrapper.find(o).length;
                            e = r > 1 ? n : s, r = (r = r.replace("RESULT_TEXT_HERE", e)).replace("%found_listing%", l), i.html(r)
                        })
                    }
                }, {
                    key: "getAllTerms",
                    value: function() {
                        if (this.aAllFilters.length) return this.aAllFilters;
                        var t = this;
                        return this.$navFilter.find("a").each(function() {
                            var e = o(this).data("filter");
                            "all" !== e && t.aAllFilters.push(e)
                        }), this.aAllFilters
                    }
                }, {
                    key: "handleFilter",
                    value: function(t) {
                        t.siblings(".active").removeClass("active"), t.addClass("active"), this.paged = 1, this.currentFilter = t.data("filter");
                        var e = !1;
                        if ("all" !== this.currentFilter) {
                            this.$gridWrapper.isotope({
                                filter: "." + this.currentFilter
                            });
                            var i = this.$gridWrapper.find(".wiloke-listgo-listing-item." + this.currentFilter).length;
                            "listing_location" === this.filterBy ? (this.aListingLocations = [], this.aListingLocations.push(this.currentFilter)) : (this.aListingCats = [], this.aListingCats = this.currentFilter, this.aListingCats.push(this.currentFilter)), i < t.data("total") && i < this.postsPerPage && (e = !0, this.method = "loadmore", this.ajaxLoading(this.postsPerPage - i))
                        } else {
                            var a = this.getAllTerms();
                            "listing_location" === this.filterBy ? (this.aListingLocations = [], this.aListingLocations = a) : (this.aListingCats = [], this.aListingCats = a), this.$component.find(".listgo-wrapper-grid-items").isotope({
                                filter: "*"
                            })
                        }
                        e || this.$component.trigger("ajax_completed"), this.getCurrentTermInfo(), this.$component.trigger("recheck_loadmore")
                    }
                }, {
                    key: "pagination",
                    value: function() {
                        var t = this;
                        this.$component.on("click", "a.page-numbers", function(e) {
                            e.preventDefault();
                            var i = o(e.currentTarget);
                            if (i.hasClass("current")) return !1;
                            o("html, body").animate({
                                scrollTop: t.$component.offset().top + 100
                            }, 600), i.hasClass("next") ? i = t.$navLink.find(".page-numbers.current").next() : i.hasClass("prev") && (i = t.$navLink.find(".page-numbers.current").prev()), t.paged = i.data("page"), t.fetchPostIDs(), i.siblings(".page-numbers").removeClass("current"), i.addClass("current"), t.$component.trigger("search_handle"), t.method = "pagination", t.ajaxLoading()
                        })
                    }
                }, {
                    key: "loadmore",
                    value: function() {
                        var t = this;
                        this.$component.on("recheck_loadmore", function() {
                            var e = 0,
                                i = ".grid-item";
                            "all" === t.currentFilter ? e = t.$gridWrapper.data("total") : (e = t.$navFilter.find(".active").data("total"), i += "." + t.currentFilter), e === t.$gridWrapper.find(i).length ? "all" !== t.currentFilter ? t.$loadmoreBtn.addClass("hidden") : t.$loadmoreBtn.remove() : t.$loadmoreBtn.removeClass("hidden")
                        }), this.$loadmoreBtn.on("click", function(e) {
                            e.preventDefault(), t.method = "loadmore", t.paged++, t.ajaxLoading()
                        })
                    }
                }, {
                    key: "getCurrentTermInfo",
                    value: function() {
                        this.totalPosts = this.$navFilter.find(".active").data("total"), "all" !== this.currentFilter ? this.currentPosts = this.$component.find(".grid-item." + this.currentFilter).length : this.currentPosts = this.$component.find(".grid-item").length, this.$navLink.trigger("reset_pagination")
                    }
                }]), i
            }(),
            b = function() {
                function i() {
                    t(this, i), this.$app = o("#s_listing_location"), this.init()
                }
                return e(i, [{
                    key: "parseSuggestion",
                    value: function() {
                        var t = this,
                            e = this.$suggestLocation.val();
                        "" !== e && void 0 !== e && (e = o.parseJSON(e), _.each(e, function(e) {
                            t.aSuggestion.push({
                                value: e.name,
                                label: e.name,
                                term_id: e.term_id
                            }), t.aListOfLocationName.push(e.name)
                        }))
                    }
                }, {
                    key: "init",
                    value: function() {
                        if (!this.$app.length) return !1;
                        this.$suggestLocation = o("#s-listing-location-suggestion"), this.$latLng = o("#s-location-latitude-longitude-id"), this.$termID = o("#s-location-term-id"), this.ggAutoCompleteService = this.ggGeoCode = null, this.$app.hasClass("auto-location-by-google") && (this.ggAutoCompleteService = "undefined" != typeof google && void 0 !== google.maps.places ? new google.maps.places.AutocompleteService : null, this.ggGeoCode = "undefined" != typeof google ? new google.maps.Geocoder : null), console.log(this.ggAutoCompleteService), this.aSuggestion = [], this.aListOfLocationName = [], this.aLocations = [], this.delay = !1, this.parseSuggestion(), this.autoComplete()
                    }
                }, {
                    key: "inLocationSuggestion",
                    value: function(t) {
                        return -1 !== this.aListOfLocationName.indexOf(t)
                    }
                }, {
                    key: "autoComplete",
                    value: function() {
                        var t = this;
                        this.$app.autocomplete({
                            source: function(e, i) {
                                var a = e.term;
                                if (0 === a.length || t.inLocationSuggestion(a)) return !_.isEmpty(t.aSuggestion) && (i(t.aSuggestion), !1);
                                var n = t;
                                return !(a.length < 2) && (null !== t.ggAutoCompleteService && void t.ggAutoCompleteService.getPlacePredictions({
                                    input: e.term,
                                    componentRestrictions: {
                                        country: WILOKE_GLOBAL.countryRestriction
                                    }
                                }, function(t, e) {
                                    e === google.maps.places.PlacesServiceStatus.OK && (n.aLocations = _.map(t, function(t) {
                                        return {
                                            label: t.description,
                                            value: t.description,
                                            real_value: t.structured_formatting.main_text,
                                            details: t,
                                            placeID: t.place_id
                                        }
                                    }), i(n.aLocations))
                                }))
                            },
                            minLength: 0,
                            focus: function(e, i) {
                                if (s.Any()) {
                                    if (t.$app.attr("value", i.item.label), _.isUndefined(i.item.term_id)) {
                                        t.delay = !0;
                                        var a = null;
                                        t.$termID.attr("value", ""), null !== t.ggGeoCode && t.ggGeoCode.geocode({
                                            placeId: i.item.placeID
                                        }, function(e, n) {
                                            "OK" === n ? (a = Math.round10(e[0].geometry.location.lat(), -4) + "," + Math.round10(e[0].geometry.location.lng(), -4), t.$latLng.attr("value", a), t.$app.trigger("location_changed", [{
                                                is_suggestion: !1,
                                                value: i.item.real_value,
                                                placeID: i.item.placeID,
                                                details: i.item.details,
                                                latLng: a
                                            }])) : (t.$latLng.attr("value", ""), t.$app.trigger("location_changed", [{
                                                is_suggestion: !1,
                                                value: i.item.real_value,
                                                placeID: i.item.placeID,
                                                details: i.item.details,
                                                latLng: a
                                            }]))
                                        })
                                    } else t.$latLng.attr("value", ""), t.$termID.attr("value", i.item.term_id), t.$app.trigger("location_changed", [{
                                        is_suggestion: !0,
                                        value: i.item.value,
                                        term_id: i.item.term_id,
                                        latLng: i.item.latLng
                                    }]);
                                    o(".ui-menu").hide()
                                }
                            },
                            select: function(e, i) {
                                if (i.item.value === t.$app.data("previous-location")) return !1;
                                if (_.isUndefined(i.item.term_id)) {
                                    t.delay = !0;
                                    var a = null;
                                    t.$termID.attr("value", ""), null !== t.ggGeoCode && t.ggGeoCode.geocode({
                                        placeId: i.item.placeID
                                    }, function(e, n) {
                                        "OK" === n ? (a = Math.round10(e[0].geometry.location.lat(), -4) + "," + Math.round10(e[0].geometry.location.lng(), -4), t.$latLng.attr("value", a), t.$app.trigger("location_changed", [{
                                            is_suggestion: !1,
                                            value: i.item.real_value,
                                            placeID: i.item.placeID,
                                            details: i.item.details,
                                            latLng: a
                                        }])) : t.$app.trigger("location_changed", [{
                                            is_suggestion: !1,
                                            value: i.item.real_value,
                                            placeID: i.item.placeID,
                                            details: i.item.details,
                                            latLng: a
                                        }])
                                    })
                                } else t.$latLng.attr("value", ""), t.$termID.attr("value", i.item.term_id), t.$app.trigger("location_changed", [{
                                    is_suggestion: !0,
                                    value: i.item.value,
                                    term_id: i.item.term_id,
                                    latLng: i.item.latLng
                                }]), t.delay = !1;
                                t.$app.data("previous-location", i.item.value);
                                var n = setTimeout(function() {
                                    clearTimeout(n), t.$app.trigger("blur")
                                }, 400)
                            }
                        }).bind("focus", function() {
                            o(this).autocomplete("search")
                        }).bind("blur", function(e) {
                            var i = t;
                            "" == o(e.currentTarget).val() && (i.$latLng.attr("value", ""), i.$termID.attr("value", ""), i.$app.trigger("location_changed", [{
                                placeID: "",
                                latLng: ""
                            }]))
                        })
                    }
                }]), i
            }(),
            C = function() {
                function i(e) {
                    t(this, i), this.oResults = {}, this.oLatLng = {}, this.address = "", this.isFocusSetLocationField = e, this.askForPosition()
                }
                return e(i, [{
                    key: "askForPosition",
                    value: function() {
                        var t = this;
                        if (!o("#s_listing_location").length && !o("#listing-single_listing_near_by").length) return !1;
                        localStorage.getItem("listgo_mylocation"), localStorage.getItem("listgo_address"), localStorage.getItem("listgo_mylocation_created_at");
                        navigator.geolocation && navigator.geolocation.getCurrentPosition(function(e) {
                            t.detectingPosition(e)
                        })
                    }
                }, {
                    key: "cachingPosition",
                    value: function() {
                        var t = new Date;
                        localStorage.setItem("listgo_adress", this.address), localStorage.setItem("listgo_mygeocode", JSON.stringify(this.oResults)), localStorage.setItem("listgo_mylocation_created_at", t.getMinutes())
                    }
                }, {
                    key: "findYourLocationInSearchForm",
                    value: function() {
                        var t = o("#s_listing_location");
                        if (t.length && (this.isFocusSetLocationField || "" === t.val() && o(".is-saprated-searchform").length)) {
                            t.attr("value", this.address);
                            var e = o("#s-location-latitude-longitude-id"),
                                i = o("#s-location-term-id");
                            e.attr("value", this.oLatLng.lat + "," + this.oLatLng.lng), i.attr("value", ""), t.trigger("location_changed", [{
                                latLng: this.oLatLng.lat + "," + this.oLatLng.lng,
                                is_suggestion: !1,
                                term_id: ""
                            }]), o("body").trigger("wiloke_listgo_user_latlng", [this.oLatLng])
                        }
                    }
                }, {
                    key: "detectingPosition",
                    value: function(t) {
                        var e = this;
                        this.oLatLng = {
                            lat: parseFloat(t.coords.latitude),
                            lng: parseFloat(t.coords.longitude)
                        }, o("body").trigger("wiloke_listgo_user_latlng", [this.oLatLng]), (new google.maps.Geocoder).geocode({
                            location: this.oLatLng
                        }, function(t, i) {
                            "OK" === i && t[0] && (o("body").trigger("wiloke_listgo_got_location", [t]), e.address = t[0].formatted_address, e.findYourLocationInSearchForm(), e.oResults = t, e.cachingPosition())
                        })
                    }
                }]), i
            }(),
            x = function() {
                function i(e) {
                    t(this, i), this.$searchForm = o("#listgo-searchform, #listgo-event-searchfrom"), this.$search = this.$searchForm.find("#s_search"), this.$hiddenOriginalSuggestion = this.$searchForm.find("#wiloke-original-search-suggestion"), this.$location = this.$searchForm.find("#s_listing_location"), this.$category = this.$searchForm.find("#s_listing_cat"), this.$tag = this.$searchForm.find('[name="s_listing_tag[]"]'), this.$sTermID = this.$searchForm.find("#s-location-term-id"), this.$sPlaceID = this.$searchForm.find("#s-location-place-id"), this.$sLatLng = this.$searchForm.find("#s-location-latitude-longitude-id"), this.$isHeroSc = this.$searchForm.find("#wiloke-is-hero-sc"), this.oCache = {}, this.aSuspects = {}, this.aListings = {}, this.oOriginalSuggestion = {}, this.aAvailableTags = [], this.latestSelected = null, this.reCheck = !1, this.isShowingCats = !1, this.oListingsInfo = {}, this.xhr = null, this.instMap = e, this.init()
                }
                return e(i, [{
                    key: "subscribeTaxonomy",
                    value: function() {
                        var t = this;
                        this.$location.on("change", function() {
                            t.reCheck = !0
                        }), this.$category.on("change", function() {
                            t.reCheck = !0
                        }), this.$tag.on("change", function() {
                            t.reCheck = !0
                        })
                    }
                }, {
                    key: "renderNestedCategories",
                    value: function(t, e) {
                        var i = "";
                        e = void 0 === e ? 0 : e;
                        var a = "" !== t.icon ? '<img src="' + t.icon + '">' : "";
                        if (i += '<li class="ui-menu-item wil-search-suggestion-item wiloke-cat-level-' + e + '" data-value="' + t.name + '"><div class="ui-menu-item-wrapper">' + a + '<div class="label-and-info">' + t.name + "</div></div></li>", !_.isEmpty(t.children))
                            for (var n in t.children) {
                                var s = t.children[n];
                                _.isEmpty(s.children) ? (a = "" !== s.icon ? '<img src="' + s.icon + '">' : "", i += '<li class="ui-menu-item wil-search-suggestion-item wiloke-cat-level-' + e + '" data-value="' + s.name + '"><div class="ui-menu-item-wrapper">' + a + '<div class="label-and-info">' + s.name + "</div></div></li>") : i += this.renderNestedCategories(s, e), e++
                            }
                        return i
                    }
                }, {
                    key: "blackenSearchFieldWhenFocus",
                    value: function() {
                        this.$search.on("focus", function() {
                            o(this).select(), this.setSelectionRange(0, 9999)
                        }), this.$location.on("focus", function() {
                            o(this).select(), this.setSelectionRange(0, 9999)
                        })
                    }
                }, {
                    key: "init",
                    value: function() {
                        var t = this;
                        if (!this.$search.length) return !1;
                        this.blackenSearchFieldWhenFocus(), this.instMap && this.instMap.$container.on("reset_listing", function() {
                            _.isEmpty(t.instMap.aListings) || (t.reCheck = !0, t.aListings = _.map(t.instMap.aListings, function(t) {
                                return t.title
                            }))
                        }), this.$hiddenOriginalSuggestion.length && "" !== this.$hiddenOriginalSuggestion.val() && (this.oOriginalSuggestion = o.parseJSON(this.$hiddenOriginalSuggestion.val())), this.subscribeTaxonomy(), this.$search.autocomplete({
                            at: "left bottom",
                            minLength: 0,
                            create: function(e) {
                                o(e.target).data("ui-autocomplete")._renderItem = function(e, i) {
                                    var a = "";
                                    if (t.isShowingCats) {
                                        var n = "" !== i.icon ? '<img src="' + i.icon + '">' : "",
                                            s = '<li class="ui-menu-item wil-search-suggestion-item" data-value="' + i.name + '"><div class="ui-menu-item-wrapper">' + n + '<div class="label-and-info">' + i.name + "</div></div></li>";
                                        return o(s).appendTo(e)
                                    }
                                    return void 0 !== i.isFalse && i.isFalse ? o('<li class="ui-menu-item wil-search-suggestion-item">').append('<div class="ui-menu-item-wrapper"><div class="label-and-info">' + i.label + "</div></div>").appendTo(e) : (i.full.listing_settings && (a = '<span class="more-info">' + i.full.listing_settings.map.location + "</span>"), _.isUndefined(i.full.first_cat_info) || !i.full.first_cat_info || "" === i.full.first_cat_info.map_marker_image ? o('<li class="ui-menu-item wil-search-suggestion-item">').append('<div class="ui-menu-item-wrapper"><div class="label-and-info">' + i.label + a + "</div></div>").appendTo(e) : o('<li class="ui-menu-item wil-search-suggestion-item">').append('<div class="ui-menu-item-wrapper"><img src="' + i.full.first_cat_info.map_marker_image + '"><div class="label-and-info">' + i.label + a + "</div></div>").appendTo(e))
                                }
                            },
                            open: function(e, i) {
                                var a = t.$search.data("ui-autocomplete").menu.element,
                                    n = t.$search.innerWidth();
                                a.width(n)
                            },
                            source: function(e, i) {
                                var a = t.$category.val(),
                                    n = t.$tag.val(),
                                    s = t.$location.val(),
                                    r = e.term.toLowerCase(),
                                    l = t.$search.closest(".input-text");
                                return 0 === r.length || (void 0 === t.$search.data("is-disable-suggestion") || !1 === t.$search.data("is-disable-suggestion")) && (_.isUndefined(t.oCache[r]) || _.isEmpty(t.oCache[r])) ? !_.isEmpty(t.oOriginalSuggestion) && (l.removeClass("loading"), t.isShowingCats = !0, i(t.oOriginalSuggestion), !1) : t.$search.closest("form").hasClass("listing-template") ? (t.$search.autocomplete("close"), t.latestSelected !== r && t.$search.trigger("search_changed"), !1) : r !== t.$search.data("latest-search") && !t.$searchForm.hasClass("listgo-search-on-map") && (t.isShowingCats = !1, _.isEmpty(t.oCache) || t.reCheck || _.isUndefined(t.oCache[r]) ? (null !== t.xhr && 200 !== t.xhr.status && t.xhr.abort(), !t.$search.hasClass("disabled-autocomplete-ajax") && void(t.xhr = o.ajax({
                                    type: "GET",
                                    url: WILOKE_GLOBAL.ajaxurl,
                                    beforeSend: function(t) {
                                        l.addClass("loading")
                                    },
                                    data: {
                                        action: "wiloke_search_suggestion",
                                        s: r,
                                        listing_locations: s,
                                        listing_tags: n,
                                        listing_cats: a,
                                        security: WILOKE_GLOBAL.wiloke_nonce,
                                        location_place_id: t.$sPlaceID.attr("value"),
                                        location_term_id: t.$sTermID.val(),
                                        latLng: t.$sLatLng.val(),
                                        is_hero_sc: t.$isHeroSc.length
                                    },
                                    success: function(e) {
                                        if (e.success) {
                                            t.oListingsInfo = e.data;
                                            var a = _.map(e.data, function(t) {
                                                return {
                                                    label: t.title,
                                                    value: t.title,
                                                    full: t
                                                }
                                            });
                                            t.oCache[r] = a, i(a), t.reCheck = !1
                                        } else {
                                            t.oListingsInfo = e.data;
                                            var n = [{
                                                label: e.data.message,
                                                isFalse: !0,
                                                value: ""
                                            }];
                                            i(n), t.reCheck = !1
                                        }
                                        l.removeClass("loading")
                                    }
                                }))) : (i(t.oCache[r]), !1))
                            },
                            select: function(e, i) {
                                s.Any() || (t.$search.data("is-disable-suggestion", !1), t.$search.data("latest-search", i.item.value), t.$search.attr("value", i.item.label), t.latestSelected = i.item.value, void 0 !== i.item.taxonomy && "listing_cat" === i.item.taxonomy ? (t.$category.attr("value", i.item.term_id), o("#cache_previous_search").attr("value", i.item.name), t.$category.trigger("change"), t.$search.trigger("blur")) : (t.$category.val(""), t.$search.trigger("change", i.item.label), o(e.target).closest("form").parent().hasClass("is-saprated-searchform") && (o(e.target).val(i.item.label), window.location.href = i.item.full.link), t.$search.val(i.item.label), t.$search.trigger("keywordchanged")))
                            },
                            focus: function(e, i) {
                                s.Any() && (t.$search.attr("value", i.item.label), void 0 !== i.item.taxonomy && "listing_cat" === i.item.taxonomy ? (t.$category.attr("value", i.item.term_id), o("#cache_previous_search").attr("value", i.item.name), t.$category.trigger("change"), t.$search.trigger("blur")) : (t.$category.val(""), t.$search.trigger("change", i.item.label), o(e.target).closest("form").parent().hasClass("is-saprated-searchform") && (o(e.target).val(i.item.label), window.location.href = i.item.full.link), t.$search.val(i.item.label)), o(".ui-menu").hide())
                            }
                        }).bind("focus", function() {
                            return o(this).autocomplete("search"), o(this).data("is-disable-suggestion", !0), this
                        }).on("blur", function(t) {
                            if ("" === o(this).attr("value")) {
                                var e = o("#s_listing_cat");
                                o("#cache_previous_search").attr("value", ""), e.attr("value", ""), e.trigger("change"), o(this).data("triggerAllowable") && (o(this).data("triggerAllowable", !1), o(this).on("keypress", function() {
                                    o(this).data("triggerAllowable", !0), o(this).off("keypress")
                                }))
                            }
                            o(this).data("is-disable-suggestion", !1)
                        }).one("keypress", function(t) {
                            o(this).data("triggerAllowable", !0), o(this).hasClass("disabled-autocomplete-ajax") && o(".ui-menu-item").hide()
                        })
                    }
                }]), i
            }(),
            I = function() {
                function i() {
                    t(this, i), this.$main = o("#main"), this.$favorite = this.$main.find(".js_favorite"), this.handle(), this.removeFavorite()
                }
                return e(i, [{
                    key: "handle",
                    value: function() {
                        this.$main.on("click", ".js_favorite", function(t) {
                            if (t.preventDefault(), "no" === WILOKE_GLOBAL.isLoggedIn) return o(".header__user").find(".user__icon").trigger("click"), !1;
                            var e = o(t.currentTarget);
                            e.toggleClass("active"), _.isNull(null) || 4 === null.status || null.abort(), o.ajax({
                                url: WILOKE_GLOBAL.ajaxurl,
                                type: "POST",
                                data: {
                                    action: "wiloke_toggle_favorite_list",
                                    ID: e.data("postid"),
                                    security: WILOKE_GLOBAL.wiloke_nonce
                                },
                                success: function(t) {}
                            })
                        })
                    }
                }, {
                    key: "removeFavorite",
                    value: function() {
                        o(document).on("click", ".js-remove-favorite", function(t) {
                            t.preventDefault();
                            var e = o(t.currentTarget);
                            e.closest(".f-listings-item").remove(), o.ajax({
                                url: WILOKE_GLOBAL.ajaxurl,
                                type: "POST",
                                data: {
                                    action: "wiloke_remove_favorite",
                                    ID: e.data("postid"),
                                    security: WILOKE_GLOBAL.wiloke_nonce
                                }
                            })
                        })
                    }
                }]), i
            }(),
            O = function() {
                function i(e) {
                    t(this, i), this.$trigger = e, this.init()
                }
                return e(i, [{
                    key: "template",
                    value: function(t, e) {
                        return _.template("<li class='gallery-item bg-scroll' data-id='<%- id %>' style='background-image: url(<%- backgroundUrl %>)'><span class='wil-addlisting-gallery__list-remove'>Remove</span></li>")({
                            backgroundUrl: t,
                            id: e
                        })
                    }
                }, {
                    key: "init",
                    value: function() {
                        var t = this;
                        this.$trigger.length && this.$trigger.on("click", function(e) {
                            var i = o(e.currentTarget);
                            e.preventDefault();
                            var a = i.data("multiple");
                            if (i.data("frame")) return i.data("frame").open(), !1;
                            var n = void 0 !== i.data("imgsize") ? i.data("imgsize") : "thumbnail",
                                s = wp.media({
                                    title: "",
                                    button: {
                                        text: "Select"
                                    },
                                    multiple: a
                                });
                            i.data("frame", s), i.data("frame").on("select", function() {
                                if (a) {
                                    var e = i.data("frame").state().get("selection").toJSON(),
                                        s = "";
                                    _.forEach(e, function(e) {
                                        var i = !_.isUndefined(e.sizes[n]) && e.sizes[n] ? e.sizes[n].url : e.url;
                                        s += t.template(i, e.id)
                                    }), i.parent().before(s)
                                } else {
                                    var r = i.data("frame").state().get("selection").first().toJSON(),
                                        l = !_.isUndefined(r.sizes[n]) && r.sizes[n] ? r.sizes[n].url : r.url;
                                    i.find(".wiloke-preview").attr("src", l).removeClass("hidden"), i.find(".wiloke-insert-id").val(r.id), i.find("#wiloke-avatar-by-text").addClass("hidden"), i.hasClass("profile-background") && o(".header-page").css("background-image", "url(" + r.url + ")")
                                }
                            }), i.data("frame").open()
                        })
                    }
                }]), i
            }(),
            S = function() {
                function i() {
                    t(this, i), this.$notifications = o("#wiloke-notifications"), this.filterBy = "all", this.init()
                }
                return e(i, [{
                    key: "init",
                    value: function() {
                        var t = this;
                        o(window).load(function() {
                            setTimeout(function() {
                                t.$notifications.length && (t.totalNewFeed = 0, t.$countNewFeeds = t.$notifications.find(".count"), t.$showLists = t.$notifications.find(".notifications__list"), t.xhrLastCheck = null, t.xhrFetchNotification = null, t.updateLastCheck(), t.fetchNotification())
                            }, 3e3)
                        }), this.infiniteLoadMoreNotification(), this.filterReview(), this.removeNotification(), this.dismissNotification()
                    }
                }, {
                    key: "updateLastCheck",
                    value: function() {
                        var t = this;
                        this.$notifications.on("click", ".notifications__icon", function(e) {
                            0 === t.totalNewFeed || "" === t.totalNewFeed || o(e.currentTarget).data("clicked") || (null !== t.xhrLastCheck && 200 !== t.xhrLastCheck.status && t.xhrLastCheck.abort(), t.xhrLastCheck = o.ajax({
                                type: "POST",
                                url: WILOKE_GLOBAL.ajaxurl,
                                data: {
                                    action: "wiloke_listgo_update_lastcheck_notification",
                                    security: WILOKE_GLOBAL.wiloke_nonce
                                },
                                success: function(i) {
                                    t.$countNewFeeds.html(""), t.totalNewFeed = 0, o(e.currentTarget).data("clicked", !0)
                                }
                            }))
                        })
                    }
                }, {
                    key: "fetchNotification",
                    value: function() {
                        var t = this;
                        if (void 0 !== WILOKE_GLOBAL.isDisableFetchNotification && "yes" === WILOKE_GLOBAL.isDisableFetchNotification) return "";
                        null !== this.xhrFetchNotification && 200 !== this.xhrFetchNotification.status && this.xhrFetchNotification.abort(), this.$showLists.addClass("loading"), this.xhrFetchNotification = o.ajax({
                            type: "POST",
                            url: WILOKE_GLOBAL.ajaxurl,
                            data: {
                                action: "wiloke_listgo_update_fetch_notifications",
                                security: WILOKE_GLOBAL.wiloke_nonce,
                                user_id: this.$notifications.data("userid")
                            },
                            success: function(e) {
                                e.success && (0 === e.data.countnew && (e.data.countnew = ""), t.totalNewFeed = e.data.countnew, t.$countNewFeeds.html(e.data.countnew), t.$showLists.html(e.data.notifications), e.data.is_empty || t.$showLists.next().removeClass("hidden")), t.$showLists.removeClass("loading")
                            }
                        })
                    }
                }, {
                    key: "infiniteLoadMoreNotification",
                    value: function() {
                        var t = [],
                            e = o("#wiloke-show-notifications"),
                            i = e.data("cursor"),
                            a = o("#wiloke-loadmore-notifications"),
                            n = a.html();
                        if (a.data("loadall")) return !1;
                        a.on("click", function(s) {
                            if (s.preventDefault(), "hide" === this.filterBy) return !1;
                            var r = this;
                            e.find("li.notification-item").each(function() {
                                t.push(o(this).data("objectid"))
                            }), a.html("Loading..."), o.ajax({
                                type: "GET",
                                url: WILOKE_GLOBAL.ajaxurl + "?action=fetch_more_notifications&posts__not_in=" + t + "&cursor=" + i + "&security=" + WILOKE_GLOBAL.wiloke_nonce + "&filter_by=" + this.filterBy,
                                success: function(t) {
                                    a.html(n), t.success ? (i = t.data.cursor, e.append(t.data.notifications), r.showFilterResult()) : (a.data("loadall", !0), a.remove()), e.removeClass("loading")
                                }
                            })
                        }), e.length && e.find("li.notification-item").length !== o(".wiloke-notifications-wrapper").data("total") && a.removeClass("hidden")
                    }
                }, {
                    key: "filterReview",
                    value: function() {
                        var t = this,
                            e = o("#filter_by_review"),
                            i = o("#filter_by_listing");
                        o("#wiloke-filter-notifications").on("change", function() {
                            var a = e.is(":checked"),
                                n = i.is(":checked");
                            t.filterBy = a || n ? !n && a ? "review" : n && !a ? "listing" : "all" : "hide", t.showFilterResult()
                        })
                    }
                }, {
                    key: "showFilterResult",
                    value: function() {
                        var t = o("#wiloke-show-notifications");
                        "review" === this.filterBy ? (t.find('li.notification-item[data-type="review"]').show(), t.find('li.notification-item[data-type!="review"]').hide()) : "listing" === self.filterBy ? (t.find('li.notification-item[data-type!="review"]').show(), t.find('li.notification-item[data-type!="review"]').show()) : t.find("li.notification-item").show()
                    }
                }, {
                    key: "removeNotification",
                    value: function() {
                        var t = o(".wiloke-notifications-wrapper");
                        o("#wiloke-show-notifications").on("click", ".notifications__remove", function() {
                            var e = o(this).closest("li.notification-item"),
                                i = parseInt(t.data("total"), 10) - 1,
                                a = e.data("objectid");
                            o.ajax({
                                type: "POST",
                                url: WILOKE_GLOBAL.ajaxurl,
                                data: {
                                    action: "remove_more_notifications",
                                    object_ID: a,
                                    security: WILOKE_GLOBAL.wiloke_nonce
                                },
                                success: function(t) {
                                    t.success ? (e.remove(), o(".wiloke-notifications-count").html("(" + i + ")")) : alert(t.data)
                                }
                            })
                        })
                    }
                }, {
                    key: "dismissNotification",
                    value: function() {
                        var t = null;
                        o(".wil-alert-remove").on("click", function(e) {
                            e.preventDefault(), null !== t && 200 !== t.status && t.abort();
                            var i = o(e.currentTarget),
                                a = o.parseJSON(WILOKE_GLOBAL.userInfo),
                                n = void 0 !== a.user_id ? a.user_id : "";
                            i.closest(".wil-alert").fadeOut(), t = o.ajax({
                                type: "POST",
                                url: WILOKE_GLOBAL.ajaxurl,
                                data: {
                                    action: "wiloke_dismiss_notification",
                                    objectID: i.data("id"),
                                    currentUserID: n,
                                    security: WILOKE_GLOBAL.wiloke_nonce
                                },
                                success: function(t) {}
                            })
                        })
                    }
                }]), i
            }(),
            T = function() {
                function i() {
                    t(this, i), this.$app = o("#signup-signin-wrapper, #wiloke-sc-signup-signin-wrapper, #wiloke-widget-signup-signin-wrapper"), this.controller()
                }
                return e(i, [{
                    key: "controller",
                    value: function() {
                        this.$app.length && (this.xhr = null, this.action = null, this.$this = null, this.$btn = null, this.oData = {}, this.clearErrorIndication(), this.signIn(), this.signUp(), this.resetPassword(), this.socialConnection())
                    }
                }, {
                    key: "clearErrorIndication",
                    value: function() {
                        this.$app.find("input").on("keydown", function(t) {
                            var e = o(t.currentTarget),
                                i = null;
                            null !== i && clearTimeout(i), i = setTimeout(function(t) {
                                e.closest(".form-item").removeClass("validate-required"), clearTimeout(i)
                            }, 1e3)
                        })
                    }
                }, {
                    key: "socialConnection",
                    value: function() {
                        var t = o("body");
                        t.on("wiloke_login_with_social/connecting", function(t, e) {
                            e.addClass("loading"), e.closest(".signup-signin-with-social").find(".print-msg-here").html("")
                        }), t.on("wiloke_login_with_social/ajax_completed", function(t, e, i) {
                            e.removeClass("loading"), _.isUndefined(i) || i.success || e.closest(".signup-signin-with-social").find(".print-msg-here").html(i.data.message)
                        })
                    }
                }, {
                    key: "signIn",
                    value: function() {
                        var t = this;
                        o("#wiloke-popup-signin-form").on("submit", function(e) {
                            var i = o(e.target),
                                a = i.find(".signin-btn");
                            e.preventDefault(), t.action = "wiloke_signin", t.$this = i, t.$btn = a, delete t.oData, t.oData = {
                                userlogin: i.find('[name="userlogin"]').val(),
                                password: i.find('[name="password"]').val(),
                                remember: i.find('[name="remember"]').is(":checked") ? "yes" : ""
                            }, t.timeout = 1e3, t.ajaxProcessing()
                        })
                    }
                }, {
                    key: "signUp",
                    value: function() {
                        var t = this;
                        o("#wiloke-widget-signup-form, #wiloke-shortcode-signup-form, #wiloke-popup-signup-form").on("submit", function(e) {
                            e.preventDefault();
                            var i = o(e.target),
                                a = i.find(".signup-btn");
                            t.action = "wiloke_signup", t.$this = i, t.$btn = a, t.oData = {
                                username: i.find('[name="username"]').val(),
                                email: i.find('[name="email"]').val(),
                                password: i.find('[name="password"]').val(),
                                agree_to_term_conditional: i.find('[name="agree_to_term_conditional"]:checked').val(),
                                ggrecaptcha: "undefined" != typeof grecaptcha ? i.find('[name="g-recaptcha-response"]').val() : ""
                            }, t.timeout = 3e3, t.ajaxProcessing()
                        })
                    }
                }, {
                    key: "resetPassword",
                    value: function() {
                        var t = this,
                            e = this.$app.find("#recoverpassword"),
                            i = this.$app.find("#recoverpassword-form");
                        i.on("submit", function(a) {
                            a.preventDefault(), t.action = "wiloke_resetpassword", t.$this = i, t.$btn = e, delete t.oData, t.oData = {
                                user_login: i.find('[name="user_login"]').val()
                            }, t.timeout = 1e3, t.ajaxProcessing()
                        })
                    }
                }, {
                    key: "ajaxProcessing",
                    value: function() {
                        var t = this;
                        if (null !== this.xhr && 200 !== this.xhr.status) return !1;
                        this.$btn.addClass("loading");
                        var e = {
                            action: this.action,
                            security: WILOKE_GLOBAL.wiloke_nonce
                        };
                        e = _.extend(e, this.oData), this.$this.find(".print-msg-here").html(""), this.xhr = o.ajax({
                            type: "POST",
                            url: WILOKE_GLOBAL.ajaxurl,
                            data: e,
                            success: function(e) {
                                e.success ? (t.$this.find(".print-msg-here").removeClass("error-msg").addClass("success-msg").html(e.data.message), "wiloke_signin" === t.action || "wiloke_signup" === t.action ? setTimeout(function() {
                                    void 0 !== e.data.redirectTo ? window.location.href = e.data.redirectTo : location.reload()
                                }, t.timeout) : "wiloke_resetpassword" === t.action && t.$this.find(".form-item").remove()) : _.isUndefined(e.data) || _.isUndefined(e.data.target) || !t.$this.find("#" + e.data.target).length ? _.isUndefined(e.data) || _.isUndefined(e.data.message) || t.$this.find(".print-msg-here").html(e.data.message) : t.$this.find("#" + e.data.target).closest(".form-item").addClass("validate-required"), t.$btn.removeClass("loading")
                            }
                        })
                    }
                }]), i
            }(),
            F = function() {
                function i(e) {
                    t(this, i), this.$app = e, this.init()
                }
                return e(i, [{
                    key: "init",
                    value: function() {
                        var t = this;
                        if (!this.$app.find(".wil-gridratio__list").children().length) return !1;
                        this.oConfigs = {
                            preventClick: !1,
                            resize: function() {
                                t.$app.find("li").hoverdir({
                                    speed: 250,
                                    hoverElem: ".wil-gridratio__caption"
                                })
                            },
                            replaceItem: function(t) {
                                o(".wil-gridratio__caption", t).css("left", "-100%"), o(t).hoverdir({
                                    speed: 250,
                                    hoverElem: ".wil-gridratio__caption"
                                })
                            }
                        }, this.oConfigs = o.extend({}, this.oConfigs, this.$app.data("configuration")), this.letStart()
                    }
                }, {
                    key: "letStart",
                    value: function() {
                        this.$app.gridrotator(this.oConfigs)
                    }
                }]), i
            }(),
            P = function() {
                function i() {
                    t(this, i), this.$formUpdate = o("#wiloke-listgo-update-profile"), this.passedValidation = !0, this.xhr = null, this.init()
                }
                return e(i, [{
                    key: "scrollTop",
                    value: function(t) {
                        o("body, html").animate({
                            scrollTop: t.offset().top - 100
                        }, 600)
                    }
                }, {
                    key: "validate",
                    value: function() {
                        var t = this;
                        _.forEach(["user_email", "display_name", "nickname"], function(e) {
                            if ("" === t.$formUpdate.find("#" + e).val()) return t.passedValidation = !1, t.$formUpdate.find("#" + e).closest(".form-item").addClass("validate-required"), t.scrollTop(o("#" + e)), !1;
                            var i = null;
                            t.$formUpdate.find("#" + e).on("keydown", function(t) {
                                var e = o(t.currentTarget);
                                null !== i && clearTimeout(i), i = setTimeout(function() {
                                    e.closest(".form-item").removeClass("validate-required"), e.closest(".form-item").find(".validate-message").remove(), clearTimeout(i)
                                }, 1e3)
                            })
                        });
                        var e = this.$formUpdate.find("#current_password"),
                            i = this.$formUpdate.find("#new_password"),
                            a = this.$formUpdate.find("#confirm_new_password");
                        if (i.on("keydown", function() {
                                i.closest(".form-item").removeClass("validate-required"), i.find(".validate-message").remove()
                            }), e.on("keydown", function() {
                                e.closest(".form-item").removeClass("validate-required"), e.find(".validate-message").remove()
                            }), a.on("keydown", function() {
                                a.closest(".form-item").removeClass("validate-required"), a.find(".validate-message").remove()
                            }), "" !== i.val()) {
                            if ("" === e.val()) return this.passedValidation = !1, e.closest(".form-item").addClass("validate-required"), this.scrollTop(e), !1;
                            if (i.val() !== a.val()) return this.passedValidation = !1, i.closest(".form-item").addClass("validate-required"), a.closest(".form-item").addClass("validate-required"), this.scrollTop(i), !1
                        }
                    }
                }, {
                    key: "init",
                    value: function() {
                        var t = this;
                        this.$btnUpdate = this.$formUpdate.find("#wiloke-listgo-submit-update-profile"), this.$formUpdate.length > 0 && this.$formUpdate.on("submit", function(e) {
                            if (e.preventDefault(), t.passedValidation = !0, t.validate(), !t.passedValidation) return !1;
                            null !== t.xhr && 200 !== t.xhr.status && t.xhr.abort(), "" !== t.$formUpdate.find("#current_password").val() && "" === t.$formUpdate.find("#new_password").val() && t.$formUpdate.find("#new_password").closet(".form-item").addClass("validate-required"), t.$formUpdate.addClass("loading"), t.$btnUpdate.addClass("loading"), t.xhr = o.ajax({
                                type: "POST",
                                data: {
                                    action: "wiloke_listgo_update_profile",
                                    security: WILOKE_GLOBAL.wiloke_nonce,
                                    data: t.$formUpdate.serialize()
                                },
                                url: WILOKE_GLOBAL.ajaxurl,
                                success: function(e) {
                                    e.success ? t.$formUpdate.find(".update-status").html('<i class="fa fa-check-circle"></i> ' + e.data.message) : "" !== e.data && _.forEach(e.data, function(e, i) {
                                        var a = o("#" + i);
                                        if (a.length) return a.closest(".form-item").addClass("validate-required").append('<span class="validate-message">' + e + "</span>"), t.scrollTop(a), !1
                                    }), t.$formUpdate.removeClass("loading"), t.$btnUpdate.removeClass("loading")
                                }
                            })
                        })
                    }
                }]), i
            }(),
            D = function() {
                function i() {
                    t(this, i), this.init()
                }
                return e(i, [{
                    key: "init",
                    value: function() {
                        var t = this;
                        this.xhr = !1, this.authorID = null, this.status = "follow", this.oTranslate = WILOKE_LISTGO_TRANSLATION;
                        var e = o(".js_subscribe");
                        e.on("click", function(e) {
                            if (e.preventDefault(), "no" === WILOKE_GLOBAL.isLoggedIn) return o(".header__user").find(".user__icon").trigger("click"), !1;
                            var i = o(e.currentTarget);
                            if (t.$app = i, t.authorID = i.data("authorid"), i.parent().toggleClass("active"), _.isUndefined(t.authorID)) return !1;
                            t.status = "follow" === i.data("status") || void 0 === i.data("status") ? "unfollow" : "follow", i.data("status", t.status), t.ajax()
                        }), e.on("completed", function(t, i) {
                            i.data.status, e.html(i.data.text + ' <i class="fa fa-rss"></i>')
                        }), o(".js_subscribe_on_profile").on("click", function(e) {
                            if (e.preventDefault(), "no" === WILOKE_GLOBAL.isLoggedIn) return alert(t.oTranslate.needsingup), !1;
                            var i = o(e.currentTarget);
                            t.$app = i, t.authorID = i.data("authorid"), t.status = "follow" === i.data("status") || void 0 === i.data("status") ? "unfollow" : "follow";
                            var a = o(".account-subscribe").find(".followers .count"),
                                n = a.html();
                            n = parseInt(n, 10), t.$app.on("completed", function(e, i) {
                                "following" === i.data.status ? (t.$app.html(t.oTranslate.followingtext + ' <i class="fa fa-rss"></i>'), a.html(n + 1)) : (t.$app.html(t.oTranslate.unfollowingtext + ' <i class="fa fa-rss"></i>'), a.html(n - 1))
                            }), t.ajax()
                        })
                    }
                }, {
                    key: "ajax",
                    value: function() {
                        var t = this;
                        this.xhr && 200 !== this.xhr.status && this.xhr.abort(), this.xhr = o.ajax({
                            type: "POST",
                            url: WILOKE_GLOBAL.ajaxurl,
                            data: {
                                action: "wiloke_follow",
                                security: WILOKE_GLOBAL.wiloke_nonce,
                                author_id: this.authorID,
                                status: this.status
                            },
                            success: function(e) {
                                WILOKE_GLOBAL.is_debug && console.log(e), e.success ? (t.status = e.data.status, t.$app.trigger("completed", e)) : alert(e.data)
                            }
                        })
                    }
                }]), i
            }(),
            W = function() {
                function i() {
                    t(this, i), this.xhr = null, this.init()
                }
                return e(i, [{
                    key: "init",
                    value: function() {
                        var t = this;
                        this.$report = o(".js_report"), this.oTranslation = WILOKE_LISTGO_TRANSLATION, this.$report.on("click", function(e) {
                            e.preventDefault();
                            var i = o(e.currentTarget);
                            if (i.data("didit")) return !1;
                            var a = prompt(t.oTranslation.report, "");
                            if (null === a || "" === a) return !1;
                            null !== t.xhr && 200 !== t.xhr.status && t.xhr.abort(), t.xhr = o.ajax({
                                type: "POST",
                                data: {
                                    action: "add_report",
                                    ID: WILOKE_GLOBAL.postID,
                                    reason: a,
                                    security: WILOKE_GLOBAL.wiloke_nonce
                                },
                                url: WILOKE_GLOBAL.ajaxurl,
                                success: function(t) {
                                    t.success ? (i.data("didit", !0), alert(t.data.msg)) : alert(t.data.msg)
                                }
                            })
                        })
                    }
                }]), i
            }();
        ! function() {
            var t = o(".add-listing__style"),
                e = o(".add-listing-group-preview"),
                i = o(".add-listing-group-preview-map"),
                a = t.find(".add-listing__style-selected"),
                n = a.data("preview-title"),
                s = a.data("preview-category"),
                r = o("#listing_style");
            e.find("img").attr("src", n), t.length && (t.owlCarousel({
                items: 3,
                margin: 20,
                nav: !0,
                rtl: y,
                lazyLoad: !0,
                mouseDrag: !1,
                navText: ['<i class="arrow_carrot-left"></i>', '<i class="arrow_carrot-right"></i>'],
                responsive: {
                    992: {
                        items: 3
                    },
                    0: {
                        items: 2
                    }
                }
            }), o(".add-listing__style-item").on("click", function(i) {
                var l = o(this),
                    c = l.data("template");
                l.hasClass("add-listing__style-selected") || l.hasClass("add-listing__style-disable") || (r.val(c), t.find(".add-listing__style-selected").removeClass("add-listing__style-selected"), l.addClass("add-listing__style-selected"), a = l, n = l.data("preview-title"), s = l.data("preview-category"), o(".add-listing-input-title").find(".input-text").hasClass("active") && e.find("img").attr("src", n), o(".add-listing-input-categories").find(".input-select2").hasClass("active") && e.find("img").attr("src", s))
            })), o(".add-listing-input-title").on("focus", "input", function(t) {
                o(this).closest(".input-text").addClass("active"), i.css({
                    opacity: 0,
                    visibility: "hidden"
                }), e.css({
                    opacity: 1,
                    visibility: "visible"
                }).find("img").attr("src", n), o(".add-listing-input-location").find(".input-text").removeClass("active"), o(".add-listing-input-categories").find(".input-select2").removeClass("active")
            }), o(".add-listing-input-categories").on("focus", "select, input", function(t) {
                o(this).closest(".input-select2").addClass("active"), i.css({
                    opacity: 0,
                    visibility: "hidden"
                }), e.css({
                    opacity: 1,
                    visibility: "visible"
                }).find("img").attr("src", s), o(".add-listing-input-title, .add-listing-input-location").find(".input-text").removeClass("active")
            }), o(".add-listing-input-location").on("focus", "input", function(t) {
                o(this).closest(".input-text").addClass("active"), o(".add-listing-input-categories").find("select").parent().removeClass("active"), o(".add-listing-input-title").find("input").parent().removeClass("active"), e.css({
                    opacity: 0,
                    visibility: "hidden"
                }), i.css({
                    opacity: 1,
                    visibility: "visible"
                }), o(".add-listing-input-categories, .add-listing-input-title").find(".input-text, .input-select2").removeClass("active")
            }), o("#toggle-business-hours").on("change", function(t) {
                var e = o(this),
                    i = o("#table-businees-hour");
                e.is(":checked") ? i.addClass("active") : i.removeClass("active")
            }).trigger("change"), o("#createaccount").on("change", function(t) {
                var e = o(this),
                    i = o("#wiloke-signup-signin-wrapper");
                e.is(":checked") ? i.addClass("active") : i.removeClass("active")
            })
        }(), o(".comment__rate a").mouseenter(function() {
                var t = o(this),
                    e = t.index(),
                    i = t.data("title");
                t.closest(".comment__rate").find(".comment__rate-placeholder").text(i), t.addClass("hover-active"), o(".comment__rate a:lt(" + e + ")").addClass("hover-active")
            }), o(".comment__rate").mouseout(function(t) {
                var e = o(this),
                    i = e.data("title"),
                    a = e.find(".comment__rate-placeholder"),
                    i = a.attr("data-placeholder");
                a.text(i), e.find("a").removeClass("hover-active")
            }), o(".comment__rate a").on("click", function(t) {
                var e = o(this),
                    i = e.data("title");
                e.closest(".comment__rate").find(".comment__rate-placeholder").attr("data-placeholder", i)
            }), o(window).on("scroll", function(t) {
                var e = o(".listing-single-bar");
                e.length && e.trigger("ListingBarScroll");
                var i = o(".listgo-map__result");
                i.length && i.trigger("scrollResultMap");
                var a = o(".wil-scroll-top");
                a.length && a.trigger("scrollTop");
                var n = o(".header-page__breadcrumb-filter");
                n.length && n.trigger("filterScoll")
            }), o(window).on("resize", function() {
                var t = o(window).innerWidth(),
                    e = o(".listgo-map__result"),
                    i = o(".from-wide-listing");
                t > 480 && i && i.css("display", ""), e.length && e.trigger("scrollResultMap");
                var a = o(".listgo-register");
                a.length && a.trigger("onRegister")
            }), o(window).on("load", function() {
                h(), u(), new F(o(".wil-gridratio"))
            }), new W, o(".wil_accordion").on("click", ".wil_accordion__header a", function(t) {
                t.preventDefault();
                var e = o(this),
                    i = e.closest(".wil_accordion"),
                    a = e.closest(".wil_accordion__header"),
                    n = e.attr("href"),
                    s = o(".wil_accordion__header.active", i),
                    r = o(".wil_accordion__content.active", i),
                    l = o("#" + n.split("#")[1]);
                a.hasClass("active") ? (l.slideToggle(300).toggleClass("active"), a.toggleClass("active")) : (r.slideUp(300).removeClass("active"), s.removeClass("active"), l.slideDown(300).addClass("active"), a.addClass("active"))
            }),
            function() {
                var t = o(".events-carousel");
                t.length && t.each(function() {
                    o(this).find(".event-item").length ? o(this).owlCarousel({
                        items: 1,
                        lazyLoad: !0,
                        nav: !0,
                        autoHeight: !0,
                        loop: !0,
                        rtl: y,
                        navText: ['<i class="arrow_left"></i>', '<i class="arrow_right"></i>']
                    }) : o(this).closest(".listgo-event-container").remove()
                });
                var e = o(".blog-carousel");
                e.length && e.each(function() {
                    var t = o(this);
                    t.owlCarousel({
                        nav: !0,
                        lazyLoad: !0,
                        margin: 30,
                        loop: !0,
                        rtl: y,
                        navText: ['<i class="arrow_carrot-left"></i>', '<i class="arrow_carrot-right"></i>'],
                        responsive: {
                            1200: {
                                items: parseInt(t.data("showposts"), 10)
                            },
                            768: {
                                items: 2
                            },
                            0: {
                                items: 1
                            }
                        }
                    })
                }), o(".testimonials").each(function(t, e) {
                    var i = o(this),
                        a = i.find(".testimonial__avatars"),
                        n = i.find(".testimonials-carousel");
                    a.length && a.owlCarousel({
                        startPosition: 0,
                        center: !0,
                        items: 3,
                        loop: !0,
                        nav: !1,
                        autoplay: !0,
                        margin: 10,
                        mouseDrag: !1,
                        touchDrag: !1,
                        pullDrag: !1,
                        freeDrag: !1,
                        rtl: y,
                        smartSpeed: 1e3
                    }), n.length && n.owlCarousel({
                        startPosition: 0,
                        items: 1,
                        nav: !0,
                        loop: !0,
                        autoplay: !0,
                        rtl: y,
                        smartSpeed: 1e3,
                        animateIn: "fadeIn",
                        animateOut: "fadeOut",
                        navText: ['<i class="arrow_carrot-left"></i>', '<i class="arrow_carrot-right"></i>']
                    }), n.on("changed.owl.carousel", function(t) {
                        a.trigger("to.owl.carousel", [t.item.index, 300])
                    })
                })
            }(), o(".header__user").on("click", ".user__avatar", function(t) {
                t.preventDefault(), o(this).closest(".header__user").toggleClass("active")
            }), o(".header__notifications").on("click", ".notifications__icon", function(t) {
                o(this).closest(".header__notifications").toggleClass("active")
            }), o(".account-nav__toggle").on("click", function(t) {
                t.preventDefault(), o(".account-nav").toggleClass("active")
            }), o(".listing-filter__button").on("click", function(t) {
                t.preventDefault(), o(".from-wide-listing").slideToggle()
            }), o(".label--dropdown").on("click", function(t) {
                t.preventDefault(), o(this).toggleClass("active")
            }), o(".input-slider").each(function() {
                var t = o(this);
                t.slider({
                    value: t.data("currentRadius"),
                    min: t.data("minRadius"),
                    max: t.data("maxRadius"),
                    slide: function(e, i) {
                        t.find("input").val(i.value), t.find("input").trigger("change"), t.attr("data-current-radius", i.value), o(e.target).find("span.ui-slider-handle").attr("data-value", i.value)
                    },
                    create: function(e, i) {
                        o(e.target).find("span.ui-slider-handle").attr("data-value", t.data("currentRadius"))
                    }
                })
            }), o(".input-datepicker").each(function(t, e) {
                o(this).datepicker({
                    beforeShow: function(t, e) {
                        o("#ui-datepicker-div").addClass("wo_datepicker")
                    }
                })
            }), o(".icon-search-map, .mapsearch__close").on("click", function(t) {
                t.preventDefault(), o(".listgo-mapsearch, .icon-search-map").toggleClass("active")
            }), o(".dropdown").on("click", "span", function(t) {
                t.preventDefault();
                var e = o(this),
                    i = e.data("tagert");
                if (e.hasClass("active")) return !1;
                e.parent().find(".active").removeClass("active"), e.addClass("active"), o("#leave-now, #depart-at").removeClass("active"), o(i).addClass("active"), e.closest(".label--dropdown").find(".label-dropdown--text").text(e.text())
            }), o("[data-modal]").on("click", function(t) {
                var e = o(this).data("modal");
                void 0 !== e && o(e).length && o(e).addClass("wil-modal--open")
            }), o(".wil-modal__close").on("click", function(t) {
                o(this).closest(".wil-modal").removeClass("wil-modal--open").trigger("closed")
            }), o(".wil-modal").on("click", function(t) {
                var e = o(t.target);
                e.closest(".wil-modal__content").length || (e.closest(".wil-modal").removeClass("wil-modal--open"), e.trigger("closed"))
            }),
            function() {
                if (!s.Any()) {
                    var t = o(".mapsearch__form");
                    t.length && t.perfectScrollbar()
                }
            }(),
            function() {
                var t = o("#header");
                if (t.length) {
                    var e = t.data("breakMobile");
                    void 0 === e && (e = 991), o(window).on("resize", function(i) {
                        o(window).innerWidth() <= e ? t.addClass("header-responsive").removeClass("header-desktop") : (t.removeClass("header-responsive").addClass("header-desktop"), o("body").removeClass("menu-mobile__open"))
                    }).trigger("resize")
                }
                o(".header__toggle").on("click", function(t) {
                    t.preventDefault(), o("body").toggleClass("menu-mobile__open")
                })
            }(),
            function() {
                var t = o(".bg_video");
                t.length && t.each(function() {
                    var t = self.closest(".bg-video");
                    o(this).mb_YTPlayer({
                        containment: t
                    })
                })
            }(), o(document).on("click", function(t) {
                var e = o(t.target);
                e.closest(".header__notifications").length || o(".header__notifications").removeClass("active"), e.closest(".header__user").length || o(".header__user").removeClass("active"), e.closest(".header-mobile").length || e.closest(".header__toggle").length || o("body").removeClass("menu-mobile__open")
            }),
            function() {
                var t = o(".listgo-map__result");
                t.length && (t.perfectScrollbar(), t.on("scrollResultMap", function(t) {
                    if (o(window).innerWidth() >= 567) {
                        var e = o(this),
                            i = e.closest(".listgo-map-wrap"),
                            a = i.find(".listgo-map__settings"),
                            n = i.find(".listgo-map__field"),
                            s = i.innerHeight();
                        a.length && (s -= parseInt(a.css("padding-top"))), n.length && "none" !== n.css("display") && (s -= n.outerHeight(!0)), e.height(s), e.perfectScrollbar("update")
                    }
                }).trigger("scrollResultMap"))
            }(),
            function() {
                var t = o("body");
                o(".listgo-map-wrap-expand").on("click", function(e) {
                    var i = o(this),
                        a = i.data("status"),
                        n = i.closest(".listgo-map-wrap"),
                        s = n.data("id"),
                        r = i.closest(".listgo-map-container"),
                        l = o('<div class="wil-popup-map"></div>');
                    a ? (i.data("status", !1), o("#" + s).css("height", "").html(n), o("body").find(".wil-popup-map").remove()) : (i.data("status", !0), r.height(n.innerHeight()), t.find(".wil-popup-map").remove(), l.css({
                        position: "fixed",
                        width: "100%",
                        height: "100%",
                        "z-index": 99999,
                        top: 0,
                        left: 0,
                        "background-color": "#fff"
                    }), l.html(n), t.append(l))
                })
            }(), o(".item--tags").on("click", ".label", function(t) {
                t.preventDefault(), o(this).parent(".item--tags").find(".item--tags-toggle").slideToggle()
            }), s.Any() && o(".item--tags").trigger("click"),
            function() {
                var t = o("body"),
                    e = o("#wrap-page");
                o(".header-page__breadcrumb-filter, .from-wide-listing__header-close, .from-wide-listing__footer").on("click", function(i) {
                    e.toggleClass("form-search-active"), s.IOS() && (e.hasClass("form-search-active") ? t.addClass("body--disable-scrolling") : t.removeClass("body--disable-scrolling"))
                }), o(document).on("click", function(t) {
                    var i = o(t.target);
                    i.closest(".from-wide-listing").length || i.closest(".header-page__breadcrumb-filter").length || e.removeClass("form-search-active")
                });
                var i = o(".header-page__breadcrumb-filter");
                if (i.length && (t.hasClass("page-template-listing") || t.hasClass("page-template-half-map"))) {
                    var a = 0;
                    i.on("filterScoll", function(t) {
                        var e = o(this),
                            i = 15,
                            n = 0,
                            s = o(window).scrollTop(),
                            r = o(window).innerWidth();
                        e.hasClass("activeScroll") || (a = e.offset().top);
                        var l = o("#wpadminbar");
                        l.length ? (n = a - i - l.height(), i += l.height()) : n = a - i, n < s && r < 768 ? (o(".header-page").css("z-index", 100), e.addClass("activeScroll").css("top", i)) : (o(".header-page").css("z-index", ""), e.removeClass("activeScroll").css("top", ""))
                    }).trigger("filterScoll"), o(window).on("scroll", function(t) {
                        i.trigger("filterScoll")
                    })
                }
            }(), o(window).on("load", function(t) {
                var e = o(".page-template-addlisting #listing_content_ifr");
                e.length && (e.contents().find("body").addClass("wil-mce-editor"), o("#listgo-googlefont-css").clone().appendTo(e.contents().find("head")), e.contents().find("head").append('\t            <style type="text/css">\t\t         \tbody, .mce-content-body p, .mce-content-body div:not(.addlisting-placeholder__title) {\t\t\t\t\t\tfont: ' + o("body").css("font") + ";\t\t\t\t\t\tline-height: " + o("body").css("line-height") + ";\t\t\t\t\t\tcolor: " + o("body").css("color") + ";\t\t\t\t\t}\t\t\t\t\t.mce-content-body h1,.mce-content-body h2,.mce-content-body h3,.mce-content-body h4,.mce-content-body h5,.mce-content-body h6 {\t\t\t\t\t\tfont: " + o(".page-template-addlisting .header-page__title").css("font") + ";\t\t\t\t\t\tfont-weight: 600;\t\t\t\t\t\tcolor: #212122;\t\t\t\t\t}\t\t\t\t\t.mce-content-body h1 {\t\t\t\t\t\tfont-size: 40px;\t\t\t\t\t}\t\t\t\t\t.mce-content-body h2 {\t\t\t\t\t\tfont-size: 34px;\t\t\t\t\t}\t\t\t\t\t.mce-content-body h3 {\t\t\t\t\t\tfont-size: 28px;\t\t\t\t\t}\t\t\t\t\t.mce-content-body h4 {\t\t\t\t\t\tfont-size: 22px;\t\t\t\t\t}\t\t\t\t\t.mce-content-body h5 {\t\t\t\t\t\tfont-size: 18px;\t\t\t\t\t}\t\t\t\t\t.mce-content-body h6 {\t\t\t\t\t\tfont-size: 14px;\t\t\t\t\t}\t\t\t\t\t.mce-content-body h1, .mce-content-body h2, .mce-content-body h3 {\t\t\t\t\t\tmargin-top: 20px;\t\t\t\t\t\tmargin-bottom: 10px;\t\t\t\t\t}\t\t\t\t\t.mce-content-body h4, .mce-content-body h5, .mce-content-body h6 {\t\t\t\t\t\tmargin-top: 10px;\t\t\t\t\t\tmargin-bottom: 10px;\t\t\t\t\t}\t\t\t\t\t.mce-content-body > p {\t\t\t\t\t\tmargin-top: 0;\t\t\t\t\t\tmargin-bottom: 10px\t\t\t\t\t}\t\t\t\t</style>\t        ")), o(".wiloke-listing-layout").each(function() {
                    o(this).addClass("loaded")
                })
            }), o.fn.WilokeNiceGridTitle = function() {
                o(this).each(function() {
                    var t = o(this),
                        e = o(".listing__title", t);
                    if ((t.hasClass("listing--grid3") || t.hasClass("listing--grid4") ? o("a", e).outerWidth(!0) + 100 : o("a", e).outerWidth(!0) + 30) >= t.width()) {
                        var i = null;
                        1 === e.children().length ? i = "listing-fix-title" : 2 === e.children().length ? i = "listing-fix-title2" : 3 === e.children().length && (i = "listing-fix-title3"), t.addClass(i)
                    }
                })
            }, jQuery(document).ready(function() {
                c(), w(), v(), new $, new I, new L, new x, new P, new T, d(), new b, new D, new S, g(), o(".wiloke-js-upload").each(function() {
                    new O(o(this))
                });
                var t = null;
                if ("disable" !== WILOKE_GLOBAL.toggleAskForPosition) {
                    if (o(".page-template-addlisting").length) return !1;
                    t = new C
                }
                o("#listgo-current-location").on("click", function() {
                    null === t ? (t = new C(!0)).isFocusSetLocationField = !1 : (t.isFocusSetLocationField = !0, t.findYourLocationInSearchForm(), t.isFocusSetLocationField = !1)
                });
                var e = o(".listgo-register");
                e.length && e.on("onRegister", function(t) {
                    var e = o(this),
                        i = e.closest(".vc_row");
                    i.length && (e.offset().top > i.offset().top ? e.addClass("listgo-register--remove-line") : e.removeClass("listgo-register--remove-line"))
                }).trigger("onRegister"), f(), o(".listgo-map-wrap .listgo-map__apply").on("click", function(t) {
                    o(this).closest(".listgo-map-wrap").removeClass("list-map-wrap--setting-active")
                }), o(".listing--grid3, .listing--grid4").closest(".page-template-listing").css("background-color", "#f6f7f8"), o(".wil-dashbroad__bar-menu-toggle").on("click", function(t) {
                    t.preventDefault(), o(this).next("ul").toggleClass("active")
                }), o(".wil-dashbroad__bar-menu").on("click", ".has-children > a", function(t) {
                    t.preventDefault(), o(this).parent("li").toggleClass("active")
                }), m(), o(".listing--grid3, .listing--grid4").closest(".page-template-listing").css("background-color", "#f4f4f4"), o('.listing[class*="listing--grid"]').length && o('.listing[class*="listing--grid"]').WilokeNiceGridTitle(), p()
            }), o(".header-page-form").closest(".vc_row").css({
                "padding-top": 0,
                "padding-bottom": 0
            }), o(window).on("load", function() {
                var t = o(".nav-filter").next(".wiloke-listgo-listlayout").children(".listgo-wrapper-grid-items");
                t.length && t.imagesLoaded(function() {
                    t.isotope({
                        layoutMode: "fitRows",
                        itemSelector: '[class^="col-"]',
                        masonry: {
                            columnWidth: '[class^="col-"]'
                        }
                    })
                }), o(".wiloke-menu-sub.wiloke-menu-sub-mega").each(function() {
                    o.trim(o(this).html()).length || o(this).remove()
                })
            })
    }(jQuery)
}();