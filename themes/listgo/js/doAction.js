/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    $("#form-submit-listing").validate({
        rules: {
            listing_title: {
                required: true
            },
            listing_address: {
                required: true
            }
        },
        messages: {
            listing_title: {
                required: "This field cannot be left blank"
            },
            listing_address: {
                required: "This field cannot be left blank"
            }
        }
    });
    $("#submit-listing").on("click", function () {
        var url_ajax = JSON.parse(urls).postListing;
        var token = JSON.parse(globals).token;
        var $submit = $("#form-submit-listing");
        $("#form-submit-listing").ajaxForm({
            url: url_ajax,
            data: {
                "X_REQUESTED_WITH": "XMLHttpRequest",
                data: $submit.serialize(),
                token: token
            },
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            xhr: function () {
                var xhr = $.ajaxSettings.xhr();
                xhr.upload.onprogress = function (e) {
                    console.log(Math.floor(e.loaded / e.total * 100) + '%');
                    $('#submit-listing').html('Uploading... ' + Math.floor(e.loaded / e.total * 100) + '%');
                    $('#submit-listing').css(
                        'width', Math.floor(e.loaded / e.total * 100) + '%'
                    );
                };
                return xhr;
            },
            beforeSubmit: function (e) {
                $submit.addClass("loading");
                $submit.prop("disabled", !1);
                $('#wiloke-print-msg-here').html('');
            },
            success: function (data, i) {
                if (data.response.code == 200) {
                    setTimeout(function () {
                        resetForm();
                        $('#submit-listing').html('Submit Your Map');
                        $('#submit-listing').removeAttr('style');
                    }, 2000);
                } else {
                    $submit.prop("disabled", !1);
                    $submit.removeClass("loading");
                    $('#wiloke-print-msg-here').html('<p class="update-status error-msg">' + data.response.messages + '</p>');
                }
                $('html, body').animate({
                    scrollTop: $('.page-addlisting').offset().top - 50
                }, 1000);
            },
            error: function (xhr, ajaxOptions, thrownError) {}
        });

        var resetForm = function () {
            $submit.prop("disabled", !1);
            $submit.removeClass("loading");
            $submit[0].reset();
            $('input[name="location_lat"]').val("");
            $('input[name="location_lng"]').val("");
            $('.wil-addlisting-gallery__list').html("");
            $('.select2-selection__rendered').html("");
            $('#wiloke-print-msg-here').html('');
        };
    });
});