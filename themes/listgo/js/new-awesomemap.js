! function() {
    "use strict";
    var t = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(t) {
            return typeof t
        } : function(t) {
            return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t
        },
        i = (function() {
            function t(t) {
                this.value = t
            }

            function i(i) {
                function e(a, n) {
                    try {
                        var o = i[a](n),
                            l = o.value;
                        l instanceof t ? Promise.resolve(l.value).then(function(t) {
                            e("next", t)
                        }, function(t) {
                            e("throw", t)
                        }) : s(o.done ? "return" : "normal", o.value)
                    } catch (t) {
                        s("throw", t)
                    }
                }

                function s(t, i) {
                    switch (t) {
                        case "return":
                            a.resolve({
                                value: i,
                                done: !0
                            });
                            break;
                        case "throw":
                            a.reject(i);
                            break;
                        default:
                            a.resolve({
                                value: i,
                                done: !1
                            })
                    }(a = a.next) ? e(a.key, a.arg): n = null
                }
                var a, n;
                this._invoke = function(t, i) {
                    return new Promise(function(s, o) {
                        var l = {
                            key: t,
                            arg: i,
                            resolve: s,
                            reject: o,
                            next: null
                        };
                        n ? n = n.next = l : (a = n = l, e(t, i))
                    })
                }, "function" != typeof i.return && (this.return = void 0)
            }
            "function" == typeof Symbol && Symbol.asyncIterator && (i.prototype[Symbol.asyncIterator] = function() {
                return this
            }), i.prototype.next = function(t) {
                return this._invoke("next", t)
            }, i.prototype.throw = function(t) {
                return this._invoke("throw", t)
            }, i.prototype.return = function(t) {
                return this._invoke("return", t)
            }
        }(), function(t, i) {
            if (!(t instanceof i)) throw new TypeError("Cannot call a class as a function")
        }),
        e = function() {
            function t(t, i) {
                for (var e = 0; e < i.length; e++) {
                    var s = i[e];
                    s.enumerable = s.enumerable || !1, s.configurable = !0, "value" in s && (s.writable = !0), Object.defineProperty(t, s.key, s)
                }
            }
            return function(i, e, s) {
                return e && t(i.prototype, e), s && t(i, s), i
            }
        }();
    ! function(s) {
        var a = function() {
            function a(t) {
                i(this, a), this.id = t, this.$container = s("#" + t), this.xhr = null, this.reCheck = !1, this.oCluster = null, this.latestPopup = null, this.aNewMarkers = [], this.aLayers = [], this.oListingsLatLng = [], this.aMarkers = [], this.mapInit()
            }
            return e(a, [{
                key: "sidebarListingTpl",
                value: function(t) {
                    return _.template('<li class="listing-item <%- hiddenClass %>" data-placeid="<%- oItem.placeID %>" data-locationid="<%- oItem.first_location_id %>" data-postid="<%- oItem.ID %>" data-latlng="<%- oItem.listing_settings.map.latlong %>" data-catids="<%- oItem.listing_cat_id %>" data-parentlocationid="<%- oItem.parentLocationID %>">\n\t\t\t\t<% if (!_.isUndefined(oItem.thumbnail) && (oItem.thumbnail !== \'\') && oItem.thumbnail){ %>\n\t\t\t\t\t<div class="listing-item__media">\n\t\t\t\t\t\t<img src="<%- oItem.thumbnail %>" alt="<%- oItem.title %>">\n\t\t\t\t\t\t<% if ( oItem.business_status.status === \'opening\'){ %> \n\t\t                    <span class="ongroup">\n\t\t                    \t<span class="onopen"><%- opennow %></span>\n\t\t                    </span>\n\t                    <% }else if(oItem.business_status.status === \'closed\'){ %>\n\t\t                    <span class="ongroup">\n\t\t                    \t<span class="onclose red"><%- closednow %></span>\n\t\t                    </span>\n\t                    <% } %>\n\t\t\t\t\t</div>\n\t\t\t\t<% } %>\n                <div class="overflow-hidden">\n                    <h4><%- oItem.title %></h4>\n                    <div class="listgo__rating">\n\t\t\t\t\t\t<span class="rating__star">\n\t\t\t\t\t\t\t<% for (var i = 1; i <= 5; i++){ %>\n\t\t\t\t\t\t\t\t<% var className = \'\'; %>\n\t\t\t\t\t\t\t\t<% if (oItem.average_rating < i ){ %>\n\t\t\t\t\t\t\t\t\t<% className = i == Math.floor(oItem.average_rating) ? \'fa fa-star-half-o\' : \'fa fa-star-o\'; %>\n\t\t\t\t\t\t\t\t<% }else{ %>\n\t\t\t\t\t\t\t\t\t<% className = \'fa fa-star\'; %>\n\t\t\t\t\t\t\t\t<% } %>\n\t\t\t\t\t\t\t\t<i class="<%- className %>"></i>\n\t\t\t\t\t\t\t<% } %>\n\t\t\t\t\t\t</span>\n\t\t\t\t\t\t<span class="rating__number"><%- oItem.average_rating %></span>\n\t\t\t\t\t</div>\n                    <% if (oItem.listing_settings){ %>\n                    <p><i class="icon_pin_alt"></i><%- oItem.listing_settings.map.location %></p>\n                    <% } %>\n                    <span class="actions"><a href="<%- oItem.link %>"><%- readmore %></a> | <a target="_blank" href="//maps.google.com/maps?search=<%- oItem.listing_settings.map.latlong %>"><%- getdirections %></a></span>\n                </div>\n            </li>')({
                        oItem: t,
                        hiddenClass: this.isInCategory(t) && this.isInLocation(t) && this.isInsideCircle(t.listing_settings.map.latlong) && this.isMatchedTitle(t) && this.isOpenNow(t) ? "" : "hidden",
                        readmore: this.oTranslation.readmore,
                        getdirections: this.oTranslation.getdirections,
                        opennow: this.oTranslation.opennow,
                        closednow: this.oTranslation.closednow
                    })
                }
            }, {
                key: "generateListingSidebar",
                value: function() {
                    var t = this;
                    if (!_.isEmpty(this.aListings)) {
                        var i = "",
                            e = this.aListings;
                        1 === this.s_highest_rated && (e = _.sortBy(e, function(t) {
                            return t.average_rating
                        })).reverse(), _.forEach(e, function(e) {
                            i += t.sidebarListingTpl(e)
                        }), this.$showListing.html(i)
                    }
                }
            }, {
                key: "watch",
                value: function() {
                    var t = this;
                    this.$container.on("listing_change", function() {
                        t.generateListingSidebar()
                    }), this.$searchForm.on("focus_search", function() {
                        var i = t.$search.val();
                        if ("" === i || _.isUndefined(i)) return !1;
                        _.forEach(t.aRawListings, function(e) {
                            i = i.toLowerCase(), e.title.toLowerCase() === i && (t.currentDestination = e, t.panTo(e.listing_settings.map.latlong))
                        })
                    }), this.$showListing.on("click", "li", function(i) {
                        t.panTo(s(i.currentTarget).data("latlng")), WilokeDetectMobile.Any() && t.$wrap.removeClass("list-map-wrap--setting-active")
                    })
                }
            }, {
                key: "mapIcon",
                value: function(t) {
                    var i = decodeURIComponent(WILOKE_GLOBAL.homeURI) + "img/icon-marker.png";
                    return t.listing_cat_settings && !_.isUndefined(t.listing_cat_settings.map_marker_image) && "" !== t.listing_cat_settings.map_marker_image && (i = t.listing_cat_settings.map_marker_image), L.icon({
                        iconUrl: i
                    })
                }
            }, {
                key: "showService",
                value: function(t) {
                    var i = this;
                    _.isNaN(t) || "" === t ? _.forEach(this.aMarkers, function(t) {
                        i.showMarker(t)
                    }) : _.forEach(this.aMarkers, function(e) {
                        var s = i.isInLocation(e);
                        s && (s = _.isArray(e.listing_cat_id) && -1 !== e.listing_cat_id.indexOf(t)), s ? i.showMarker(e) : i.clearMarker(e)
                    })
                }
            }, {
                key: "panTo",
                value: function(t, i) {
                    var e = null,
                        s = t;
                    _.isArray(t) ? this.instMap.panTo({
                        lat: parseFloat(t[0]),
                        lng: parseFloat(t[1])
                    }) : (e = t, s = t.split(","), s = _.map(s, function(t) {
                        return parseFloat(t)
                    }), this.instMap.panTo({
                        lat: s[0],
                        lng: s[1]
                    })), this.$showListing.find("li").removeClass("active"), this.$showListing.find('li[data-latlng="' + e + '"]').addClass("active")
                }
            }, {
                key: "parseCoordinate",
                value: function(t) {
                    if ("" === t) return !1;
                    var i = t.split(",");
                    return i = _.map(i, function(t) {
                        return parseFloat(t)
                    })
                }
            }, {
                key: "getCenter",
                value: function() {
                    for (var t in this.aListings) return this.aListings[t].listing_settings.map.latlong
                }
            }, {
                key: "setPopup",
                value: function(t, i, e) {
                    var s = this;
                    this.aLayers[i.join("-")] = t, t.addEventListener("click", function(t) {
                        s.createPopup(e, i)
                    })
                }
            }, {
                key: "renderTerms",
                value: function(t) {
                    var i = "",
                        e = t.length - 1;
                    return _.forEach(t, function(t, s) {
                        0 === s ? i += '<a href="' + t.link + '">' + t.name + "</a>" : (1 === s && (i += '<span class="listing__cat-more">+</span><ul class="listing__cats">'), i += '<li><a href="' + t.link + '">' + t.name + "</a></li>", e === s && (i += "</ul>"))
                    }), i = '<div class="listing__cat">' + i + "</div>"
                }
            }, {
                key: "renderContent",
                value: function(t) {
                    var i = this,
                        e = "";
                    return _.isUndefined(t) || (e += '<div class="listing__content">', e += '<div class="address">', _.forEach(t, function(t, s) {
                        "" === t || _.isUndefined(i.oText[s]) || "website" === s || "email" === s || ("phone_number" === s && (t = '<a href="tel:' + t + '">' + t + "</a>"), e += '<span class="address-' + s + '"><strong>' + i.oText[s] + ":</strong> " + t + "</span>")
                    }), e += "</div>", e += "</div>"), e
                }
            }, {
                key: "createPopup",
                value: function(t) {
                    var i = t.featured_image ? t.featured_image : "",
                        e = this.renderContent(t.listing_settings);
                    return _.template('<div class="listing listing--grid">\n\t\t\t\t\t<div class="listing__media">\n\t\t\t\t\t\t<a href="<%- oItem.link %>" style=\'background-image: url(<%= featuredImage %>)\'></a>\n\t\t\t\t\t\t<div class="listing__author">\n\t\t\t\t\t\t\t<a href="<%- oItem.author.link %>">\n\t\t\t\t\t\t\t<% if ( !_.isUndefined(oItem.author.user_first_character) ){ %>\n\t\t\t\t\t\t\t\t<span style="background-color: <%= oItem.author.avatar_color %>" class="widget_author__avatar-placeholder"><%- oItem.author.user_first_character %></span>\n\t\t\t\t\t\t\t<% }else{ %>\n\t\t\t\t\t\t\t\t<img src="<%- oItem.author.avatar %>" alt="<%- oItem.author.nickname %>">\n\t\t\t\t\t\t\t<% } %>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class="listing__body">\n\t\t\t\t\t\t<h3 class="listing__title"><a href="<%- oItem.link %>"><%- oItem.title %></a></h3>\n\t\t\t\t\t\t<div class="listgo__rating">\n\t\t\t\t\t\t\t<span class="rating__star">\n\t\t\t\t\t\t\t\t<% for (var i = 1; i <= 5; i++){ %>\n\t\t\t\t\t\t\t\t\t<% var className = \'\'; %>\n\t\t\t\t\t\t\t\t\t<% if (oItem.average_rating < i ){ %>\n\t\t\t\t\t\t\t\t\t\t<% className = i == Math.floor(oItem.average_rating) ? \'fa fa-star-half-o\' : \'fa fa-star-o\'; %>\n\t\t\t\t\t\t\t\t\t<% }else{ %>\n\t\t\t\t\t\t\t\t\t\t<% className = \'fa fa-star\'; %>\n\t\t\t\t\t\t\t\t\t<% }  %>\n\t\t\t\t\t\t\t\t\t<i class="<%- className %>"></i>\n\t\t\t\t\t\t\t\t<% } %>\n\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<%= address %>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>')({
                        oItem: t,
                        address: e,
                        featuredImage: i
                    })
                }
            }, {
                key: "layerOnClick",
                value: function(t) {
                    var i = this;
                    t.addEventListener("click", function() {
                        i.oDirections.setDestination(t._latlng), i.mapDirectionQuery()
                    })
                }
            }, {
                key: "addCluster",
                value: function() {
                    if (null !== this.oCluster ? this.aNewMarkers.length && this.oCluster.addMarkers(this.aNewMarkers) : this.oCluster = new MarkerClusterer(this.instMap, this.aNewMarkers, {
                            imagePath: "https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m",
                            gridSize: WILOKE_GLOBAL.mapcluster,
                            zoomOnClick: !0
                        }), "" !== this.s_listing_cat)
                        for (var t in this.aMarkers) {
                            var i = this.aMarkers[t];
                            _.isEmpty(i.listing_cat_id) ? this.clearMarker(i) : -1 != i.listing_cat_id.indexOf(this.s_listing_cat) || this.clearMarker(i)
                        }
                }
            }, {
                key: "addMarker",
                value: function(t, i, e, s) {
                    var a = this,
                        n = new google.maps.InfoWindow({
                            content: this.createPopup(s)
                        }),
                        o = {
                            position: t,
                            map: this.instMap,
                            title: s.title
                        };
                    void 0 !== e && "" !== e && (o.icon = e);
                    var l = new google.maps.Marker(o);
                    l.addListener("click", function() {
                        null !== a.latestPopup && a.latestPopup.close(), n.open(a.instMap, l), a.latestPopup = n
                    }), this.aMarkers.push({
                        postID: i,
                        marker: l,
                        listing_cat_id: s.listing_cat_id,
                        listing_location_id: s.listing_location_id,
                        infowindow: n,
                        oInit: l
                    }), this.oListingsLatLng[i] = t, this.aNewMarkers.push(l)
                }
            }, {
                key: "addLayers",
                value: function(t) {
                    var i = this;
                    this.aNewMarkers = [], _.forEach(t, function(t) {
                        if (i.aPostIDs.push(t.ID), t.listing_settings && !_.isUndefined(t.listing_settings.map)) {
                            if (void 0 === t.listing_settings.map || "" === t.listing_settings.map.latlong) return !1;
                            var e = i.parseCoordinate(t.listing_settings.map.latlong);
                            if (2 === e.length) {
                                var s = void 0 !== t.listing_cat_settings.map_marker_image ? t.listing_cat_settings.map_marker_image : "";
                                i.addMarker({
                                    lat: e[0],
                                    lng: e[1]
                                }, t.ID, s, t)
                            }
                        }
                    }), this.addCluster(), setTimeout(function() {
                        i.fetching()
                    }, 2e3)
                }
            }, {
                key: "setUp",
                value: function() {
                    this.accessToken = WILOKE_GLOBAL.maptoken, this.aTemporaryCachingResults = [], this.cachingMylocation = null, this.instMyLocation = null, this.radiusInKM = 5, this.circle = !1, this.$wrap = this.$container.closest(".listgo-map-wrap"), this.$sidebar = this.$wrap.find(".listgo-map__settings"), this.$toggleSidebar = this.$wrap.find(".searchbox-hamburger"), this.$mapSidebar = this.$wrap.find("#listgo-map__sidebar"), this.$showListing = this.$mapSidebar.find("#listgo-map__show-listings"), this.$searchForm = this.$wrap.find("#listgo-searchform"), this.$location = this.$wrap.find("#s_listing_location"), this.$submitSearchKeyWord = this.$wrap.find("#listgo-submit-searchkeyword"), this.$search = this.$wrap.find("#s_search"), this.$openNow = this.$wrap.find("#s_opennow"), this.$highestRated = this.$wrap.find("#s_highestrated"), this.$category = this.$searchForm.find("#s_listing_cat"), this.$location = this.$searchForm.find("#s_listing_location"), this.oData = this.$container.data("configs"), this.s_open_now = 0, this.s_highest_rated = 0, this.s_listing_location = this.$location.val(), this.s_listing_cat = this.$category.val(), this.s_listing_cat = "" !== this.s_listing_cat ? parseInt(this.s_listing_cat, 10) : "", this.aRawListings = this.$container.data("listings"), this.aListings = this.aRawListings, this.aListingInLocations = [], this.oDefault = {
                        gridSize: WILOKE_GLOBAL.mapcluster,
                        styles: this.mapStyles(WILOKE_GLOBAL.googleMapThemeType),
                        minZoom: WILOKE_GLOBAL.mapminzoom,
                        maxZoom: WILOKE_GLOBAL.mapmaxzoom,
                        zoom: WILOKE_GLOBAL.centerZoom,
                        zoomOnClick: !0
                    }, this.aPostIDs = [], this.oText = WILOKE_LISTGO_TRANSLATION, this.oData = s.extend({}, this.oDefault, this.oData), this.s = null, this.find_listing_location = null, this.find_listing_cat = null
                }
            }, {
                key: "convertRadiusToMeters",
                value: function() {
                    return 1e3 * this.radiusInKM
                }
            }, {
                key: "setCircle",
                value: function() {
                    this.circle ? this.circle.setRadius(100) : (this.circle = L.circle(this.cachingMylocation, {
                        radius: 100
                    }).addTo(this.instMap), this.instMap.fitBounds(this.circle.getBounds())), this.filterLocationInsideCircle()
                }
            }, {
                key: "filterLocationInsideCircle",
                value: function() {
                    var t = this;
                    this.instMyLocation = null === this.instMyLocation ? L.latLng(this.cachingMylocation) : this.instMyLocation, _.each(this.aRawListings, function(i) {
                        t.isInsideCircle(i.listing_settings.map.latlong) ? (t.$showListing.find('[data-latlng="' + i.listing_settings.map.latlong + '"]').removeClass("hidden"), t.aTemporaryCachingResults.push(i)) : t.$showListing.find('[data-latlng="' + i.listing_settings.map.latlong + '"]').addClass("hidden")
                    })
                }
            }, {
                key: "detectingMyLocation",
                value: function() {
                    var t = this,
                        i = new Date;
                    this.$mapSidebar.find(".listgo-map__result").addClass("loading"), this.getMylocationFromCaching(), null === this.cachingMylocation ? (this.instMap.on("locationfound", function(e) {
                        t.cachingMylocation = e.latlng, localStorage.setItem("listgo_mylocation", JSON.stringify(t.cachingMylocation)), localStorage.setItem("listgo_mylocation_created_at", i.getMinutes()), t.instMap.panTo(t.cachingMylocation), t.setCircle()
                    }), this.instMap.on("locationerror", function(t) {
                        alert(t.message)
                    }), this.instMap.locate({
                        setView: !0,
                        maxZoom: this.oData.centerZoom,
                        maximumAge: 3e4,
                        enableHighAccuracy: !0
                    })) : (this.instMap.panTo(this.cachingMylocation), this.setCircle())
                }
            }, {
                key: "getMylocationFromCaching",
                value: function() {
                    var t = new Date;
                    if (null === this.cachingMylocation) {
                        var i = localStorage.getItem("listgo_mylocation_created_at");
                        t.getMinutes() - i <= 5 && (this.cachingMylocation = s.parseJSON(localStorage.getItem("listgo_mylocation")))
                    }
                }
            }, {
                key: "getMyLocationController",
                value: function() {
                    var t = this,
                        i = decodeURIComponent(WILOKE_GLOBAL.homeURI) + "img/detect-mylocation.png";
                    return new(L.Control.extend({
                        options: {
                            position: "topright"
                        },
                        onAdd: function(e) {
                            var s = L.DomUtil.create("div", "leaflet-bar leaflet-control leaflet-control-detect-mylocation");
                            return s.style.backgroundColor = "white", s.style.backgroundImage = "url(" + i + ")", s.style.backgroundSize = "30px 30px", s.style.width = "30px", s.style.height = "30px", s.onclick = function() {
                                WilokeDetectMobile.Any() ? (t.getMylocationFromCaching(), null === t.cachingMylocation ? (t.instMap.on("locationfound", function(i) {
                                    t.cachingMylocation = i.latlng, t.instMap.panTo(t.cachingMylocation)
                                }), t.instMap.locate({
                                    setView: !0,
                                    maxZoom: t.oData.centerZoom,
                                    maximumAge: 3e4,
                                    enableHighAccuracy: !0
                                })) : t.instMap.panTo(t.cachingMylocation)) : (t.$wrap.addClass("list-map-wrap--setting-active"), t.$searchForm.find("#s_listing_location").val("mylocation").trigger("change"), t.s_listing_location = "mylocation", t.s = "", t.$showListing.trigger("click"))
                            }, s
                        }
                    }))
                }
            }, {
                key: "mapStyles",
                value: function(t) {
                    if ("custom" == t) return WILOKE_GLOBAL.googleMapThemeCode;
                    var i = {
                        blurWater: [{
                            featureType: "administrative",
                            elementType: "labels.text.fill",
                            stylers: [{
                                color: "#444444"
                            }]
                        }, {
                            featureType: "landscape",
                            elementType: "all",
                            stylers: [{
                                color: "#f2f2f2"
                            }]
                        }, {
                            featureType: "poi",
                            elementType: "all",
                            stylers: [{
                                visibility: "off"
                            }]
                        }, {
                            featureType: "road",
                            elementType: "all",
                            stylers: [{
                                saturation: -100
                            }, {
                                lightness: 45
                            }]
                        }, {
                            featureType: "road.highway",
                            elementType: "all",
                            stylers: [{
                                visibility: "simplified"
                            }]
                        }, {
                            featureType: "road.arterial",
                            elementType: "labels.icon",
                            stylers: [{
                                visibility: "off"
                            }]
                        }, {
                            featureType: "transit",
                            elementType: "all",
                            stylers: [{
                                visibility: "off"
                            }]
                        }, {
                            featureType: "water",
                            elementType: "all",
                            stylers: [{
                                color: "#46bcec"
                            }, {
                                visibility: "on"
                            }]
                        }],
                        ultraLight: [{
                            featureType: "water",
                            elementType: "geometry",
                            stylers: [{
                                color: "#e9e9e9"
                            }, {
                                lightness: 17
                            }]
                        }, {
                            featureType: "landscape",
                            elementType: "geometry",
                            stylers: [{
                                color: "#f5f5f5"
                            }, {
                                lightness: 20
                            }]
                        }, {
                            featureType: "road.highway",
                            elementType: "geometry.fill",
                            stylers: [{
                                color: "#ffffff"
                            }, {
                                lightness: 17
                            }]
                        }, {
                            featureType: "road.highway",
                            elementType: "geometry.stroke",
                            stylers: [{
                                color: "#ffffff"
                            }, {
                                lightness: 29
                            }, {
                                weight: .2
                            }]
                        }, {
                            featureType: "road.arterial",
                            elementType: "geometry",
                            stylers: [{
                                color: "#ffffff"
                            }, {
                                lightness: 18
                            }]
                        }, {
                            featureType: "road.local",
                            elementType: "geometry",
                            stylers: [{
                                color: "#ffffff"
                            }, {
                                lightness: 16
                            }]
                        }, {
                            featureType: "poi",
                            elementType: "geometry",
                            stylers: [{
                                color: "#f5f5f5"
                            }, {
                                lightness: 21
                            }]
                        }, {
                            featureType: "poi.park",
                            elementType: "geometry",
                            stylers: [{
                                color: "#dedede"
                            }, {
                                lightness: 21
                            }]
                        }, {
                            elementType: "labels.text.stroke",
                            stylers: [{
                                visibility: "on"
                            }, {
                                color: "#ffffff"
                            }, {
                                lightness: 16
                            }]
                        }, {
                            elementType: "labels.text.fill",
                            stylers: [{
                                saturation: 36
                            }, {
                                color: "#333333"
                            }, {
                                lightness: 40
                            }]
                        }, {
                            elementType: "labels.icon",
                            stylers: [{
                                visibility: "off"
                            }]
                        }, {
                            featureType: "transit",
                            elementType: "geometry",
                            stylers: [{
                                color: "#f2f2f2"
                            }, {
                                lightness: 19
                            }]
                        }, {
                            featureType: "administrative",
                            elementType: "geometry.fill",
                            stylers: [{
                                color: "#fefefe"
                            }, {
                                lightness: 20
                            }]
                        }, {
                            featureType: "administrative",
                            elementType: "geometry.stroke",
                            stylers: [{
                                color: "#fefefe"
                            }, {
                                lightness: 17
                            }, {
                                weight: 1.2
                            }]
                        }],
                        shadesOfGrey: [{
                            featureType: "all",
                            elementType: "labels.text.fill",
                            stylers: [{
                                saturation: 36
                            }, {
                                color: "#000000"
                            }, {
                                lightness: 40
                            }]
                        }, {
                            featureType: "all",
                            elementType: "labels.text.stroke",
                            stylers: [{
                                visibility: "on"
                            }, {
                                color: "#000000"
                            }, {
                                lightness: 16
                            }]
                        }, {
                            featureType: "all",
                            elementType: "labels.icon",
                            stylers: [{
                                visibility: "off"
                            }]
                        }, {
                            featureType: "administrative",
                            elementType: "geometry.fill",
                            stylers: [{
                                color: "#000000"
                            }, {
                                lightness: 20
                            }]
                        }, {
                            featureType: "administrative",
                            elementType: "geometry.stroke",
                            stylers: [{
                                color: "#000000"
                            }, {
                                lightness: 17
                            }, {
                                weight: 1.2
                            }]
                        }, {
                            featureType: "landscape",
                            elementType: "geometry",
                            stylers: [{
                                color: "#000000"
                            }, {
                                lightness: 20
                            }]
                        }, {
                            featureType: "poi",
                            elementType: "geometry",
                            stylers: [{
                                color: "#000000"
                            }, {
                                lightness: 21
                            }]
                        }, {
                            featureType: "road.highway",
                            elementType: "geometry.fill",
                            stylers: [{
                                color: "#000000"
                            }, {
                                lightness: 17
                            }]
                        }, {
                            featureType: "road.highway",
                            elementType: "geometry.stroke",
                            stylers: [{
                                color: "#000000"
                            }, {
                                lightness: 29
                            }, {
                                weight: .2
                            }]
                        }, {
                            featureType: "road.arterial",
                            elementType: "geometry",
                            stylers: [{
                                color: "#000000"
                            }, {
                                lightness: 18
                            }]
                        }, {
                            featureType: "road.local",
                            elementType: "geometry",
                            stylers: [{
                                color: "#000000"
                            }, {
                                lightness: 16
                            }]
                        }, {
                            featureType: "transit",
                            elementType: "geometry",
                            stylers: [{
                                color: "#000000"
                            }, {
                                lightness: 19
                            }]
                        }, {
                            featureType: "water",
                            elementType: "geometry",
                            stylers: [{
                                color: "#000000"
                            }, {
                                lightness: 17
                            }]
                        }]
                    };
                    return void 0 !== i[t] ? i[t] : i.blurWater
                }
            }, {
                key: "mapInit",
                value: function() {
                    var t = this;
                    if (!this.$container.length || this.$container.data("initialized")) return !1;
                    if (this.ggGeoCode = null, this.oTranslation = WILOKE_LISTGO_TRANSLATION, this.$map = document.getElementById(this.id), this.sByLatLng = null, this.setUp()/*, this.watch(), this.showResult()*/, !_.isEmpty(this.aListings)) {
                        this.$container.data("initialized", !0);
                        var i = this.getCenter(),
                            e = this.parseCoordinate(i);
                        this.oData.center = new google.maps.LatLng(e[0], e[1]), this.instMap = new google.maps.Map(this.$map, this.oData), this.$container.on("reset_listing", function() {
                            t.aFilterListings()
                        }), this.$container.trigger("listing_change"), this.$submitSearchKeyWord.on("click", function(i) {
                            i.preventDefault(), t.$searchForm.trigger("focus_search")
                        }), this.addLayers(this.aListings), this.controlPanel(), "" !== this.$search.val() && (this.$searchForm.trigger("focus_search"), this.$wrap.addClass("list-map-wrap--setting-active"));
                        var s = {};
                        "" !== this.s_listing_location && ("" !== this.$searchForm.find("#s-location-place-id").val() ? (s.placeID = this.$searchForm.find("#s-location-place-id").val(), s.is_suggestion = !1) : "" !== this.$searchForm.find("#s-location-term-id").val() && (s.term_id = this.$searchForm.find("#s-location-term-id").val(), s.is_suggestion = !0), _.isEmpty(s) || this.$location.trigger("location_changed", [s]))
                    }
                }
            }, {
                key: "toggleDraggableControl",
                value: function() {
                    var t = this.$container,
                        i = this.instMap,
                        e = decodeURIComponent(WILOKE_GLOBAL.homeURI),
                        s = e + "img/draggable.png",
                        a = e + "img/disabledraggable.png",
                        n = "";
                    return new(L.Control.extend({
                        options: {
                            position: "topright"
                        },
                        onAdd: function(e) {
                            var o = L.DomUtil.create("div", "leaflet-bar leaflet-control leaflet-control-toggle-draggable");
                            return "enable" === t.data("disabledraggableoninit") ? (i.dragging.disable(), i.touchZoom.disable(), i.zoomControl.disable(), i.scrollWheelZoom.disable(), i.touchZoom.disable(), i.boxZoom.disable(), i.tap && i.tap.disable(), i.doubleClickZoom.disable(), t.data("isDisabled", !0), n = s) : n = a, o.style.backgroundColor = "white", o.style.backgroundImage = "url(" + n + ")", o.style.backgroundSize = "30px 30px", o.style.width = "30px", o.style.height = "30px", o.onclick = function() {
                                t.data("isDisabled") ? (i.dragging.enable(), i.touchZoom.enable(), i.tap && i.tap.enable(), i.zoomControl.enable(), i.touchZoom.enable(), i.boxZoom.enable(), i.doubleClickZoom.enable(), i.scrollWheelZoom.enable(), t.data("isDisabled", !1), o.style.backgroundImage = "url(" + a + ")") : (i.dragging.disable(), i.touchZoom.disable(), i.zoomControl.disable(), i.tap && i.tap.disable(), i.touchZoom.disable(), i.doubleClickZoom.disable(), i.boxZoom.disable(), i.scrollWheelZoom.disable(), t.data("isDisabled", !0), o.style.backgroundImage = "url(" + s + ")")
                            }, o
                        }
                    }))
                }
            }, {
                key: "selected",
                value: function(t) {
                    return this.s_mapdirections === t
                }
            }, {
                key: "isInsideCircle",
                value: function(t) {
                    return null === this.instMyLocation || "mylocation" !== this.s_listing_location || this.instMyLocation.distanceTo(this.parseCoordinate(t)) <= this.convertRadiusToMeters()
                }
            }, {
                key: "hasMatched",
                value: function(t, i) {
                    if (!t || !i) return !1;
                    var e = !1;
                    return _.each(t, function(t) {
                        if (-1 !== i.indexOf(t)) return e = !0, !0
                    }), e
                }
            }, {
                key: "detectListingIncatByListinData",
                value: function(t) {
                    return !("" !== this.s_listing_cat && !_.isNaN(this.s_listing_cat)) || void 0 !== t && (_.isNumber(t) ? this.s_listing_cat === t : -1 !== (t = t.split(",")).indexOf(this.s_listing_cat))
                }
            }, {
                key: "showUpAllListingInLocation",
                value: function(t) {
                    var i = this.dominoPopupChildrenLocation(t),
                        e = this.$showListing.find('li.listing-item[data-locationid="' + t + '"]');
                    if (this.$showListing.find(".listing-item").addClass("hidden"), this.s_listing_cat ? this.detectListingIncatByListinData(e.data("catids")) ? e.removeClass("hidden") : e.addClass("hidden") : e.removeClass("hidden"), !_.isEmpty(i))
                        for (var s in i) this.showUpAllListingInLocation(i[s])
                }
            }, {
                key: "dominoPopupChildrenLocation",
                value: function(t) {
                    var i = this,
                        e = [];
                    return this.$showListing.find('li.listing-item[data-parentlocationid="' + t + '"]').each(function(t) {
                        i.detectListingIncatByListinData(s(this).data("catids")) && (s(t.currentTarget).removeClass("hidden"), e.push(s(this).attr("data-locationid")))
                    }), e
                }
            }, {
                key: "deg2rad",
                value: function(t) {
                    return t * (Math.PI / 180)
                }
            }, {
                key: "getDistanceFromLatLonInKm",
                value: function(t, i, e, s) {
                    var a = this.deg2rad(e - t),
                        n = this.deg2rad(s - i),
                        o = Math.sin(a / 2) * Math.sin(a / 2) + Math.cos(this.deg2rad(t)) * Math.cos(this.deg2rad(e)) * Math.sin(n / 2) * Math.sin(n / 2);
                    return 6371 * (2 * Math.atan2(Math.sqrt(o), Math.sqrt(1 - o)))
                }
            }, {
                key: "showUpAllListingInRadius",
                value: function() {
                    var t = null,
                        i = this,
                        e = 0;
                    this.$mapSidebar.find("li.listing-item").each(function() {
                        void 0 !== (t = s(this).data("latlng")) && i.detectListingIncatByListinData(s(this).data("catids")) && (t = t.split(","), (e = i.getDistanceFromLatLonInKm(parseFloat(t[0], 10), parseFloat(t[1], 10), i.sByLatLng.lat, i.sByLatLng.lng)) <= i.radiusInKM && (s(this).removeClass("hidden"), i.aListingInLocations.push(s(this).data("locationid"))))
                    })
                }
            }, {
                key: "isInLocation",
                value: function(t) {
                    if (_.isEmpty(this.aListingInLocations)) return !0;
                    var i = 0,
                        e = t.listing_location_id.length,
                        s = !1;
                    for (i = 0; i < e; i++)
                        if (-1 !== this.aListingInLocations.indexOf(t.listing_location_id[i])) {
                            s = !0;
                            break
                        }
                    return s
                }
            }, {
                key: "isInCategory",
                value: function(t) {
                    return !(!_.isNaN(this.s_listing_cat) && "" !== this.s_listing_cat) || !(_.isUndefined(t.listing_cat_id) || !t.listing_cat_id) && -1 !== _.indexOf(t.listing_cat_id, this.s_listing_cat)
                }
            }, {
                key: "isMatchedTitle",
                value: function(t) {
                    return !(_.isNaN(this.s_listing_cat) && !_.isNull(this.s) && "" !== this.s) || (this.s = this.s.toLowerCase(), -1 !== t.title.toLowerCase().search(this.s))
                }
            }, {
                key: "isOpenNow",
                value: function(t) {
                    return 0 === this.s_open_now || "opening" === t.business_status.status
                }
            }, {
                key: "clearMarker",
                value: function(t) {
                    t.oInit.setMap(null)
                }
            }, {
                key: "showMarker",
                value: function(t) {
                    t.oInit.setMap(this.instMap)
                }
            }, {
                key: "showResult",
                value: function() {
                    var t = this;
                    this.$searchForm.on("show_search_result", function() {
                        t.$mapSidebar.find(".listgo-map__result").addClass("loading");
                        var i = [],
                            e = [],
                            a = {};
                        if (1 !== t.s_highest_rated && -1 !== t.s_highest_rated || t.$container.trigger("listing_change"), _.isEmpty(t.aListingInLocations) ? _.forEach(t.aRawListings, function(e) {
                                t.isInLocation(e) && i.push(e)
                            }) : i = t.aListingInLocations, "" === t.s_listing_cat || _.isNaN(t.s_listing_cat) || (a = i[0], i = _.filter(i, function(i) {
                                return t.isInCategory(i)
                            })), i.length) {
                            var n = 0;
                            _.forEach(i, function(i) {
                                return (1 !== t.s_open_now || "opening" === i.business_status.status) && (!(1 === t.s_highest_rated && parseInt(i.average_rating, 10) < 3) && void(t.isMatchedTitle(i) && t.isOpenNow(i) && (e.push(i.ID), 0 === n && (a = i), n++)))
                            }), t.$showListing.find("li").each(function() {
                                -1 === e.indexOf(s(this).data("postid")) ? s(this).addClass("hidden") : s(this).removeClass("hidden")
                            })
                        } else t.$showListing.find("li").addClass("hidden");
                        !_.isEmpty(a) && _.isEmpty(t.aListingInLocations) && t.panTo(a.listing_settings.map.latlong, !0), t.aTemporaryCachingResults = [], t.$mapSidebar.find(".listgo-map__result").removeClass("loading"), t.$showListing.find("li:not(.hidden)").length ? s("#wiloke-map-no-results").addClass("hidden") : s("#wiloke-map-no-results").removeClass("hidden")
                    })
                }
            }, {
                key: "aFilterListings",
                value: function() {
                    var t = this;
                    this.aListings = this.aRawListings, _.isNaN(this.s_listing_location) || _.isUndefined(this.s_listing_location) || "all" === this.s_listing_location || _.isNull(this.s_listing_location) || "mylocation" === this.s_listing_location || (this.aListings = _.filter(this.aListings, function(i) {
                        if (i.listing_settings && !_.isUndefined(i.listing_settings.map) && !_.isUndefined(i.listing_location_id)) return t.s_listing_location = parseInt(t.s_listing_location, 10), -1 !== _.indexOf(i.listing_location_id, t.s_listing_location)
                    })), _.isNaN(this.s_listing_cat) || "" === this.s_listing_cat || (this.s_listing_cat = parseInt(this.s_listing_cat, 10), this.aListings = _.filter(this.aListings, function(i) {
                        if (i.listing_settings && !_.isUndefined(i.listing_settings.map) && !_.isUndefined(i.listing_cat_id)) return -1 !== _.indexOf(i.listing_cat_id, t.s_listing_cat)
                    })), this.$container.trigger("listing_change")
                }
            }, {
                key: "controlPanel",
                value: function() {
                    var t = this;
                    this.$toggleSidebar.on("click", function(i) {
                        i.preventDefault(), t.$wrap.toggleClass("list-map-wrap--setting-active")
                    }), this.$search.on("focus", function(i) {
                        t.$wrap.addClass("list-map-wrap--setting-active")
                    }), this.$searchForm.on("change", function(i, e) {
                        var a = s(i.target);
                        switch (s(i.target).attr("id")) {
                            case "s_listing_cat":
                                t.reCheck = !0, t.s_listing_cat = parseInt(a.val(), 10), t.$container.trigger("reset_listing"), t.showService(t.s_listing_cat), t.$searchForm.trigger("show_search_result");
                                break;
                            case "s_highestrated":
                                t.$highestRated.is(":checked") ? t.s_highest_rated = 1 : t.s_highest_rated = -1, t.$searchForm.trigger("show_search_result");
                                break;
                            case "s_opennow":
                                t.$openNow.is(":checked") ? t.s_open_now = 1 : t.s_open_now = 0, t.$searchForm.trigger("show_search_result");
                                break;
                            case "s_listing_location":
                                t.aListingInLocations = [], t.$searchForm.trigger("show_search_result");
                                break;
                            case "s_search":
                                "" === a.val() && (t.$category.attr("value", ""), t.$category.trigger("change"));
                                break;
                            case "s_toggle_sidebar":
                                t.$sidebar.toggleClass("active")
                        }
                    }), this.ggGeoCode = null === this.ggGeoCode ? new google.maps.Geocoder : this.ggGeoCode, this.$location.on("location_changed", function(i, e) {
                        if (e.is_suggestion) {
                            t.reCheck = !0, t.aListingInLocations = [], t.aListingInLocations.push(e.term_id);
                            var s = t.$mapSidebar.find('li.listing-item[data-locationid="' + e.term_id + '"]:first');
                            if (s.length) {
                                var a = s.data("latlng").split(",");
                                t.panTo(a)
                            }
                            t.showUpAllListingInLocation(t.aListingInLocations)
                        } else t.ggGeoCode.geocode({
                            placeId: e.placeID
                        }, function(i, e) {
                            "OK" === e && (t.sByLatLng = {
                                lat: i[0].geometry.location.lat(),
                                lng: i[0].geometry.location.lng()
                            }, t.panTo([i[0].geometry.location.lat(), i[0].geometry.location.lng()]))
                        })
                    });
                    var i = null,
                        e = !1;
                    this.$search.on("keyup", function(a) {
                        null !== i && i && clearTimeout(i), t.s = t.$search.attr("value"), "" !== t.s ? e || (s(".ui-menu-item").hide(), e = !0) : e && (s(".ui-menu-item").show(), e = !1), i = setTimeout(function() {
                            t.$searchForm.trigger("show_search_result"), clearTimeout(i)
                        }, 500)
                    })
                }
            }, {
                key: "mergeObject",
                value: function(t, i) {
                    for (var e in i) i.hasOwnProperty(e) && (t[e] = i[e]);
                    return t
                }
            }, {
                key: "fetching",
                value: function() {
                    var i = this;
                    if (this.isGetAll) return !1;
                    null !== this.xhr && 200 !== this.xhr.status && this.xhr.abort();
                    var e = this.oData;
                    delete e.styles, delete e.center, this.xhr = jQuery.ajax({
                        type: "POST",
                        url: WILOKE_GLOBAL.ajaxurl,
                        data: {
                            action: "wiloke_loadmore_map",
                            post__not_in: this.aPostIDs,
                            atts: e,
                            s: this.s,
                            s_current_cat: this.find_listing_cat,
                            s_current_location: this.find_listing_location
                        },
                        success: function(e) {
                            if (i.s = null, i.find_listing_cat = null, i.find_listing_location = null, e.success) {
                                var s = e.data;
                                _.isEmpty(s) ? i.isGetAll = !0 : (i.aRawListings = "object" === t(i.aRawListings) ? i.mergeObject(i.aRawListings, s) : i.aRawListings.concat(s), i.$container.trigger("reset_listing"), i.addLayers(s), i.$container.trigger("has_new_listing"))
                            } else i.isGetAll = !0
                        }
                    })
                }
            }]), a
        }();
        s(document).ready(function() {
            if (s("#listgo-map").length) {
                s(window).on("Wiloke.Resizemap", function() {
                    if (s("#listgo-map").data("initialized")) return !1;
                    new a("listgo-map")
                });
                var t = setTimeout(function() {
                    if (s("#listgo-map").data("initialized")) return clearTimeout(t), !1;
                    new a("listgo-map"), clearTimeout(t)
                }, 4e3)
            }
        })
    }(jQuery)
}();