<?php

function smarty_function_tab_header($params, &$template) {
    $smarty = $template->smarty;

    if (!isset($params['name']))
        return;
    $name = trim($params['name']);
    $label = $name;
    $active = FALSE;
    if (isset($params['label']))
        $label = trim($params['label']);
    if (isset($params['active'])) {
        $tmp = trim($params['active']);
        if ($tmp == $name) {
            $active = TRUE;
        } else {
            $active = cms_to_bool($tmp);
        }
    }

    if (isset($params['class']))
        $class = trim($params['class']);

    $out = cms_admin_tabs::set_tab_header($name, $label, $active, $class);
    if (isset($params['assign'])) {
        $smarty->assign(trim($params['assign']), $out);
        return;
    }
    return $out;
}
