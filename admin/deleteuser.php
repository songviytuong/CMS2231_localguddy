<?php

$CMS_ADMIN_PAGE = 1;

require_once("../lib/include.php");
$urlext = '?' . CMS_SECURE_PARAM_NAME . '=' . $_SESSION[CMS_USER_KEY];

check_login();
$cur_userid = get_userid();
if (!check_permission($cur_userid, 'Manage Users')) {
    die('Permission Denied');
    return;
}

$dodelete = true;

$user_id = -1;
if (isset($_GET["user_id"])) {
    $user_id = $_GET["user_id"];
    $user_name = "";
    $userid = get_userid();

    if ($user_id != $cur_userid) {
        $gCms = cmsms();
        $userops = $gCms->GetUserOperations();
        $oneuser = $userops->LoadUserByID($user_id);
        $user_name = $oneuser->username;
        $ownercount = $userops->CountPageOwnershipByID($user_id);

        if ($ownercount > 0) {
            $dodelete = false;
        }

        if ($dodelete) {
            \CMSMS\HookManager::do_hook('Core::DeleteUserPre', ['user' => &$oneuser]);

            cms_userprefs::remove_for_user($user_id);
            $oneuser->Delete();

            \CMSMS\HookManager::do_hook('Core::DeleteUserPost', ['user' => &$oneuser]);

            // put mention into the admin log
            audit($user_id, 'Admin Username: ' . $user_name, 'Deleted');
        }
    }
}

if ($dodelete == true) {
    redirect("listusers.php" . $urlext);
} else {
    redirect("listusers.php" . $urlext . "&message=" . lang('erroruserinuse'));
}
