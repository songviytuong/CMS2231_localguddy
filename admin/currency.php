<?php

$orig_memory = (function_exists('memory_get_usage') ? memory_get_usage() : 0);

$CMS_ADMIN_PAGE = 1;
$CMS_TOP_MENU = 'admin';
$CMS_ADMIN_TITLE = 'currency';
require_once("../lib/include.php");
$urlext = '?' . CMS_SECURE_PARAM_NAME . '=' . $_SESSION[CMS_USER_KEY];
$thisurl = basename(__FILE__) . $urlext;
$userid = get_userid(); // Checks also login
if (!check_permission($userid, 'Manage My Settings') && !check_permission($userid, 'Manage Currency'))
    return;

$userobj = UserOperations::get_instance()->LoadUserByID($userid); // <- Safe to do, cause if $userid fails, it redirects automatically to login.
$db = cmsms()->GetDb();
$error = '';
$message = '';
/**
 * Cancel
 */
if (isset($_POST["cancel"]))
    redirect("index.php" . $urlext);

/**
 * Check tab
 */
$tab = '';
if (isset($_POST['active_tab']))
    $tab = trim(cleanValue($_POST['active_tab']));

/**
 * Submit account
 *
 * NOTE: Assumes that we succesfully acquired user object.
 */
if (isset($_POST['sync']) && check_permission($userid, 'Manage Currency')) {
    // echo $_POST['sync'];
    switch ($_POST['sync']) {
        case 'countries';
            break;
        case 'currencies';
            break;
        default:
            $error = 'Lỗi rồi nhé...';
            break;
    }
    if (!$error) {
        $message = lang('sync_' . $_POST['sync']);
        $tab = $_POST['sync'] . 'tab';
    }
} // end of account submit

/**
 * Build page
 */
include_once("header.php");

if ($error != "") {
    $themeObject->ShowErrors($error);
}
if ($message != "") {
    $themeObject->ShowMessage($message);
}

$smarty = cmsms()->GetSmarty();
$contentops = cmsms()->GetContentOperations();
$smarty->assign('SECURE_PARAM_NAME', CMS_SECURE_PARAM_NAME); // Assigned at include.php?
$smarty->assign('CMS_USER_KEY', $_SESSION[CMS_USER_KEY]); // Assigned at include.php?

$url = 'region.json';
$cachefile = TMP_CACHE_LOCATION . '/region_' . md5($url) . '.json';
if (!is_file($cachefile)) {
    $data = @file_get_contents($url);
    if ($data) {
        file_put_contents($cachefile, $data);
        $regions = $cachefile;
    }
} else {
    $regions = $cachefile;
}
$regions = json_decode(@file_get_contents($regions));

#Tabs
$out = $themeObject->StartTabHeaders();
if (check_permission($userid, 'Manage Currency')) {
    $out .= $themeObject->SetTabHeader('countriestab', lang('countries'), ('countriestab' == $tab) ? true : false);
    $out .= $themeObject->SetTabHeader('currenciestab', lang('currencies'), ('currenciestab' == $tab) ? true : false);
}

$out .= $themeObject->EndTabHeaders() . $themeObject->StartTabContent();
$smarty->assign('tab_start', $out);

$smarty->assign('tabs_end', $themeObject->EndTabContent());
$smarty->assign('countries_start', $themeObject->StartTab("countriestab"));
$smarty->assign('currencies_start', $themeObject->StartTab("currenciestab"));
$smarty->assign('tab_end', $themeObject->EndTab());

$smarty->assign('countries', $regions->countries);
$smarty->assign('currencies', $regions->currencies);

$smarty->assign('managecurrency', check_permission($userid, 'Manage Currency'));
$smarty->assign('formurl', $thisurl);

$smarty->assign('sync_countries', '<input type="submit" name="sync_countries" value="' . lang('sync_countries') . '" class="pagebutton" />');
$smarty->assign('sync_currencies', '<input type="submit" name="sync_currencies" value="' . lang('sync_currencies') . '" class="pagebutton" />');
# Output
$smarty->display('mycurrency.tpl');
include_once("footer.php");
