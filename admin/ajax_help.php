<?php

$CMS_ADMIN_PAGE = 1;
require_once("../lib/include.php");
$urlext = '?' . CMS_SECURE_PARAM_NAME . '=' . $_SESSION[CMS_USER_KEY];
check_login();

$realm = 'admin';
$key = 'help';
$out = 'NO HELP KEY SPECIFIED';
if (isset($_GET['key'])) $key = cms_htmlentities(trim($_GET['key']));
if (strstr($key, '__') !== FALSE) {
  list($realm, $key) = explode('__', $key, 2);
}
if (strtolower($realm) == 'core') $realm = 'admin';
$out = CmsLangOperations::lang_from_realm($realm, $key);

echo $out;
exit;
