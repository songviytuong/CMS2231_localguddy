<?php

$CMS_ADMIN_PAGE = 1;

require_once("../lib/include.php");
$urlext = '?' . CMS_SECURE_PARAM_NAME . '=' . $_SESSION[CMS_USER_KEY];
$userid = get_userid();
$access = check_permission($userid, 'Modify User-defined Tags');
if (!$access) {
    die('Permission Denied');
    return;
}

include_once("header.php");

function listudt_summarize($str, $numwords, $ets = '...')
{
    $str = strip_tags($str);
    $stringarray = explode(" ", $str);
    $numwords = min(max($numwords, 1), 100);
    if ($numwords >= count($stringarray)) return $str;
    $tmp = array_slice($stringarray, 0, $numwords);
    $tmp = implode(' ', $tmp) . $ets;
    return $tmp;
}

if (FALSE == empty($_GET['message'])) echo $themeObject->ShowMessage(lang($_GET['message']));

$list = UserTagOperations::get_instance()->ListUserTags();
$tags = null;
if (count($list)) {
    foreach ($list as $id => $label) {
        $tag = UserTagOperations::get_instance()->GetUserTag($id);
        $rec = array();
        $rec['id'] = $id;
        $rec['name'] = $label;
        $rec['description'] = listudt_summarize($tag['description'], 20);
        $tags[$id] = $rec;
    }
}
$smarty = \Smarty_CMS::get_instance();
$smarty->assign('tags', $tags);
$smarty->assign('addurl', 'editusertag.php' . $urlext);
$smarty->assign('urlext', $urlext);
echo $smarty->display('listusertags.tpl');
include_once("footer.php");
